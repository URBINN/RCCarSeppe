/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

public final class BNO055OperationSignals {

    /**
     * A bit that tells whether the accelerometer signal is present.
     */
    public static final int ACCELEROMETER_SIGNAL = 0x1;

    /**
     * A bit that tells whether the magnetometer signal is present.
     */
    public static final int MAGNETOMETER_SIGNAL = 0x2;

    /**
     * A bit that tells whether the gyroscope signal is present.
     */
    public static final int GYROSCOPE_SIGNAL = 0x4;

    /**
     * A bit that tells whether the relative orientation data is present.
     */
    public static final int RELATIVE_ORIENTATION_DATA = 0x8;

    /**
     * A bit that tells whether the absolute orientation data is present.
     */
    public static final int ABSOLUTE_ORIENTATION_DATA = 0x10;

    private BNO055OperationSignals() {
    }
}
