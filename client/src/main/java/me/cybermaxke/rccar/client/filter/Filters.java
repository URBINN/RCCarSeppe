/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.filter;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;

import java.util.Arrays;

import javax.annotation.Nullable;

@SuppressWarnings("Duplicates")
public final class Filters {

    public static final class FilterData {

        private final Vector3d[][] filter;
        @Nullable private final boolean[][] stationary;

        FilterData(Vector3d[][] filter, boolean[][] stationary) {
            this.stationary = stationary;
            this.filter = filter;
        }

        public Vector3d[][] getData() {
            return this.filter;
        }

        @Nullable
        public boolean[][] getStationary() {
            return this.stationary;
        }
    }

    public static FilterData filterRotation(Vector3d[] values, int neighbours,
            double maxAbsValue, int from, int to, boolean standardDeviationFilter) {
        return filter0(null, values, null, neighbours, maxAbsValue,
                from, to, null, standardDeviationFilter);
    }

    public static FilterData filterAcceleration(@Nullable SGFilter3 sgFilter, Vector3d[] values,
            @Nullable double[] coeffs, int neighbours, double maxAbsValue, int from, int to,
            @Nullable Double stationaryDeviation) {
        return filter0(sgFilter, values, coeffs, neighbours, maxAbsValue, from, to, stationaryDeviation, false);
    }

    private static FilterData filter0(@Nullable SGFilter3 sgFilter, Vector3d[] values,
            @Nullable double[] coeffs, int neighbours, double maxAbsValue, int from, int to,
            @Nullable Double stationaryDeviation, boolean standardDeviationCheck) {
        final double minValue = -Math.abs(maxAbsValue);
        final double maxValue = Math.abs(maxAbsValue);
        checkNotNull(values, "values");
        checkArgument(from >= 0 && from <= Math.max(0, values.length - 1),
                "from index %s out of bounds [%s;%s]", from, 0, Math.max(0, values.length - 1));
        checkArgument(to >= 0 && to <= Math.max(0, values.length - 1),
                "to index %s out of bounds [%s;%s]", to, 0, Math.max(0, values.length - 1));
        checkArgument(to >= from,
                "to index %s must be greater or equal to the from index %s", to, from);
        final double[][] v = new double[3][values.length];
        for (int j = 0; j < values.length; j++) {
            v[0][j] = values[j].getX();
            v[1][j] = values[j].getY();
            v[2][j] = values[j].getZ();
        }
        final double[][] v1 = new double[v.length][];
        final boolean[][] s = stationaryDeviation != null ? new boolean[v.length][values.length] : null;
        for (int i = 0; i < v.length; i++) {
            v1[i] = new double[v[i].length];
            for (int j = from; j <= to; j++) {
                v1[i][j] = filterPeaks0(j, v[i], neighbours, minValue, maxValue,
                        stationaryDeviation != null ? s[i] : null,
                        stationaryDeviation == null ? 0 : stationaryDeviation, standardDeviationCheck);
            }
            if (sgFilter != null) {
                v[i] = sgFilter.smooth(v1[i], coeffs);
            } else {
                v[i] = v1[i];
            }
        }
        final Vector3d[] output = Arrays.copyOf(values, values.length);
        for (int j = from; j <= to; j++) {
            output[j] = new Vector3d(v[0][j], v[1][j], v[2][j]);
        }
        final Vector3d[] output1 = Arrays.copyOf(values, values.length);
        for (int j = from; j <= to; j++) {
            output1[j] = new Vector3d(v1[0][j], v1[1][j], v1[2][j]);
        }
        return new FilterData(new Vector3d[][] { output, output1 }, s);
    }

    private static double filterPeaks0(int j, double[] v, int neighbours, double minValue,
            double maxValue, @Nullable boolean[] stationary, double stationaryDeviation, boolean standardDeviationCheck) {
        /// Step 1: Change faulty value range, values bigger then the working range, spikes.
        /// Step 2: Change values outside the standard deviation of the surrounding values.
        /// Step 3: There is almost no noise when stationary, when the value doesn't change
        ///         a lot, then the value is forced to zero, only processed if 'stationary'
        ///         isn't null.
        int minIndex = Math.max(0, j - neighbours);
        int maxIndex = Math.min(v.length - 1, j + neighbours);

        // Peak and standard deviation peak filterAcceleration combined
        double t = 0;
        int c = 0;

        // Generate the total value and calculate the mean
        for (int i = minIndex; i <= maxIndex; i++) {
            if (v[i] >= minValue && v[i] <= maxValue) {
                t += v[i];
                c++;
            }
        }
        double m =  t / (double) c;
        // We received a signal that is way out of line, use the previous and next signals
        // to assume the actual value. This will be done separately for the x, y and z
        // components.
        if (v[j] < minValue || v[j] > maxValue) {
            return m;
        }

        if (standardDeviationCheck) {
            t = 0;
            c = 0;
            for (int i = minIndex; i <= maxIndex; i++) {
                if (v[i] >= minValue && v[i] <= maxValue) {
                    final double a = v[i] - m;
                    t += a * a;
                    c++;
                }
            }

            // Calculate the standard deviation
            final double sd = Math.sqrt(t / (double) c);
            // Calculate the min and max values
            minValue = m - 2 * sd;
            maxValue = m + 2 * sd;

            t = 0;
            c = 0;
            for (int i = minIndex; i <= maxIndex; i++) {
                if (v[i] >= minValue && v[i] <= maxValue) {
                    final double a = v[i] - m;
                    t += a * a;
                    c++;
                }
            }
            m = t / (double) c;
        }
        if (stationary == null) {
            return m;
        }

        minIndex = Math.max(0, j - 15);
        maxIndex = j;

        double max = 0;
        double min = 0;

        for (int i = minIndex; i <= maxIndex; i++) {
            final double a = Math.abs(v[i]);
            if (a < 1) {
                max = Math.max(v[i], max);
                min = Math.min(v[i], min);
            } else if (a < 1.2 || a > 1.4){
                max = Double.MAX_VALUE;
                break;
            }
        }
        if (max - min < stationaryDeviation) {
            stationary[j] = true;
            return 0;
        }

        minIndex = j;
        maxIndex = Math.min(v.length - 1, j + 15);

        if (maxIndex - minIndex > 2) {
            max = 0;
            min = 0;

            for (int i = minIndex; i <= maxIndex; i++) {
                final double a = Math.abs(v[i]);
                if (a < 1) {
                    max = Math.max(v[i], max);
                    min = Math.min(v[i], min);
                } else if (a < 1.2 || a > 1.4){
                    max = Double.MAX_VALUE;
                    break;
                }
            }
            if (max - min < stationaryDeviation) {
                stationary[j] = true;
                return 0;
            }
        }

        return m;
    }

    private Filters() {
    }
}
