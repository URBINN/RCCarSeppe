/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network.handler;

import me.cybermaxke.rccar.network.Handler;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.server.ServerHandshakePacket;

public class ServerHandshakeHandler implements Handler<ServerHandshakePacket> {

    @Override
    public void handle(Session session, ServerHandshakePacket packet) {
        session.setProtocolVersion(packet.getProtocol());
    }
}
