/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.orientation.debug;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;

public class DebugOrientationSystem implements OrientationSystem {

    @Override
    public Vector3d getRotationVector() {
        return Vector3d.ZERO;
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
    }
}
