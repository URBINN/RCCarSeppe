/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import static me.cybermaxke.rccar.sys.PulseableSystemPiece.INTERVAL_25_MILLIS;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.Framework;
import me.cybermaxke.rccar.client.bno055.BNO055CalibrationCommand;
import me.cybermaxke.rccar.client.driving.DrivingSystem;
import me.cybermaxke.rccar.client.driving.DrivingSystemImpl;
import me.cybermaxke.rccar.client.io.I2CBusExecutor;
import me.cybermaxke.rccar.client.network.ClientNetworkSystem;
import me.cybermaxke.rccar.client.network.ClientSession;
import me.cybermaxke.rccar.client.orientation.debug.DebugOrientationSystem;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;
import me.cybermaxke.rccar.client.positioning.bno055.BNO055PositioningDrivingTestSystem;
import me.cybermaxke.rccar.client.positioning.debug.DebugPositioningSystem;
import me.cybermaxke.rccar.client.positioning.PositioningSystem;
import me.cybermaxke.rccar.client.positioning.bno055.BNO055PositioningSystem;
import me.cybermaxke.rccar.client.positioning.bno055.BNO055PositioningTestSystem;
import me.cybermaxke.rccar.network.Packet;
import me.cybermaxke.rccar.network.types.client.TransformPacket;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public final class Client extends Framework {

    private static boolean DEBUG_MODE;
    public static boolean NO_BNO055_CALIBRATION;

    public static void main(String[] args) throws IOException {
        String serverIp = "localhost";
        int index = 0;
        while (index < args.length) {
            final String arg = args[index++];
            switch (arg) {
                case "--no-bno055-calibration":
                    final String value0 = index < args.length ? args[index] : "--";
                    if (value0.startsWith("--")) {
                        NO_BNO055_CALIBRATION = true;
                    } else {
                        NO_BNO055_CALIBRATION = Boolean.parseBoolean(value0);
                        index++;
                    }
                    continue;
                case "--debug":
                case "--d":
                    final String value1 = index < args.length ? args[index] : "--";
                    if (value1.startsWith("--")) {
                        DEBUG_MODE = true;
                    } else {
                        DEBUG_MODE = Boolean.parseBoolean(value1);
                        index++;
                    }
                    continue;
                case "--server-ip":
                case "--server":
                case "--sip":
                    if (index >= args.length) {
                        throw new IllegalStateException("The parameter " + arg + " requires a value.");
                    }
                    serverIp = args[index++];
                    getLogger().info("Found target server ip: {}", serverIp);
                    continue;
                default:
                    getLogger().info("Unknown launch parameter: {}", arg);
            }
        }
        if (DEBUG_MODE) {
            getLogger().info("Debug mode enabled.");
        }

        final Client client;
        try {
            client = new Client(serverIp);
        } catch (IOException e) {
            throw new RuntimeException("Failed to initialize the client.");
        }
        client.init();
    }

    private final PositioningSystem positioningSystem;
    private final OrientationSystem orientationSystem;
    private final ClientNetworkSystem clientNetworkSystem;

    private final Engine engine;
    private final UUID uniqueId;

    private Vector3d lastRotation;
    private Vector3d lastPosition;

    private Client(String serverIp) throws IOException {
        if (DEBUG_MODE) {
            this.engine = new DebugEngine();
        } else {
            this.engine = new RCCarEngine();
        }
        // Read or generate the UUID of the client
        final Path uuidFile = Paths.get("rccar_client_uuid.dat");
        if (Files.exists(uuidFile)) {
            try (DataInputStream dis = new DataInputStream(Files.newInputStream(uuidFile))) {
                final long most = dis.readLong();
                final long least = dis.readLong();
                this.uniqueId = new UUID(most, least);
            } catch (IOException e) {
                throw new RuntimeException("Unable to read the client uuid file.", e);
            }
        } else {
            this.uniqueId = UUID.randomUUID();
            final Path dir = uuidFile.getParent();
            if (dir != null && !Files.exists(dir)) {
                try {
                    Files.createDirectories(dir);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            try (DataOutputStream dos = new DataOutputStream(Files.newOutputStream(uuidFile))) {
                dos.writeLong(this.uniqueId.getMostSignificantBits());
                dos.writeLong(this.uniqueId.getLeastSignificantBits());
                dos.flush();
            } catch (IOException e) {
                throw new RuntimeException("Unable to write the client uuid file.", e);
            }
        }

        // Create the client network system
        this.clientNetworkSystem = new ClientNetworkSystem(this, serverIp);
        addSystem(this.clientNetworkSystem);

        // Create the orientation and positioning systems
        if (DEBUG_MODE) {
            this.positioningSystem = new DebugPositioningSystem();
            addSystem(this.positioningSystem);
            this.orientationSystem = new DebugOrientationSystem();
            addSystem(this.orientationSystem);
        } else {
            final BNO055PositioningSystem positioningSystem = new BNO055PositioningSystem();
            addSystem(positioningSystem);
            getCommandManager().register("bno055-pos", positioningSystem);
            this.orientationSystem = positioningSystem;
            this.positioningSystem = positioningSystem;
            final BNO055PositioningTestSystem testSystem = new BNO055PositioningTestSystem(this.engine, positioningSystem);
            addSystem(testSystem);
            getCommandManager().register("test-system", testSystem);
        }

        addSystem(this.engine);
        // addSystem(new RemoteControllerSystem(this.engine));
        addSystem(PulseableSystemPiece.of(this::pulse, INTERVAL_25_MILLIS));
        final DrivingSystem drivingSystem = new DrivingSystemImpl(
                this.engine, this.positioningSystem, this.orientationSystem);
        addSystem(drivingSystem);
        if (this.positioningSystem instanceof BNO055PositioningSystem) {
            final BNO055PositioningDrivingTestSystem testSystem2 = new BNO055PositioningDrivingTestSystem(
                    drivingSystem, (BNO055PositioningSystem) this.positioningSystem);
            addSystem(testSystem2);
            getCommandManager().register("test-system-2", testSystem2);
        }

        getLogger().info("Loaded the client with unique id: {}", this.uniqueId);
        getCommandManager().register("engine", new EngineCommand(this.engine));
        getCommandManager().register("bno055-cal", new BNO055CalibrationCommand());
    }

    // private boolean direction;

    /**
     * This method will handle synced {@link Packet}s and the
     * {@link RCCarEngine} of the car.
     */
    private void pulse(double deltaTime) {
        final ClientSession session = this.clientNetworkSystem.getSession().orElse(null);
        this.engine.setConnectionStatus(session != null);
        boolean transformUpdate = false;
        final Vector3d rotation = this.orientationSystem.getRotationVector();
        // Update the rotation if needed, use a threshold to not overload the server
        if (this.lastRotation == null || rotation.distanceSquared(this.lastRotation) > 0.05) {
            this.lastRotation = rotation;
            transformUpdate = true;
        }
        final Vector3d position = this.positioningSystem.getPosition();
        // Update the position if needed, use a threshold to not overload the server
        if (this.lastPosition == null || position.distanceSquared(this.lastPosition) > 0.05) {
            this.lastPosition = position;
            transformUpdate = true;
        }
        if (transformUpdate && session != null) {
            session.send(new TransformPacket(position, rotation, System.currentTimeMillis()));
        }

        // Test Movement
        /*
        double steering = this.engine.getSteering();
        if (this.direction) {
           steering += 0.020;
            if (Math.abs(steering - 1.0) < 0.001) {
                this.direction = false;
            }
        } else {
            steering -= 0.020;
            if (Math.abs(steering + 1.0) < 0.001) {
                this.direction = true;
            }
        }
        steering = steering > 1.0 ? 1.0 : steering < -1.0 ? -1.0 : steering;
        this.engine.setSteering(steering);
        this.engine.setSpeed(0.1f);
        */
    }

    @Override
    public void cleanup() {
        super.cleanup();
        I2CBusExecutor.SERVICE.shutdownNow();
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }
}
