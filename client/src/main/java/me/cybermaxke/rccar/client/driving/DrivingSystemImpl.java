/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import static com.google.common.base.Preconditions.checkNotNull;
import static me.cybermaxke.rccar.Framework.getLogger;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.client.CarSpecifications;
import me.cybermaxke.rccar.client.Engine;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;
import me.cybermaxke.rccar.client.positioning.PositioningSystem;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;
import me.cybermaxke.rccar.util.Transform;

import javax.annotation.Nullable;

public class DrivingSystemImpl implements DrivingSystem, PulseableSystemPiece {

    private static final double MAX_PATH_DEVIATION = 0.25;
    private static final double MAX_ANGLE_DEVIATION = 15;

    private final Engine engine;
    private final PositioningSystem positioningSystem;
    private final OrientationSystem orientationSystem;

    private double minimumTurningRange = -1;

    private final PathProcessor pathProcessor = new PathProcessor();

    @Nullable private Path path;

    /**
     * The index of the point the car is currently heading to.
     */
    private int pointIndex = -1;

    /**
     * Creates a new {@link DrivingSystemImpl} with the given {@link Engine}, {@link PositioningSystem}.
     *
     * @param engine The engine of the car
     * @param positioningSystem The positioning system
     * @param orientationSystem The orientation system
     */
    public DrivingSystemImpl(Engine engine, PositioningSystem positioningSystem, OrientationSystem orientationSystem) {
        this.orientationSystem = checkNotNull(orientationSystem, "orientationSystem");
        this.positioningSystem = checkNotNull(positioningSystem, "positioningSystem");
        this.engine = checkNotNull(engine, "engine");
    }

    @Override
    public Engine getEngine() {
        return this.engine;
    }

    @Override
    public void drive(Path path) {
        checkNotNull(path, "path");
        if (this.minimumTurningRange == -1) {
            final CarSpecifications specs = this.engine.getSpecifications();
            this.minimumTurningRange = Math.sqrt(
                    Math.pow(specs.getTurningPointDistance(), 2.0) +
                            Math.pow(specs.getShaftDistance() * (1.0 / Math.tan(Math.toRadians(specs.getMaxWheelTurnAngle()))), 2.0));
            getLogger().info("Calculated a minimum turning range of {} meter", this.minimumTurningRange);
        }
        this.path = this.pathProcessor.process(path, this.minimumTurningRange).getPath();
        this.pointIndex = 0;
    }

    @Override
    public void cancel() {
        this.path = null;
        this.pointIndex = -1;
        this.engine.setSpeed(0);
    }

    @Override
    public boolean hasPath() {
        return this.path != null;
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
    }

    private PathProcessor.ControlResult lastControlResult;
    private Vector3d lastPosition;

    @Override
    public void pulse(double deltaTime) {
        // No need to pulse if we don't have a path to follow
        if (this.path == null) {
            return;
        }
        final Vector3d position = this.positioningSystem.getPosition();
        final Vector3d rotation = this.orientationSystem.getRotationVector();
        final double speed = this.positioningSystem.getSpeed();

        final PathProcessor.ControlResult result = this.pathProcessor.processControlling(
                new Transform(position, rotation),
                this.path,
                this.minimumTurningRange,
                MAX_PATH_DEVIATION,
                MAX_ANGLE_DEVIATION,
                this.pointIndex);

        if (result == null || Math.abs(position.getY()) > 0.5) {
            // The end is reached
            this.path = null;
            this.pointIndex = -1;
            this.engine.setSpeed(0);
            return;
        }

        System.out.println("POS: " + position);
        System.out.println("ROT: " + rotation);
        System.out.println("DEBUG: " + result);

        if (result.isRequired()) {
            final double steerAngle = result.getSteerAngle();
            this.engine.setSpeed((float) steerAngle);
            this.lastControlResult = result;
            this.lastPosition = position;
        } else if (this.lastControlResult != null) {
            final double movement = position.distance(this.lastPosition);
            if (movement > this.lastControlResult.getArcALength() - speed * 0.2) {
                if (movement > (this.lastControlResult.getArcALength() + this.lastControlResult.getArcBLength()) - speed * 0.3) {
                    this.engine.setSteering(0);
                    this.lastControlResult = null;
                } else {
                    this.engine.setSteering((float) -this.lastControlResult.getSteerAngle());
                }
            }
        }
        this.engine.setSpeed(0.65f);
        if (result.getPathIndex() != this.pointIndex) {
            this.pointIndex = result.getPathIndex();
            System.out.println("Moved to the next point: " + result.getPathIndex());
        }
    }
}
