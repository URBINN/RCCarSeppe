/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/**
 * A {@link me.cybermaxke.rccar.client.orientation.OrientationSystem} that utilizes
 * the {@link me.cybermaxke.rccar.client.io.bno055.BNO055} sensor.
 */
package me.cybermaxke.rccar.client.orientation.bno055;
