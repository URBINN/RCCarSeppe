/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static me.cybermaxke.rccar.Framework.getLogger;

import com.google.common.base.Throwables;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

import me.cybermaxke.rccar.client.Client;
import me.cybermaxke.rccar.network.ConnectionSide;
import me.cybermaxke.rccar.network.NetworkSystem;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.pipeline.CodecHandler;
import me.cybermaxke.rccar.network.pipeline.FramingHandler;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.Nullable;

public class ClientNetworkSystem implements NetworkSystem, PulseableSystemPiece {

    private static final int ATTEMPT_CONNECTION_DURATION = 500;
    private static final int RECONNECT_DELAY = 100;

    private final Client client;
    private final String serverIp;

    private EventLoopGroup workerGroup;
    private ScheduledExecutorService connectionExecutor;

    /**
     * The {@link ClientSession} that is currently active.
     */
    @Nullable private volatile ClientSession clientSession;

    public ClientNetworkSystem(Client client, String serverIp) {
        this.serverIp = checkNotNull(serverIp, "serverIp");
        this.client = checkNotNull(client, "client");
    }

    @Override
    public void init() {
        if (this.workerGroup != null) {
            return;
        }
        this.connectionExecutor = Executors.newSingleThreadScheduledExecutor(
                runnable -> new Thread(runnable, "connection"));

        final ClientPacketRegistry packetRegistry = new ClientPacketRegistry();

        final boolean epoll = Epoll.isAvailable();
        this.workerGroup = epoll ? new EpollEventLoopGroup(0, NETTY_THREAD_FACTORY) : new NioEventLoopGroup(0, NETTY_THREAD_FACTORY);

        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(this.workerGroup);
        bootstrap.channel(epoll ? EpollSocketChannel.class : NioSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline()
                        .addLast(new ReadTimeoutHandler(Session.READ_TIMEOUT_SECONDS))
                        .addLast(new FramingHandler())
                        .addLast(new CodecHandler(packetRegistry, ConnectionSide.CLIENT))
                        .addLast(new ClientSession(client, ch, packetRegistry, ConnectionSide.CLIENT) {

                            @Override
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                super.channelActive(ctx);
                                checkState(clientSession == null, "There is already a active client session.");
                                clientSession = this;
                            }

                            @Override
                            public void channelInactive(ChannelHandlerContext ctx) throws Exception {
                                super.channelInactive(ctx);
                                clientSession = null;
                            }
                        });
            }
        });

        // Attempt to reconnect constantly to the network, with a small delay between the requests
        this.connectionExecutor.submit(() -> {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    final ChannelFuture future = bootstrap.connect(this.serverIp, Session.PORT);
                    if (!future.await(ATTEMPT_CONNECTION_DURATION) && !future.cancel(false)) {
                        future.await();
                    }
                    if (future.isSuccess()) {
                        getLogger().info("Successfully connected to the server.");
                        future.channel().closeFuture().sync();
                        getLogger().info("Disconnected from the server.");
                    }
                } catch (InterruptedException e) {
                    getLogger().error("An exception occurred while waiting for a closed connection", e);
                }
                Thread.sleep(RECONNECT_DELAY);
            }
        });
    }

    @Override
    public void cleanup() {
        if (this.workerGroup == null) {
            return;
        }
        this.connectionExecutor.shutdownNow();
        this.connectionExecutor = null;
        try {
            this.workerGroup.shutdownGracefully().await();
        } catch (InterruptedException e) {
            throw Throwables.propagate(e);
        }
        this.workerGroup = null;
    }

    @Override
    public Duration getPulseInterval() {
        return INTERVAL_25_MILLIS;
    }

    @Override
    public void pulse(double deltaTime) {
        final ClientSession session = this.clientSession;
        if (session != null) {
            session.process();
        }
    }

    public Optional<ClientSession> getSession() {
        return Optional.ofNullable(this.clientSession);
    }
}
