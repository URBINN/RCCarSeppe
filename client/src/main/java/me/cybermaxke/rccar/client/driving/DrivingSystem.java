/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import me.cybermaxke.rccar.client.Engine;
import me.cybermaxke.rccar.sys.SystemPiece;

/**
 * The driving system will control the car movement and steering to
 * result the car in following the given {@link Path}.
 */
public interface DrivingSystem extends SystemPiece {

    /**
     * Gets the {@link Engine} that is being controlled.
     *
     * @return The engine
     */
    Engine getEngine();

    /**
     * Drives the car for the given {@link Path}.
     *
     * @param path The path
     */
    void drive(Path path);

    /**
     * Cancels the current path, if the car is currently driving.
     */
    void cancel();

    /**
     * Gets whether car is currently driving,
     * has a path to drive on.
     *
     * @return Has path/is driving
     */
    boolean hasPath();
}
