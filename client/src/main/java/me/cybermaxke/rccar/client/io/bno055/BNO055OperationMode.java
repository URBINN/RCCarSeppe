/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import static me.cybermaxke.rccar.client.io.bno055.BNO055OperationSignals.ABSOLUTE_ORIENTATION_DATA;
import static me.cybermaxke.rccar.client.io.bno055.BNO055OperationSignals.ACCELEROMETER_SIGNAL;
import static me.cybermaxke.rccar.client.io.bno055.BNO055OperationSignals.GYROSCOPE_SIGNAL;
import static me.cybermaxke.rccar.client.io.bno055.BNO055OperationSignals.MAGNETOMETER_SIGNAL;
import static me.cybermaxke.rccar.client.io.bno055.BNO055OperationSignals.RELATIVE_ORIENTATION_DATA;

public enum BNO055OperationMode {
    CONFIG          (0x00, 0),
    ACCONLY         (0x01, ACCELEROMETER_SIGNAL),
    MAGONLY         (0x02, MAGNETOMETER_SIGNAL),
    GYRONLY         (0x03, GYROSCOPE_SIGNAL),
    ACCMAG          (0x04, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL),
    ACCGYRO         (0x05, ACCELEROMETER_SIGNAL | GYROSCOPE_SIGNAL),
    MAGGYRO         (0x06, MAGNETOMETER_SIGNAL | GYROSCOPE_SIGNAL),
    AMG             (0x07, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL | GYROSCOPE_SIGNAL),
    IMU             (0x08, ACCELEROMETER_SIGNAL | GYROSCOPE_SIGNAL | RELATIVE_ORIENTATION_DATA),
    COMPASS         (0x09, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL | ABSOLUTE_ORIENTATION_DATA),
    M4G             (0x0A, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL | RELATIVE_ORIENTATION_DATA),
    NDOF_FMC_OFF    (0x0B, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL | GYROSCOPE_SIGNAL | ABSOLUTE_ORIENTATION_DATA),
    NDOF            (0x0C, ACCELEROMETER_SIGNAL | MAGNETOMETER_SIGNAL | GYROSCOPE_SIGNAL | ABSOLUTE_ORIENTATION_DATA),
    ;

    private static final int FUSION_MODE = ABSOLUTE_ORIENTATION_DATA | RELATIVE_ORIENTATION_DATA;

    private final int opcode;
    private final byte operatingData;

    BNO055OperationMode(int val, int operatingData) {
        this.operatingData = (byte) operatingData;
        this.opcode = val;
    }

    int getOpcode() {
        return this.opcode;
    }

    public boolean isFusionMode() {
        return (this.operatingData & FUSION_MODE) != 0;
    }

    public int getOperatingData() {
        return this.operatingData;
    }

    public boolean testDataSupport(int mask) {
        return (this.operatingData & mask) != 0;
    }
}
