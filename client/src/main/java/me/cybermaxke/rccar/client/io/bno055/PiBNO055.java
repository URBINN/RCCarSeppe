/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;

import me.cybermaxke.rccar.util.Axis;

import java.io.IOException;

// https://github.com/adafruit/Adafruit_BNO055
// https://gist.github.com/d2a4u/b1f6c4bcee340e8b734ee2b3c4a922d5
public final class PiBNO055 implements BNO055 {

    public static final int BNO055_ADDRESS_A = 0x28;
    public static final int BNO055_ADDRESS_B = 0x29;

    protected static final double QUATERNION_SCALE = 1.0 / (double) (1 << 14);

    private static final double ACCELEROMETER_SCALE = 1.0 / 100.0;
    private static final double LINEAR_ACCELERATION_SCALE = 1.0 / 100.0;
    private static final double GRAVITY_SCALE = 1.0 / 100.0;
    private static final double GYROSCOPE_SCALE = 1.0 / 900.0;
    private static final double EULER_SCALE = 1.0 / 16.0;
    private static final double MAGNETOMETER_SCALE = 1.0 / 16.0;

    private static final int BNO055_ID = 0xA0;

    private BNO055OperationMode operationMode = BNO055OperationMode.NDOF;
    private boolean extCrystalUse;

    private final I2CDevice device;
    private boolean initialized;

    public PiBNO055(I2CBus bus, int address) throws IOException {
        checkNotNull(bus, "bus");
        this.device = bus.getDevice(address);
    }

    @Override
    public boolean isInitialized() {
        return this.initialized;
    }

    @Override
    public void initialize() {
        if (this.initialized) {
            return;
        }
        // Wait for the sensor to be present
        try {
            init0();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void init0() throws IOException, InterruptedException {
        do {
            Thread.sleep(50);
        } while ((read8(Register.BNO055_CHIP_ID_ADDR) & 0xff) != BNO055_ID);
        Thread.sleep(50);
        // Switch to config mode (just in case since this is the default)
        setMode0(BNO055OperationMode.CONFIG);
        // Reset
        write8(Register.BNO055_SYS_TRIGGER_ADDR, (byte) 0x20);
        Thread.sleep(1000);
        // Wait for the sensor to be present
        while ((read8(Register.BNO055_CHIP_ID_ADDR) & 0xff) != BNO055_ID) {
            Thread.sleep(10);
        }
        Thread.sleep(50);
        // Set to normal power mode
        write8(Register.BNO055_PWR_MODE_ADDR, (byte) BNO055PowerMode.NORMAL.getOpcode());
        Thread.sleep(10);
        // Use external crystal - 32.768 kHz
        write8(Register.BNO055_PAGE_ID_ADDR, (byte) 0x80);
        write8(Register.BNO055_SYS_TRIGGER_ADDR, (byte) 0x00);
        Thread.sleep(10);
        // Set operating mode to mode requested at instantiation
        setMode0(getOperationMode());
        Thread.sleep(20);
        this.initialized = true;
    }

    @Override
    public BNO055OperationMode getOperationMode() {
        return this.operationMode;
    }

    @Override
    public void setMode(BNO055OperationMode operationMode) throws IOException {
        this.operationMode = checkNotNull(operationMode, "operationMode");
        setMode0(operationMode);
    }

    @Override
    public boolean getExtCrystalUse() {
        return this.extCrystalUse;
    }

    @Override
    public void setExtCrystalUse(boolean use) throws IOException {
        this.extCrystalUse = use;
        try {
            setMode0(BNO055OperationMode.CONFIG);
            // Use external crystal - 32.768 kHz
            write8(Register.BNO055_PAGE_ID_ADDR, (byte) 0x00);
            Thread.sleep(25);
            write8(Register.BNO055_SYS_TRIGGER_ADDR, (byte) (use ? 0x80 : 0x00));
            Thread.sleep(10);
            // Set operating mode to mode requested at instantiation
            setMode0(getOperationMode());
            Thread.sleep(20);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void setMode0(BNO055OperationMode operationMode) throws IOException {
        write8(Register.BNO055_OPR_MODE_ADDR, (byte) operationMode.getOpcode());
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Vector3d readEulerAngleData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.isFusionMode(),
                "The euler angle data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_EULER_H_LSB_ADDR, EULER_SCALE);
    }

    @Override
    public Vector3d readMagnetometerData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.testDataSupport(BNO055OperationSignals.MAGNETOMETER_SIGNAL),
                "The magnetometer data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_MAG_DATA_X_LSB_ADDR, MAGNETOMETER_SCALE);
    }

    @Override
    public Vector3d readGyroscopeData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.testDataSupport(BNO055OperationSignals.GYROSCOPE_SIGNAL),
                "The gyroscope data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_GYRO_DATA_X_LSB_ADDR, GYROSCOPE_SCALE);
    }

    @Override
    public Vector3d readGravityData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.isFusionMode(),
                "The gravity data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_GRAVITY_DATA_X_LSB_ADDR, GRAVITY_SCALE);
    }

    @Override
    public Vector3d readLinearAccelerationData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(getOperationMode().isFusionMode(),
                "The linear acceleration data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR, LINEAR_ACCELERATION_SCALE);
    }

    @Override
    public Vector3d readAccelerationData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.testDataSupport(BNO055OperationSignals.ACCELEROMETER_SIGNAL),
                "The acceleration data isn't supported in the current mode: %s", operationMode);
        return readVector(Register.BNO055_ACCEL_DATA_X_LSB_ADDR, ACCELEROMETER_SCALE);
    }

    protected Vector3d readVector(Register register, double scale) throws IOException {
        final byte[] buffer = new byte[6];
        read(register, buffer);
        final double x = (short) ((buffer[1] & 0xff) << 8 | buffer[0] & 0xff);
        final double y = (short) ((buffer[3] & 0xff) << 8 | buffer[2] & 0xff);
        final double z = (short) ((buffer[5] & 0xff) << 8 | buffer[4] & 0xff);
        return new Vector3d(x * scale, y * scale, z * scale);
    }

    @Override
    public Quaterniond readQuaternionData() throws IOException {
        final BNO055OperationMode operationMode = getOperationMode();
        checkState(operationMode.isFusionMode(),
                "The gyroscope data isn't supported in the current mode: %s", operationMode);
        final byte[] buffer = new byte[8];
        read(Register.BNO055_QUATERNION_DATA_W_LSB_ADDR, buffer);
        final double w = (short) ((buffer[1] & 0xff) << 8 | buffer[0] & 0xff);
        final double x = (short) ((buffer[3] & 0xff) << 8 | buffer[2] & 0xff);
        final double y = (short) ((buffer[5] & 0xff) << 8 | buffer[4] & 0xff);
        final double z = (short) ((buffer[7] & 0xff) << 8 | buffer[6] & 0xff);
        return new Quaterniond(x * QUATERNION_SCALE, y * QUATERNION_SCALE, z * QUATERNION_SCALE, w * QUATERNION_SCALE);
    }

    @Override
    public BNO055CalibrationStatus readCalibrationStatus() throws IOException {
        final int raw = read8(Register.BNO055_CALIB_STAT_ADDR);

        final byte system = (byte) ((raw >> 6) & 0x03);
        final byte gyroscope = (byte) ((raw >> 4) & 0x03);
        final byte accelerometer = (byte) ((raw >> 2) & 0x03);
        final byte magnetometer = (byte) (raw & 0x03);

        return new BNO055CalibrationStatus(system, accelerometer, magnetometer, gyroscope);
    }

    @Override
    public boolean isCalibrated() throws IOException {
        final BNO055CalibrationStatus status = readCalibrationStatus();
        final BNO055OperationMode mode = getOperationMode();

        if (mode.testDataSupport(BNO055OperationSignals.ACCELEROMETER_SIGNAL) && status.getAccelerometerState() < 3) {
            return false;
        }
        if (mode.testDataSupport(BNO055OperationSignals.MAGNETOMETER_SIGNAL) && status.getMagnetometerState() < 3) {
            return false;
        }
        if (mode.testDataSupport(BNO055OperationSignals.GYROSCOPE_SIGNAL) && status.getGyroscopeState() < 3) {
            return false;
        }
        return true;
    }

    @Override
    public BNO055SystemStatus readSystemStatus() throws IOException {
        write8(Register.BNO055_PAGE_ID_ADDR, (byte) 0x00);

        final byte systemStatus = read8(Register.BNO055_SYS_STAT_ADDR);
        final byte systemError = read8(Register.BNO055_SYS_ERR_ADDR);
        final byte selfTestResult = read8(Register.BNO055_SELFTEST_RESULT_ADDR);

        return new BNO055SystemStatus(systemStatus, systemError, selfTestResult);
    }

    @Override
    public BNO055VersionInfo readVersionInfo() throws IOException {
        final int accelerometerVersion = read8(Register.BNO055_ACCEL_REV_ID_ADDR);
        final int magnetometerVersion = read8(Register.BNO055_MAG_REV_ID_ADDR);
        final int gyroscopeVersion = read8(Register.BNO055_GYRO_REV_ID_ADDR);
        final int bootloaderVersion = read8(Register.BNO055_BL_REV_ID_ADDR);

        // TODO: What is this?
        // final byte a = read8(Register.BNO055_SW_REV_ID_LSB_ADDR);
        // final byte b = read8(Register.BNO055_SW_REV_ID_MSB_ADDR);
        // final int sw_rev = (b << 8) | a;

        return new BNO055VersionInfo(bootloaderVersion, accelerometerVersion, magnetometerVersion, gyroscopeVersion);
    }

    private byte[] readCalibrationData(Register register) throws IOException {
        final byte[] data = new byte[6];
        read(register, data);
        return data;
    }

    @Override
    public byte[] readCalibrationData() throws IOException {
        final byte[] data = new byte[18];
        read(Register.ACCEL_OFFSET_X_LSB_ADDR, data);
        return data;
    }

    @Override
    public void writeCalibrationData(byte[] calibrationData) throws IOException {
        write(Register.ACCEL_OFFSET_X_LSB_ADDR, calibrationData);
    }

    @Override
    public int readTemperature() throws IOException {
        return read8(Register.BNO055_TEMP_ADDR);
    }

    @Override
    public void remapAxes(Axis remappedXAxis, Axis remappedYAxis, Axis remappedZAxis) throws IOException {
        checkArgument(remappedXAxis != remappedYAxis && remappedXAxis != remappedZAxis && remappedYAxis != remappedZAxis,
                "One of the axes is used multiple times. (%s; %s; %s)", remappedXAxis, remappedYAxis, remappedZAxis);
        final byte mapConfig = (byte) (remappedXAxis.ordinal() | (remappedYAxis.ordinal() << 2) | (remappedZAxis.ordinal() << 4));
        // Switch to config mode (just in case since this is the default)
        setMode0(BNO055OperationMode.CONFIG);
        write8(Register.BNO055_AXIS_MAP_CONFIG_ADDR, mapConfig);
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        setMode0(getOperationMode());
    }

    @Override
    public void setAccelerometerRadius(int radius) throws IOException {
        checkArgument(radius >= -1000 && radius <= 1000, "The radius must be in the range +/- 1000");
        final byte[] buffer = new byte[2];
        buffer[0] = (byte) (radius & 0xff);
        buffer[1] = (byte) ((radius >> 8) & 0xff);
        write(Register.ACCEL_RADIUS_LSB_ADDR, buffer);
    }

    @Override
    public void setMagnetometerRadius(int radius) throws IOException {
        checkArgument(radius >= -960 && radius <= 960, "The radius must be in the range +/- 960");
        final byte[] buffer = new byte[2];
        buffer[0] = (byte) (radius & 0xff);
        buffer[1] = (byte) ((radius >> 8) & 0xff);
        write(Register.MAG_RADIUS_LSB_ADDR, buffer);
    }

    private void write(Register register, byte[] buffer) throws IOException {
        this.device.write(register.getAddress(), buffer);
    }

    private void write(int register, byte[] buffer) throws IOException {
        this.device.write(register, buffer);
    }

    private void write8(Register register, byte value) throws IOException {
        this.device.write(register.getAddress(), value);
    }

    private void write8(int register, byte value) throws IOException {
        this.device.write(register, value);
    }

    private byte read8(Register register) throws IOException {
        return read8(register.getAddress());
    }

    private byte read8(int register) throws IOException {
        final byte[] values = new byte[1];
        read(register, values);
        return values[0];
    }

    private boolean read(Register register, byte[] buffer) throws IOException {
        return read(register.getAddress(), buffer);
    }

    private boolean read(int register, byte[] buffer) throws IOException {
        return !(buffer == null || buffer.length < 1) && this.device.read(register, buffer, 0, buffer.length) > 0;
    }

    @Override
    public void close() {
    }
}
