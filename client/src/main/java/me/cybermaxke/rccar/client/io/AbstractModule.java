/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Closeable;

public abstract class AbstractModule implements Closeable {

    protected static String checkName(String name) {
        checkNotNull(name, "name");
        checkArgument(!name.isEmpty(), "name cannot be empty");
        return name;
    }
}
