/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.orientation;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.sys.SystemPiece;

public interface OrientationSystem extends SystemPiece {

    /**
     * Gets the rotation as a {@link Vector3d}. Every value represents
     * the rotation around the specific axis.
     *
     * @return The rotation vector
     */
    Vector3d getRotationVector();

    /**
     * Gets the rotation as a {@link Quaterniond}.
     *
     * @return The rotation quaternion
     */
    default Quaterniond getRotation() {
        final Vector3d vector = getRotationVector();
        return Quaterniond.fromAxesAnglesDeg(vector.getX(), vector.getY(), vector.getZ());
    }
}
