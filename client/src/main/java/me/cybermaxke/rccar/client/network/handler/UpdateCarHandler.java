/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network.handler;

import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.SyncHandler;
import me.cybermaxke.rccar.network.types.server.UpdateCarPacket;

import java.util.Optional;

public final class UpdateCarHandler implements SyncHandler<UpdateCarPacket> {

    @Override
    public void handle(Session session, UpdateCarPacket packet) {
        final Optional<Car> car = session.getFramework().getCar(packet.getUniqueId());
        if (car.isPresent()) {
            car.get().update(packet.getPosition(), packet.getRotation(), packet.getUpdateTime());
        } else {
            getLogger().info("Received an update for a missing car: {}", packet.getUniqueId());
        }
    }
}
