/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

public enum BNO055PowerMode {
    NORMAL      (0x00),
    LOWPOWER    (0x01),
    SUSPEND     (0x02),
    ;

    private final int opcode;

    BNO055PowerMode(int opcode) {
        this.opcode = opcode;
    }

    int getOpcode() {
        return this.opcode;
    }
}

