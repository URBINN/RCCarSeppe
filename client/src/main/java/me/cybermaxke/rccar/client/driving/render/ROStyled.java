/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.annotation.Nullable;

public final class ROStyled {

    public static ROStyled builder() {
        return new ROStyled();
    }

    @Nullable private Color color;
    @Nullable private Stroke stroke;

    private ROStyled() {
    }

    public ROStyled color(@Nullable Color color) {
        this.color = color;
        return this;
    }

    public ROStyled stroke(@Nullable Stroke stroke) {
        this.stroke = stroke;
        return this;
    }

    public RenderObject build(RenderObject object) {
        return new RenderObjectImpl(this.color, this.stroke, object);
    }

    public static final class RenderObjectImpl implements RenderObject {

        @Nullable private final Color color;
        @Nullable private final Stroke stroke;
        private final RenderObject renderObject;

        public RenderObjectImpl(Color color, Stroke stroke, RenderObject renderObject) {
            this.color = color;
            this.stroke = stroke;
            this.renderObject = renderObject;
        }

        @Override
        public void draw(Graphics2D graphics, double height, double width) {
            final Stroke stroke = graphics.getStroke();
            final Color color = graphics.getColor();
            if (this.color != null) {
                graphics.setColor(this.color);
            }
            if (this.stroke != null) {
                graphics.setStroke(this.stroke);
            }
            this.renderObject.draw(graphics, height, width);
            if (this.color != null) {
                graphics.setColor(color);
            }
            if (this.stroke != null) {
                graphics.setStroke(stroke);
            }
        }
    }
}
