/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import static me.cybermaxke.rccar.Framework.getLogger;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;
import me.cybermaxke.rccar.client.driving.render.ROCircle;
import me.cybermaxke.rccar.client.driving.render.ROCircleSection;
import me.cybermaxke.rccar.client.driving.render.ROLine;
import me.cybermaxke.rccar.client.driving.render.ROStyled;
import me.cybermaxke.rccar.client.driving.render.RenderObject;
import me.cybermaxke.rccar.util.Transform;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

public final class PathProcessor {

    public static final class Result {

        private final Path path;
        private final List<RenderObject> renderObjects;

        Result(Path path, List<RenderObject> renderObjects) {
            this.renderObjects = renderObjects;
            this.path = path;
        }

        public Path getPath() {
            return this.path;
        }

        public List<RenderObject> getRenderObjects() {
            return this.renderObjects;
        }
    }

    public static final class ControlResult {

        private final List<RenderObject> renderObjects;
        private final int pathIndex;
        private final double carOffset;
        private final double arcA;
        private final double arcB;
        private final double steerAngle;
        private final boolean required;

        ControlResult(List<RenderObject> renderObjects, int pathIndex, double carOffset, double arcA, double arcB,
                double steerAngle, boolean required) {
            this.renderObjects = renderObjects;
            this.pathIndex = pathIndex;
            this.carOffset = carOffset;
            this.arcA = arcA;
            this.arcB = arcB;
            this.steerAngle = steerAngle;
            this.required = required;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("pathIndex", this.pathIndex)
                    .add("carOffset", this.carOffset)
                    .add("arcA", this.arcA)
                    .add("arcB", this.arcB)
                    .add("steerAngle", this.steerAngle)
                    .add("required", this.required)
                    .toString();
        }

        public List<RenderObject> getRenderObjects() {
            return this.renderObjects;
        }

        public int getPathIndex() {
            return this.pathIndex;
        }

        public double getCarOffset() {
            return this.carOffset;
        }

        public double getArcALength() {
            return this.arcA;
        }

        public double getArcBLength() {
            return this.arcB;
        }

        public double getSteerAngle() {
            return this.steerAngle;
        }

        public boolean isRequired() {
            return this.required;
        }
    }

    private final static class Entry {

        private PathPoint pathPoint;
        @Nullable private RenderObject renderObject;
        private List<RenderObject> extraRenderObjects = new ArrayList<>();

        private Entry(PathPoint pathPoint) {
            this(pathPoint, null);
        }

        private Entry(PathPoint pathPoint, @Nullable RenderObject renderObject) {
            this.renderObject = renderObject;
            this.pathPoint = pathPoint;
        }
    }

    /**
     * Generates the {@link RenderObject}s of the specified {@link Path}. This only
     * generates straight lines.
     *
     * @param path The path
     * @return The render objects
     */
    public List<RenderObject> toRender(Path path) {
        final List<PathPoint> pathPoints = path.getPoints();

        PathPoint a; // The previous point
        PathPoint b = null; // The current point

        final List<RenderObject> renderObjects = new ArrayList<>();

        int index = -1;
        while (++index < pathPoints.size()) {
            a = b;
            b = pathPoints.get(index);
            if (a != null) {
                renderObjects.add(new ROLine(a.getPosition(), b.getPosition()));
            }
        }

        return renderObjects;
    }

    private static Quaterniond fix(Quaterniond q) {
        final Vector3d v = q.getAxesAnglesDeg();
        double x = v.getX();
        double y = v.getY();
        double z = v.getZ();
        if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z)) {
            x = 0;
            y = 0;
            z = 0;
        }
        return Quaterniond.fromAxesAnglesDeg(x, y, z);
    }

    public static Quaterniond quaternion(Vector3d v) {
        return Quaterniond.fromAxesAnglesDeg(v.getX(), v.getY(), v.getZ());
    }

    /**
     * Processes the specified {@link Path} for a
     * specific {@code minimumTurningRange}.
     *
     * @param path The path to process
     * @param minimumTurningRange The minimum turning range
     * @return The processed path
     */
    @SuppressWarnings("ConstantConditions")
    public Result process(Path path, double minimumTurningRange) {
        final List<RenderObject> renderObjects = new ArrayList<>();
        final List<PathPoint> pathPoints = path.getPoints();
        final List<Entry> objects = new ArrayList<>();

        int index = 0;

        PathPoint a = null; // The previous point
        PathPoint b = pathPoints.get(index++); // The current point
        PathPoint c = pathPoints.get(index); // The next point

        if (pathPoints.size() > 1) {
            while (++index <= pathPoints.size()) {
                final boolean flag = a == null;

                a = b;
                b = c;
                c = index < pathPoints.size() ? pathPoints.get(index) : null;

                if (flag) {
                    objects.add(new Entry(a));
                }

                // Add the last point entry
                if (c == null) {
                    final Entry lastPair = objects.get(objects.size() - 1);
                    final PathPoint last = lastPair.pathPoint;
                    objects.add(new Entry(b, new ROLine(last.getPosition(), b.getPosition())));
                    continue;
                }

                final Vector3d aP = a.getPosition().mul(1, 0, 1);
                final Vector3d bP = b.getPosition().mul(1, 0, 1);
                final Vector3d cP = c.getPosition().mul(1, 0, 1);

                // Calculate the path we should follow, turn ABC
                // or lines |AB| & |BC| can also be written as:
                // |lastPointP pointP| & |pointP & nextPointP|

                // A
                // |
                // |
                // |
                // |___________
                // B           C

                // Calculate the direction vectors of |AB| & |BC|
                final Vector3d abVec = bP.sub(aP); // |AB|
                final Vector3d bcVec = cP.sub(bP); // |BC|

                // Calculate the angle between the lines |AB| & |BC|
                // Negate the |AB| vector, to make the 2 vectors it's origin at B
                // Get the angle from the quaternion, the rotation can only be around the y
                // axis, because we cleared the y component of position, so there is no difference
                // in height
                // Absolute to avoid negative angles
                final double angle = Math.abs(diffVectorAngle(abVec.negate(), bcVec));

                // Calculate the distances x and y, to now the center position of the circle relative
                // to the turning point B
                final double x = minimumTurningRange / Math.tan(Math.toRadians(angle / 2.0));
                final double y = minimumTurningRange / Math.sin(Math.toRadians(angle / 2.0));

                // Calculate the normalized direction vectors
                final Vector3d abDir = abVec.normalize().negate(); // Negate this vector to start from the turn point B
                final Vector3d bcDir = bcVec.normalize();

                // Calculate the turn starting point in |AB|
                Vector3d turnStart = bP.add(abDir.mul(x));
                // Calculate the turn end point in |BC|
                final Vector3d turnEnd = bP.add(bcDir.mul(x));
                // Calculate the turning point: Calculate the bisection vector, normalize it and multiply it by y
                final Vector3d turningPoint = bP.add(abDir.add(bcDir).div(2.0).normalize().mul(y));

                // Check if the turn should be combined with the last turn
                final Entry lastPair = objects.get(objects.size() - 1);

                if (lastPair.pathPoint instanceof PathTurn) {
                    final PathTurn lastTurn = (PathTurn) lastPair.pathPoint;

                    // Check if the 2 turns take more space then available on one line
                    final double distance = abVec.length() - lastTurn.getEndPosition().distance(aP) - turnStart.distance(bP);
                    if (distance < 0) {
                        // http://stackoverflow.com/questions/8367512/algorithm-to-detect-if-a-circles-intersect-with-any-other-circle-in-the-same-pla
                        final Vector3d turnDis = turningPoint.sub(lastTurn.getCenter());
                        final double rs = minimumTurningRange + lastTurn.getRange();
                        // Check if the lines are intersecting, when they intersect, it
                        // means that the turn isn't possible, but we can still drive
                        // backwards to make the turn
                        if (turnDis.lengthSquared() <= rs * rs) {
                            throw new RuntimeException("TODO: Intersecting turn circles.");
                        } else {
                            // Calculate the line between the two turn circles
                            final double e = lastTurn.getCenter().distance(turningPoint);
                            // Calculate the angle alpha
                            final double alpha = Math.toDegrees(Math.acos((1.0 / e) * rs));

                            // Create the axis rotation, so that calculation can be done on x/y level,
                            // and rotate the result afterwards
                            final Quaterniond axisRot = Quaterniond.fromRotationTo(Vector3d.UNIT_X, turnDis);

                            // Create the line and check compare 2 possibilities with the one
                            // in the more or less right direction by checking the rotation angle
                            // between them

                            // The first possibility
                            final Vector3d va = Quaterniond.fromAxesAnglesDeg(0, -alpha, 0)
                                    .rotate(new Vector3d(minimumTurningRange, 0, 0));
                            final Vector3d vb = Quaterniond.fromAxesAnglesDeg(0, -alpha + 180.0, 0)
                                    .rotate(new Vector3d(lastTurn.getRange(), 0, 0));

                            final Vector3d ta = lastTurn.getCenter().add(axisRot.rotate(va));
                            final Vector3d tb = turningPoint.add(axisRot.rotate(vb));

                            final Vector3d p = objects.get(objects.size() - 2).pathPoint.getPosition();

                            final ROCircleSection circleSection = (ROCircleSection) lastPair.renderObject;
                            lastPair.pathPoint = new PathPoint(ta, lastPair.pathPoint.getMaxSpeed());
                            final double extent = diffVectorAngle(p.sub(lastTurn.getCenter()), ta.sub(lastTurn.getCenter()));
                            lastPair.renderObject = new ROCircleSection(circleSection.getCenter(),
                                    circleSection.getRadius(), circleSection.getAngle(), extent);

                            final Vector3d add = ta.sub(tb).normalize().mul(100.0);
                            lastPair.extraRenderObjects.add(ROStyled.builder().color(Color.BLUE)
                                    .build(new ROLine(ta.add(add), tb.add(add.negate()))));

                            turnStart = tb;
                        }
                    }
                }

                // Calculate the start angle, it's relative to the unit x vector
                double turnStartAngle = diffVectorAngle(Vector3d.UNIT_X, turnStart.sub(turningPoint));
                // The angle is sometimes NaN for some reason
                if (Double.isNaN(turnStartAngle)) {
                    turnStartAngle = 0.0;
                }
                // Calculate the angle between the start and end rotation point
                double extentAngle = diffVectorAngle(turnStart.sub(turningPoint), turnEnd.sub(turningPoint));
                // The angle is sometimes NaN for some reason
                if (Double.isNaN(extentAngle)) {
                    extentAngle = 0.0;
                }

                // Add the straight path before the turn
                objects.add(new Entry(
                        new PathPoint(turnStart),
                        new ROLine(lastPair.pathPoint.getPosition(), turnStart)));
                // Add the turn
                final Entry entry = new Entry(
                        new PathTurn(turningPoint, turnEnd, minimumTurningRange),
                        new ROCircleSection(turningPoint, minimumTurningRange, turnStartAngle, extentAngle));
                objects.add(entry);
                entry.extraRenderObjects.add(ROStyled.builder().color(Color.ORANGE).build(new ROLine(turnStart, turnEnd)));
            }
        }

        for (Entry entry : objects) {
            renderObjects.addAll(entry.extraRenderObjects);
            if (entry.renderObject != null) {
                renderObjects.add(entry.renderObject);
            }
        }

        return new Result(new Path(objects.stream().map(e -> e.pathPoint).collect(Collectors.toList())), renderObjects);
    }

    @Nullable
    public ControlResult processControlling(Transform transform, Path path,
            double minimumTurningRange, double maxDeviation, double maxAngleDeviation, int pointIndex) {
        final Vector3d rotation = transform.getRotation().negate();
        final List<RenderObject> renderObjects = new ArrayList<>();
        final List<PathPoint> pathPoints = new ArrayList<>(path.getPoints());

        double carOffset = 0;
        double arcA = 0;
        double arcB = 0;
        double steerAngle = 0;
        int curIndex = 0;

        final ListIterator<PathPoint> it = pathPoints.listIterator();
        while (--pointIndex > 0) {
            it.next();
            it.remove();
        }
        PathPoint a = it.next();

        // Find the closest line
        double closestDistance = Double.MAX_VALUE;
        PathPoint a1 = null;
        PathPoint b1 = null;
        int state = 0;

        while (it.hasNext()) {
            final PathPoint b = it.next();

            Vector3d aP = a.getPosition();
            Vector3d bP = b.getPosition();

            Vector3d pos = transform.getPosition();

            boolean success = false;
            boolean angle = false;
            final double dis;

            if (b instanceof PathTurn) {
                //System.out.println("DEBUG");
                final PathTurn t = (PathTurn) b;
                final Vector3d center = t.getCenter();

                dis = minimumTurningRange - center.distance(transform.getPosition());
                final double dis1 = Math.abs(dis);
                //System.out.println("DEBUG A: " + dis1);
                if (dis1 < closestDistance) {
                    //System.out.println("DEBUG A");
                    double angle1 = diffVectorAngle(Vector3d.UNIT_X, pos.sub(center));
                    double angle2 = diffVectorAngle(Vector3d.UNIT_X, a.getPosition().sub(center));
                    double angle3 = diffVectorAngle(Vector3d.UNIT_X, t.getEndPosition().sub(center));
                    while (angle1 < 0) {
                        angle1 += 360;
                    }
                    while (angle2 < 0) {
                        angle2 += 360;
                    }
                    while (angle3 < 0) {
                        angle3 += 360;
                    }/*
                    System.out.println("DEBUG B: " + angle1);
                    System.out.println("DEBUG B: " + angle2);
                    System.out.println("DEBUG B: " + angle3);*/

                    if (angle1 > angle2 - maxAngleDeviation && angle1 < angle3 + maxAngleDeviation) {
                        //System.out.println("DEBUG B");
                        curIndex = it.previousIndex();
                        if (b.getPosition().distance(pos) > maxDeviation) {
                            curIndex--;
                        }
                        closestDistance = dis1;
                        carOffset = dis;
                        success = true;
                        angle = true;
                    }
                }
            } else {
                final Vector3d abVec = bP.sub(aP);
                final Quaterniond rot = fix(Quaterniond.fromRotationTo(abVec, Vector3d.RIGHT));
                pos = rot.rotate(pos.sub(aP));
                dis = Math.abs(pos.getZ());
                //System.out.println(pos.getX() + " >= " + (-(maxDeviation * 3)));
                //System.out.println(pos.getX() + " <= " + (abVec.length() + (maxDeviation * 3)));
                if (dis < closestDistance && pos.getX() >= -(maxDeviation * 3) && pos.getX() <= abVec.length() + (maxDeviation * 3)) {
                    curIndex = it.previousIndex();
                    if (b.getPosition().distance(pos) > maxDeviation) {
                        curIndex--;
                    }
                    closestDistance = dis;
                    carOffset = pos.getZ();
                    success = true;
                    //System.out.println(Math.abs(diffVectorAngle(Quaterniond.fromAxesAnglesDeg(
                    //        rotation.getX(), rotation.getY(), rotation.getZ()).getDirection(), abVec)));
                    //System.out.println(Quaterniond.fromAxesAnglesDeg(
                    //        rotation.getX(), rotation.getY(), rotation.getZ()).invert().getDirection());
                    angle = Math.abs(diffVectorAngle(Quaterniond.fromAxesAnglesDeg(
                            rotation.getX(), rotation.getY(), rotation.getZ()).getDirection(), abVec) - 90) < maxAngleDeviation;
                    //System.out.println(angle);
                }
            }

            if (success) {
                a1 = a;
                b1 = b;
                if (dis < maxDeviation && angle) {
                    System.out.println("onLine");
                    state = 2;
                } else if (!it.hasNext() && transform.getPosition().distance(bP) < maxDeviation) {
                    // The end is reached
                    return null;
                } else if (state == 0) {
                    state = 1;
                }
            }

            a = b;
        }
        renderObjects.add(ROStyled.builder().color(Color.ORANGE).build(new ROCircle(transform.getPosition(), 5)));
        if (state == 0) {
            getLogger().info("The Car left it's path!");
            return null;
        }
        if (state == 1) {
            // System.out.println(b1);
            if (b1 instanceof PathTurn) {
                final PathTurn t = (PathTurn) b1;
                final Vector3d pos = transform.getPosition();
                final Vector3d center = t.getCenter();

                steerAngle = diffVectorAngle(
                        a1.getPosition().sub(center),
                        t.getEndPosition().sub(center));
                steerAngle = steerAngle < 0 ? -1 : 1;

                final double angle = diffVectorAngle(pos.sub(a.getPosition()), pos.sub(t.getEndPosition()));
                arcA = 2 * Math.PI * minimumTurningRange * (angle / 360);
            } else {
                Vector3d aP = a1.getPosition();
                Vector3d bP = b1.getPosition();

                Vector3d abVec = bP.sub(aP);

                Quaterniond rot = fix(Quaterniond.fromRotationTo(abVec, Vector3d.RIGHT));
                Quaterniond invRot = rot.invert();

                Vector3d c = rot.rotate(transform.getPosition().sub(aP));
                final Vector3d hOffset = new Vector3d(c.getX(), 0, 0);
                c = c.sub(hOffset);
                final int inv = c.getZ() < 0 ? 1 : -1;
                Vector3d hP = aP.add(invRot.rotate(hOffset));

                double alpha = Math.toRadians(-rotation.getY() + invRot.getAxesAnglesDeg().getY()) * inv;

                double dx = Math.sin(alpha) * minimumTurningRange;
                double dz = (Math.cos(alpha) * minimumTurningRange) * inv + c.getZ();

                double fz = -minimumTurningRange * inv;
                double fx = dx - Math.sqrt(Math.pow(2 * minimumTurningRange, 2) - Math.pow(dz - fz, 2));

                renderObjects.add(ROStyled.builder().color(Color.ORANGE).build(new ROCircle(transform.getPosition(), 3)));

                dx *= -1;
                fx *= -1;

                final Vector3d d = hP.add(invRot.rotate(new Vector3d(dx, 0, dz)));
                final Vector3d f = hP.add(invRot.rotate(new Vector3d(fx, 0, fz)));
                final Vector3d g = hP.add(invRot.rotate(new Vector3d(fx, 0, 0)));
                final Vector3d e = d.add(f).div(2.0);

                final ROStyled blue = ROStyled.builder().color(Color.BLUE);

                renderObjects.add(blue.build(new ROCircle(d, minimumTurningRange)));
                renderObjects.add(blue.build(new ROCircle(f, minimumTurningRange)));

                Vector3d dc = transform.getPosition().sub(d);
                Vector3d de = e.sub(d);
                Vector3d fe = e.sub(f);
                Vector3d fg = g.sub(f);

                renderObjects.add(ROStyled.builder().color(Color.ORANGE).build(new ROLine(transform.getPosition(),
                        transform.getPosition().add(quaternion(rotation).rotate(Vector3d.UNIT_X).mul(30)))));

                renderObjects.add(new ROCircleSection(f, minimumTurningRange,
                        diffVectorAngle(Vector3d.UNIT_X, fg),
                        diffVectorAngle(fg, fe)));
                renderObjects.add(new ROCircleSection(d, minimumTurningRange,
                        diffVectorAngle(Vector3d.UNIT_X, dc),
                        diffVectorAngle(dc, de)));

                arcA = Math.abs(2 * Math.PI * minimumTurningRange * (diffVectorAngle(fg, fe) / 360));
                arcB = Math.abs(2 * Math.PI * minimumTurningRange * (diffVectorAngle(dc, de) / 360));

                steerAngle = ((double) inv) * -0.5;
            }
        }
        return new ControlResult(renderObjects, curIndex, carOffset, arcA, arcB, steerAngle, state == 1);
    }

    private static double diffVectorAngle(Vector3d a, Vector3d b) {
        return Quaterniond.fromRotationTo(a, b).getAxesAnglesDeg().getY();
    }
}
