/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning.bno055;

import static me.cybermaxke.rccar.Framework.getLogger;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.client.driving.DrivingSystem;
import me.cybermaxke.rccar.client.driving.Path;
import me.cybermaxke.rccar.client.driving.PathPoint;
import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class BNO055PositioningDrivingTestSystem implements PulseableSystemPiece, Command {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH.mm.ss");

    private final DrivingSystem drivingSystem;
    private final BNO055PositioningSystem positioningSystem;

    public BNO055PositioningDrivingTestSystem(DrivingSystem drivingSystem, BNO055PositioningSystem positioningSystem) {
        this.positioningSystem = positioningSystem;
        this.drivingSystem = drivingSystem;
    }

    private boolean driving;
    private long time;

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        if (arguments.length == 0) {
            throw new CommandException("Not enough arguments.");
        }
        if (arguments[0].equalsIgnoreCase("?") ||
                arguments[0].equalsIgnoreCase("help")) {
            getLogger().info("*** Testing System ***");
            getLogger().info("/test-system start");
        }else if (arguments[0].equalsIgnoreCase("start")) {
            if (arguments.length != 1) {
                throw new CommandException("Wrong amount of arguments, usage: /test-system2 start");
            }
            if (this.drivingSystem.hasPath()) {
                getLogger().info("Started the test system");
            }
            final Vector3d r = this.positioningSystem.getRotation().getAxesAnglesDeg();
            this.positioningSystem.setBaseRotation(Quaterniond.fromAxesAnglesDeg(0, r.getY(), 0));
            this.positioningSystem.reset();
            this.drivingSystem.drive(new Path(Arrays.asList(
                    new PathPoint(new Vector3d(0, 0, 0)),
                    new PathPoint(new Vector3d(0, 0, 2))/*,
                    new PathPoint(new Vector3d(3, 0, 3))*/
            )));
            this.driving = true;
            this.time = System.currentTimeMillis();
            getLogger().info("Started the test system");
        } else {
            throw new CommandException("Unknown sub command: " + arguments[0]);
        }
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void pulse(double deltaTime) {
        if (System.currentTimeMillis() - this.time > 8000) {
            this.drivingSystem.cancel();
        }
        if (this.driving && !this.drivingSystem.hasPath()) {
            this.driving = false;
            this.positioningSystem.setBaseRotation(null);
            this.positioningSystem.saveTo(Paths.get("test-system-" + formatter.format(LocalDateTime.now()) + ".xlsx"));
            getLogger().info("The test system 2 finished.");
        }
    }
}
