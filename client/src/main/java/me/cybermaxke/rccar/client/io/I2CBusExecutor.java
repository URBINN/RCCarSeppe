/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io;

import com.pi4j.io.i2c.I2CDevice;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * A common {@link ScheduledExecutorService} that should be used to execute all the tasks for
 * {@link I2CDevice}s.
 */
public final class I2CBusExecutor {

    /**
     * The {@link ScheduledExecutorService} instance.
     */
    public static final ScheduledExecutorService SERVICE = Executors.newSingleThreadScheduledExecutor(
            runnable -> new Thread(runnable, "i2c-bus"));
}
