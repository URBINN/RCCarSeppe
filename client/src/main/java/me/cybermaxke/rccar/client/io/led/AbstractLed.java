/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.led;

import static com.google.common.base.Preconditions.checkNotNull;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import me.cybermaxke.rccar.client.io.AbstractModule;

import javax.annotation.Nullable;

public abstract class AbstractLed<C> extends AbstractModule {

    private C color;

    /**
     * Gets the {@link C} of this LED.
     *
     * @return The color
     */
    public C getColor() {
        return this.color;
    }

    /**
     * Sets the {@link C} of this LED.
     *
     * @param color The color
     */
    public void setColor(C color) {
        this.color = checkNotNull(color, "color");
        applyColor(color);
    }

    protected abstract void applyColor(C color);

    protected static GpioPinPwmOutput provisionSoftPwmOutput(@Nullable String name, String componentName, Pin pin) {
        final GpioController controller = GpioFactory.getInstance();
        if (name == null) {
            return controller.provisionSoftPwmOutputPin(pin);
        } else {
            return controller.provisionSoftPwmOutputPin(pin, name + "_" + componentName);
        }
    }
}
