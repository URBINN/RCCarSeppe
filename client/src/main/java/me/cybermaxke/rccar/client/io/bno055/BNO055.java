/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;

import me.cybermaxke.rccar.util.Axis;

import java.io.Closeable;
import java.io.IOException;

public interface BNO055 extends Closeable {

    /**
     * Gets whether the {@link BNO055} is initialized.
     *
     * @return Is initialized
     */
    boolean isInitialized();

    /**
     * Initializes the {@link BNO055}.
     */
    void initialize();

    /**
     * Sets the {@link BNO055OperationMode} of this sensor.
     *
     * @return The operation mode
     */
    BNO055OperationMode getOperationMode();

    /**
     * Sets the {@link BNO055OperationMode} of this sensor.
     *
     * @param operationMode The operation mode
     */
    void setMode(BNO055OperationMode operationMode) throws IOException;

    boolean getExtCrystalUse();

    void setExtCrystalUse(boolean use) throws IOException;

    /**
     * Reads the euler angle directly from the {@link BNO055}.
     * <p>
     * This data is only available in fusion modes.
     *
     * @return The euler angle data
     * @throws IOException
     */
    Vector3d readEulerAngleData() throws IOException;

    /**
     * Reads the magnetometer data directly from the {@link BNO055}.
     *
     * @return The magnetometer data
     * @throws IOException
     */
    Vector3d readMagnetometerData() throws IOException;

    /**
     * Reads the gyroscope data directly from the {@link BNO055}.
     *
     * @return The gyroscope data
     * @throws IOException
     */
    Vector3d readGyroscopeData() throws IOException;

    /**
     * Reads the gravity directly from the {@link BNO055}.
     * <p>
     * This data is only available in fusion modes.
     *
     * @return The gravity data
     * @throws IOException
     */
    Vector3d readGravityData() throws IOException;

    /**
     * Reads the linear acceleration data directly from the {@link BNO055}.
     * <p>
     * This data is only available in fusion modes.
     *
     * @return The linear acceleration data
     * @throws IOException
     */
    Vector3d readLinearAccelerationData() throws IOException;

    /**
     * Reads the acceleration data directly from the {@link BNO055}.
     *
     * @return The acceleration data
     * @throws IOException
     */
    Vector3d readAccelerationData() throws IOException;

    /**
     * Gets the orientation as a {@link Quaterniond}.
     * <p>
     * This data is only available in fusion modes.
     *
     * @return The quaternion
     * @throws IOException
     */
    Quaterniond readQuaternionData() throws IOException;

    /**
     * Gets the current {@link BNO055CalibrationStatus}.
     *
     * @return The calibration status
     * @throws IOException
     */
    BNO055CalibrationStatus readCalibrationStatus() throws IOException;

    boolean isCalibrated() throws IOException;

    /**
     * Gets the current {@link BNO055SystemStatus} info.
     *
     * @return The sys status info
     * @throws IOException
     */
    BNO055SystemStatus readSystemStatus() throws IOException;

    /**
     * Gets the {@link BNO055VersionInfo}.
     *
     * @return The version info
     * @throws IOException
     */
    BNO055VersionInfo readVersionInfo() throws IOException;

    byte[] readCalibrationData() throws IOException;

    void writeCalibrationData(byte[] calibrationData) throws IOException;

    /**
     * Gets the sensors internal temperature.
     *
     * @return Temperature in degrees celsius.
     * @throws IOException
     */
    int readTemperature() throws IOException;

    /**
     * Sets the radius of the accelerometer.
     * <p>
     * The maximum range of the radius is +/- 1000.
     *
     * @param radius The radius
     * @throws IOException
     */
    void setAccelerometerRadius(int radius) throws IOException;

    /**
     * Sets the radius of the magnetometer.
     * <p>
     * The maximum range of the radius is +/- 960.
     *
     * @param radius The radius
     * @throws IOException
     */
    void setMagnetometerRadius(int radius) throws IOException;

    /**
     * Remaps all the axes. The same {@link Axis} may not be used multiple times.
     *
     * @param remappedXAxis The new axis the original x axis should be remapped to
     * @param remappedYAxis The new axis the original y axis should be remapped to
     * @param remappedZAxis The new axis the original z axis should be remapped to
     * @throws IOException
     */
    void remapAxes(Axis remappedXAxis, Axis remappedYAxis, Axis remappedZAxis) throws IOException;
}
