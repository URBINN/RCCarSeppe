/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import com.flowpowered.math.vector.Vector3d;

import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;

public class ROPoint implements RenderObject {

    private final Vector3d point;

    public ROPoint(Vector3d point) {
        this.point = point;
    }

    @Override
    public void draw(Graphics2D graphics, double height, double width) {
        graphics.draw(new RoundRectangle2D.Double(
                this.point.getX() - 0.5, width - (this.point.getY() - 0.5), 1.0, 1.0, 0.5, 0.5));
    }
}
