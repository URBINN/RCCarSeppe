/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

public final class DebugEngine extends AbstractEngine {

    private final CarSpecifications specifications = new CarSpecifications(
            30, 12, 18, 40);

    @Override
    public CarSpecifications getSpecifications() {
        return this.specifications;
    }

    @Override
    public void setSteeringUnits(int steering) {
    }

    @Override
    public void setSpeedUnits(int speed) {
    }

    @Override
    public void init() {
    }
}
