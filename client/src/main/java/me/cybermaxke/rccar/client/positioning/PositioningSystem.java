/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.sys.SystemPiece;

/**
 * Represents the positioning system, which will provide the position of the car.
 */
public interface PositioningSystem extends SystemPiece {

    /**
     * Gets the position as a {@link Vector3d},
     * each component is in m.
     *
     * @return The position
     */
    Vector3d getPosition();

    /**
     * Gets the velocity as a {@link Vector3d},
     * each component is in m/s.
     *
     * @return The velocity
     */
    Vector3d getVelocity();

    /**
     * Gets the speed in m/s.
     *
     * @return The speed
     */
    default double getSpeed() {
        return getVelocity().length();
    }
}
