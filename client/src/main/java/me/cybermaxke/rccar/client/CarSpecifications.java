/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import com.google.common.base.MoreObjects;

/**
 * Represents the specifications of the car, this are the values that will be
 * used to calculate the turning circle of the car.
 *
 * A: Shaft distance, this is the distance between the two shafts
 *      {@link #getShaftDistance()}
 * B: Wheel distance, this is the distance between the two wheels
 *      {@link #getWheelDistance()}
 * C: Distance from the back of the car to the the turning point (+)
 *
 *         A
 *      -------
 *      0_____O
 * B |  |__+__|>    <-- the front side
 *      O     O
 *      ---
 *       C
 *
 * Other example:
 *
 *           B
 *         -----
 *
 *         |---|   |
 *           |     |
 *     |     +     | A
 *  C  |     |     |
 *     |   |---|   |
 *
 * There is also the maximum angle that the car turn left or right:
 *      {@link #getMaxWheelTurnAngle()}
 *
 * center:
 *
 *       |---|
 *         |
 *         |
 *         |
 *       |---|
 *
 * right:
 *
 *       /---/
 *         |
 *         |
 *         |
 *       |---|
 *
 * left:
 *
 *       \---\
 *         |
 *         |
 *         |
 *       |---|
 */
public final class CarSpecifications {

    private final double shaftDistance;
    private final double wheelDistance;
    private final double turningPointDistance;
    private final double maxWheelTurnAngle;

    /**
     * Creates a new instance of {@link CarSpecifications}.
     *
     * @param shaftDistance The shaft distance, in meters
     * @param wheelDistance The wheel distance, in meters
     * @param turningPointDistance The turning point distance, in meters
     * @param maxWheelTurnAngle The maximum angle the car wheels can turn left or right, in degrees
     */
    public CarSpecifications(
            double shaftDistance,
            double wheelDistance,
            double turningPointDistance,
            double maxWheelTurnAngle) {
        this.turningPointDistance = turningPointDistance;
        this.shaftDistance = shaftDistance;
        this.wheelDistance = wheelDistance;
        this.maxWheelTurnAngle = maxWheelTurnAngle;
    }

    /**
     * Gets the distance between the wheel shafts of the car.
     *
     * @return The shaft distance, in meters
     */
    public double getShaftDistance() {
        return this.shaftDistance;
    }

    /**
     * Gets the distance between the two wheels of the car.
     *
     * @return The wheel distance, in meters
     */
    public double getWheelDistance() {
        return this.wheelDistance;
    }

    /**
     * Gets the distance from the back of the car
     * to the the turning point.
     *
     * @return The turning point distance, in meters
     */
    public double getTurningPointDistance() {
        return this.turningPointDistance;
    }

    /**
     * Gets the maximum angle the car wheels
     * can turn left or right.
     *
     * @return The maximum wheel angle, in degrees
     */
    public double getMaxWheelTurnAngle() {
        return this.maxWheelTurnAngle;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("shaftDistance", this.shaftDistance)
                .add("wheelDistance", this.wheelDistance)
                .add("turningPointDistance", this.turningPointDistance)
                .add("maxWheelTurnAngle", this.maxWheelTurnAngle)
                .toString();
    }
}
