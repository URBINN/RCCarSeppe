/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

/**
 * Represents the engine of the client {@link Car}. This class will control the
 * {@link GpioController} outputs of the car for the steering and driving.
 * This engine is designed for a RC Car.
 *
 * The following {@link Pin}s will be assigned:
 * {@link RaspiPin#GPIO_23} or BCM 13: The motor
 * {@link RaspiPin#GPIO_26} or BCM 12: The steering servo
 * {@link RaspiPin#GPIO_00} or BCM 17: The connection LED
 */
public final class RCCarEngine extends AbstractEngine implements PulseableSystemPiece {

    // http://raspberrypi.stackexchange.com/questions/4906/control-hardware-pwm-frequency

    // 55 Hz = 19.2e6 Hz / (clock * range)
    // clock * range = 19.2e6 Hz / 55 Hz
    // period time = 18,376 ms
    // resolution = 0,01 ms -> range = 18,38 / 0,01 = 1838
    // resolution = 0,005 ms -> range = 18,375 / 0,005 = 3675
    // clock = 19.2e6 Hz / (55 Hz * 3675) = 19.2e6 / 202125 = 94,99 = 95

    // 54,42 Hz = 19.2e6 Hz / (clock * range)
    // clock * range = 19.2e6 Hz / 54,42 Hz
    // period time = 18,376 ms
    // Get the best resolution that will fit into 4096
    // resolution = 0,01 ms -> range = 18,38 / 0,01 = 1838
    // resolution = 0,005 ms -> range = 18,375 / 0,005 = 3675
    // clock = 19.2e6 Hz / (54,42 Hz * 3675) = 19.2e6 / 199993,5 = 96,0031 = 96

    // 54,42 Hz = 19.2e6 Hz / (clock * range)
    // clock * range = 19.2e6 Hz / 54,42 Hz
    // period time = 18,376 ms
    // clock = 19.2e6 Hz / (54,42 Hz * 4096) = 19.2e6 / 222904,32 = 86,1356 = 86
    // range = 19.2e6 Hz / (54,42 Hz * 86) = 19.2e6 / 4680,12 = 3950,487 = 3950
    // clock = 19.2e6 Hz / (54,42 Hz * 3950) = 19.2e6 / 214959 = 89,32 = 89
    // range = 19.2e6 Hz / (54,42 Hz * 89) = 19.2e6 / 4843,38 = 3964,17 = 3964
    // clock = 19.2e6 Hz / (54,42 Hz * 3964) = 19.2e6 / 215720,88 = 89,0039 = 89
    // resolution = 0,01 ms -> range = 18,38 / 3964 = 0,0046367

    /**
     * The maximum range of the PWM.
     */
    private static final int RANGE = 3675;

    /**
     * The clock value.
     */
    private static final int CLOCK = 96;

    // TODO: Calculate the maximum left/right steering direction
    // TODO: Calculate with actual speed [m/s]

    private final CarSpecifications carSpecifications = new CarSpecifications(
            0.258,0.12, 0.178, 20.0);

    private GpioPinPwmOutput motor;
    private GpioPinPwmOutput steering;
    private GpioPinDigitalOutput connectionLED;

    @Override
    public void init() {
        try {
            init0();
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void init0() throws ExecutionException, InterruptedException {
        if (this.motor != null) {
            return;
        }
        final GpioController controller = GpioFactory.getInstance();
        this.motor = controller.provisionPwmOutputPin(RaspiPin.GPIO_23, "motor");
        this.motor.setPwmRange(RANGE);
        this.motor.setShutdownOptions(true);
        this.steering = controller.provisionPwmOutputPin(RaspiPin.GPIO_26, "steering");
        this.steering.setPwmRange(RANGE);
        this.steering.setShutdownOptions(true);
        this.connectionLED = controller.provisionDigitalOutputPin(RaspiPin.GPIO_00, "connection_led");
        this.connectionLED.setShutdownOptions(true);
        com.pi4j.wiringpi.Gpio.pwmSetMode(com.pi4j.wiringpi.Gpio.PWM_MODE_MS);
        com.pi4j.wiringpi.Gpio.pwmSetRange(RANGE);
        com.pi4j.wiringpi.Gpio.pwmSetClock(CLOCK);
        this.steering.setPwm(0);
        // Calibrate the motor controller
        try {
            this.motor.setPwm(0);
            Thread.sleep(20);
            this.motor.setPwm(300);
            Thread.sleep(3000);
            this.motor.setPwm(315);
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void cleanup() {
        if (this.motor == null) {
            return;
        }
        final GpioController controller = GpioFactory.getInstance();
        controller.unprovisionPin(this.motor, this.steering, this.connectionLED);
    }

    @Override
    public CarSpecifications getSpecifications() {
        return this.carSpecifications;
    }

    @Override
    public void setSteering(double steering) {
        super.setSteering(steering);
    }

    @Override
    public void setSteeringUnits(int steering) {
        this.steerUnits = steering;
        if (steering != 0) {
            this.steering.setPwm(steering);
        }
    }

    @Override
    public void setSpeedUnits(int speed) {
        this.speedUnits = speed;
        this.motor.setPwm(speed);
    }

    @Override
    public void setConnectionStatus(boolean status) {
        this.connectionLED.setState(status);
    }

    private int speedUnits;
    private int steerUnits;

    private static final int STEP = 100;

    @Override
    public void pulse(double deltaTime) {
        // Allow a variable speed, smaller then the smallest duty cycle
        double speed = getSpeed();
        if (this.speedUnits == 0) {
            final int speed1 = (int) (speed * (double) STEP);
            if (System.currentTimeMillis() % STEP < speed1) {
                this.motor.setPwm(speed > 0 ? 315 : 285);
            } else {
                this.motor.setPwm(300);
            }
        }
        // Make sure that the wheel stays in the middle
        if (this.steerUnits == 0) {
            final double steer = getSteering();
            final int base;
            if (System.currentTimeMillis() % 1000 < 450) {
                base = 294;
            } else {
                base = 299;
            }
            this.steering.setPwm(base + (int) Math.round(steer * 70f));
        }
    }

    @Override
    public Duration getPulseInterval() {
        return Duration.ofMillis(5);
    }
}
