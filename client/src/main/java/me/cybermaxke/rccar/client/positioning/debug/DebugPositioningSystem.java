/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning.debug;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.client.positioning.PositioningSystem;

public final class DebugPositioningSystem implements PositioningSystem {

    @Override
    public Vector3d getPosition() {
        return Vector3d.ZERO;
    }

    @Override
    public Vector3d getVelocity() {
        return Vector3d.ZERO;
    }

    @Override
    public double getSpeed() {
        return 0;
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
    }
}
