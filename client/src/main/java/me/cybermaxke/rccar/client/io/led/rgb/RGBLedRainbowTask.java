/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.led.rgb;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.awt.Color;
import java.time.Duration;

/**
 * A task that plays rainbow color animation on the target {@link RGBLed}.
 * <p>
 * The colors will loop through HSB (HSL) color scheme, where the hue component
 * (H) will be modified over time. The saturation component (S) and the brightness
 * component (B) can be modified. The same goes for the {@link Duration} that
 * one rainbow cycle should take.
 *
 * @see <a href="http://hslpicker.com">HSL Color Picker</a>
 */
public final class RGBLedRainbowTask implements Runnable {

    private static final int HUE_COLORS = 360;
    private static final float HUE_STEP = 1.0f / (float) HUE_COLORS;

    private final RGBLed rgbLed;

    private Duration duration;

    private float saturation = 1.0f;
    private float brightness = 1.0f;

    /**
     * The delay in nano seconds before the
     * next hue value.
     */
    private int delayMillis;
    private int delayNanos;

    /**
     * Creates a new {@link RGBLedRainbowTask}.
     *
     * @param rgbLed The rgb led
     * @param duration The duration of the rainbow cycle
     */
    public RGBLedRainbowTask(RGBLed rgbLed, Duration duration) {
        this.rgbLed = checkNotNull(rgbLed, "rgbLed");
        setDuration(duration);
    }

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        try {
            float hue = 0.0f;
            while (true) {
                hue += HUE_STEP;
                if (Math.abs(hue - 1.0f) < 0.00001f) { // Check if this value reached 1.0
                    hue = 0;
                }
                this.rgbLed.setColor(Color.getHSBColor(hue, this.saturation, this.brightness));
                Thread.sleep(this.delayMillis, this.delayNanos);
            }
        } catch (InterruptedException ignored) {
        }
    }

    /**
     * Gets the saturation of the rainbow colors.
     *
     * @return The saturation
     */
    public double getSaturation() {
        return this.saturation;
    }

    /**
     * Sets the saturation of the rainbow colors.
     *
     * @param saturation The saturation
     */
    public void setSaturation(double saturation) {
        checkArgument(saturation >= 0.0 && saturation <= 1.0, "saturation must scale between 0.0 and 1.0");
        this.saturation = (float) saturation;
    }

    /**
     * Gets the brightness of the rainbow colors.
     *
     * @return The brightness
     */
    public double getBrightness() {
        return this.brightness;
    }

    /**
     * Sets the brightness of the rainbow colors.
     *
     * @param brightness The brightness
     */
    public void setBrightness(double brightness) {
        checkArgument(brightness >= 0.0 && brightness <= 1.0, "brightness must scale between 0.0 and 1.0");
        this.brightness = (float) brightness;
    }

    /**
     * Gets the {@link Duration} that it takes to loop through
     * the whole rainbow cycle.
     *
     * @return The duration
     */
    public Duration getDuration() {
        return this.duration;
    }

    /**
     * Sets the {@link Duration} that it takes to loop through
     * the whole rainbow cycle.
     *
     * @param duration The duration
     */
    public void setDuration(Duration duration) {
        this.duration = checkNotNull(duration, "duration");
        final Duration delayDuration = duration.dividedBy(HUE_COLORS);
        final int nano = delayDuration.getNano();
        this.delayMillis = nano / 1000000;
        this.delayNanos = nano % 1000000;
    }
}
