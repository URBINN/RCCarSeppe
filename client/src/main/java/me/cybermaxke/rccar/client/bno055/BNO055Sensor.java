/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.bno055;

import static com.google.common.base.Preconditions.checkNotNull;
import static me.cybermaxke.rccar.Framework.getLogger;

import com.google.common.base.Throwables;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import me.cybermaxke.rccar.client.Client;
import me.cybermaxke.rccar.client.io.I2CBusExecutor;
import me.cybermaxke.rccar.client.io.bno055.BNO055;
import me.cybermaxke.rccar.client.io.bno055.BNO055CalibrationStatus;
import me.cybermaxke.rccar.client.io.bno055.PiBNO055;
import me.cybermaxke.rccar.command.CommandException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.annotation.Nullable;

public final class BNO055Sensor {

    enum OperationMode {
        /**
         * Normal operation mode, every submitted task will be updated.
         */
        WORK,
        /**
         * The sensor is being calibrated, all other actions are delayed.
         */
        CALIBRATE,
        /**
         * The sensor data is being logged, all other actions are delayed.
         */
        LOGGING,
        ;
    }

    public static final BNO055Sensor INSTANCE = new BNO055Sensor();

    private final Path bno055CalibrationDataFile = Paths.get("rccar_bno055_calibration_data.dat");
    @Nullable private BNO055 bno055;
    @Nullable private volatile OperationMode operationMode;
    @Nullable private byte[] calibrationData;

    private BNO055Sensor() {
    }

    public ScheduledFuture<?> submit(Consumer<BNO055> consumer, Duration delay, Duration interval) {
        checkNotNull(consumer, "consumer");
        checkNotNull(interval, "interval");
        try {
            init();
        } catch (ExecutionException | InterruptedException e) {
            throw Throwables.propagate(e);
        }
        return I2CBusExecutor.SERVICE.scheduleAtFixedRate(() -> {
            if (this.operationMode == OperationMode.WORK) {
                consumer.accept(this.bno055);
            }
        }, delay.toMillis(), interval.toMillis(), TimeUnit.MILLISECONDS);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void init() throws ExecutionException, InterruptedException {
        if (this.operationMode != null) {
            return;
        }
        if (Files.exists(this.bno055CalibrationDataFile)) {
            try (DataInputStream dis = new DataInputStream(Files.newInputStream(this.bno055CalibrationDataFile))) {
                this.calibrationData = new byte[18];
                dis.read(this.calibrationData);
            } catch (IOException e) {
                throw new RuntimeException("Unable to read tha calibration data.", e);
            }
        }
        // Initialize the bno055 on the i2c bus task executor,
        // wait for the initialization to complete
        I2CBusExecutor.SERVICE.submit(this::init0).get();
        this.operationMode = OperationMode.WORK;
    }

    private void init0() {
        try {
            this.bno055 = new PiBNO055(I2CFactory.getInstance(I2CBus.BUS_1), PiBNO055.BNO055_ADDRESS_A);
            this.bno055.initialize();
            this.bno055.setExtCrystalUse(true);
            if (this.calibrationData != null) {
                this.bno055.writeCalibrationData(this.calibrationData);
                getLogger().info("BNO055 Sensor: Loaded calibration data.");
            } else if (!Client.NO_BNO055_CALIBRATION) {
                startCalibration0();
            }
        } catch (IOException | I2CFactory.UnsupportedBusNumberException e) {
            throw Throwables.propagate(e);
        }
    }

    void startCalibration() throws CommandException {
        try {
            init();
        } catch (ExecutionException | InterruptedException e) {
            throw Throwables.propagate(e);
        }
        if (this.operationMode == OperationMode.LOGGING) {
            throw new CommandException("The sensor is currently being logged, finish or cancel this first.");
        } else if (this.operationMode == OperationMode.CALIBRATE) {
            throw new CommandException("The sensor is already being calibrated.");
        }
        startCalibration0();
    }

    void saveCalibrationData() throws CommandException {
        if (this.operationMode == OperationMode.LOGGING) {
            throw new CommandException("The sensor is currently being logged, finish or cancel this first.");
        } else if (this.operationMode == OperationMode.CALIBRATE) {
            throw new CommandException("The sensor is currently being calibrated, finish this first.");
        }
        saveCalibrationData0();
    }

    private void saveCalibrationData0() {
        try {
            init();
            this.operationMode = OperationMode.CALIBRATE;
            I2CBusExecutor.SERVICE.submit(() -> {
                final byte[] calibrationData1;
                try {
                    //noinspection ConstantConditions
                    calibrationData1 = this.bno055.readCalibrationData();
                    this.calibrationData = calibrationData1;
                    final Path parent = this.bno055CalibrationDataFile.getParent();
                    if (parent != null && !Files.exists(parent)) {
                        Files.createDirectories(parent);
                    }
                    try (DataOutputStream dos = new DataOutputStream(Files.newOutputStream(this.bno055CalibrationDataFile))) {
                        dos.write(calibrationData1);
                        dos.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                this.operationMode = OperationMode.WORK;
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            throw Throwables.propagate(e);
        }
    }

    private void startCalibration0() {
        final Calibrator calibrator = new Calibrator();
        this.operationMode = OperationMode.CALIBRATE;
        calibrator.scheduledFuture = I2CBusExecutor.SERVICE.scheduleAtFixedRate(calibrator::calibrate, 20, 500, TimeUnit.MILLISECONDS);
    }

    private class Calibrator {

        private ScheduledFuture<?> scheduledFuture;

        private boolean accelerometer;
        private boolean magnetometer;
        private boolean gyroscope;

        void calibrate() {
            try {
                //noinspection ConstantConditions
                final BNO055CalibrationStatus status = bno055.readCalibrationStatus();
                getLogger().info("BNO055 Sensor: Calibration status -> Accelerometer: {}; Magnetometer: {}; Gyroscope: {}",
                        status.getAccelerometerState(), status.getMagnetometerState(), status.getGyroscopeState());
                if (!this.accelerometer && status.getAccelerometerState() == 3) {
                    this.accelerometer = true;
                    getLogger().info("BNO055 Sensor: Finished calibrating the accelerometer.");
                }
                if (!this.magnetometer && status.getMagnetometerState() == 3) {
                    this.magnetometer = true;
                    getLogger().info("BNO055 Sensor: Finished calibrating the magnetometer.");
                }
                if (!this.gyroscope && status.getGyroscopeState() == 3) {
                    this.gyroscope = true;
                    getLogger().info("BNO055 Sensor: Finished calibrating the gyroscope.");
                }
                final boolean calibrated = this.accelerometer && this.magnetometer && this.gyroscope;
                if (calibrated) {
                    getLogger().info("BNO055 Sensor: Finished calibrating.");
                    operationMode = OperationMode.WORK;
                    this.scheduledFuture.cancel(false);
                }
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        }
    }
}
