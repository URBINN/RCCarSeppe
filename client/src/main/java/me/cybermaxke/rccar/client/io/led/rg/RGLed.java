/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.led.rg;

import static com.google.common.base.Preconditions.checkNotNull;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import me.cybermaxke.rccar.client.io.led.AbstractLed;

import java.awt.Color;

import javax.annotation.Nullable;

/**
 * Represents a colorable (rg) LED, each component red and green
 * is presented by a {@link Pin} that will be controlled by a soft pwm.
 *
 * For example:
 *  KY-011 (2-Color - Red + Green - 5mm LED module) from SensorKit X40:
 *    Vf [Red, Green] = 2,0 - 2,5 V
 *    If = 20 mA
 *    -> 3.3 V Output:
 *         Rf [Red]   = 120 ohm
 *         Rf [Green] = 120 ohm
 *    Pins:
 *      1. GND
 *      2. Red
 *      3. Green
 */
public final class RGLed extends AbstractLed<RGColor> {

    private final GpioPinPwmOutput red;
    private final GpioPinPwmOutput green;

    /**
     * Creates a new {@link RGLed} for the specified {@link Pin}s
     * and with a specific name.
     *
     * @param redPin The pin for the red component
     * @param greenPin The pin for the green component
     * @param name The name of this LED
     */
    public RGLed(Pin redPin, Pin greenPin, String name) {
        this(checkName(name), redPin, greenPin);
    }

    /**
     * Creates a new {@link RGLed} for the specified {@link Pin}s.
     *
     * @param redPin The pin for the red component
     * @param greenPin The pin for the green component
     */
    public RGLed(Pin redPin, Pin greenPin) {
        this(null, redPin, greenPin);
    }

    private RGLed(@Nullable String name, Pin redPin, Pin greenPin) {
        checkNotNull(redPin, "redPin");
        checkNotNull(greenPin, "greenPin");
        this.red = provisionSoftPwmOutput(name, "red", redPin);
        this.red.setShutdownOptions(true);
        this.red.setPwmRange(255);
        this.green = provisionSoftPwmOutput(name, "green", greenPin);
        this.green.setShutdownOptions(true);
        this.green.setPwmRange(255);
        setColor(Color.RED);
    }

    /**
     * Sets the {@link Color} of this LED. The blue component
     * will be ignored.
     *
     * @param color The color
     */
    public void setColor(Color color) {
        setColor(RGColor.ofColor(color));
    }

    @Override
    protected void applyColor(RGColor color) {
        this.red.setPwm(color.getRed());
        this.green.setPwm(color.getGreen());
    }

    @Override
    public void close() {
        final GpioController controller = GpioFactory.getInstance();
        controller.unprovisionPin(this.red, this.green);
    }
}
