/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning.bno055;

import static com.google.common.base.Preconditions.checkNotNull;
import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.client.Engine;
import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * A system to test the {@link BNO055PositioningSystem}, the car will
 * drive for a specific amount of seconds forward. Before the car starts
 * driving gets all the sensor data reset and when the car finishes driving
 * will all the data be saved (acceleration, velocity, displacement and rotation).
 *
 * Usage: /test-system start <duration> <speed>
 */
public class BNO055PositioningTestSystem implements PulseableSystemPiece, Command {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH.mm.ss");

    private final Engine engine;
    private final BNO055PositioningSystem positioningSystem;

    private long endTime = -1L;
    private long startTime = -1L;
    private double speed;

    public BNO055PositioningTestSystem(Engine engine, BNO055PositioningSystem positioningSystem) {
        checkNotNull(positioningSystem, "positioningSystem");
        checkNotNull(engine, "engine");
        this.positioningSystem = positioningSystem;
        this.engine = engine;
    }

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        if (arguments.length == 0) {
            throw new CommandException("Not enough arguments.");
        }
        if (arguments[0].equalsIgnoreCase("?") ||
                arguments[0].equalsIgnoreCase("help")) {
            getLogger().info("*** BNO055 Positioning Test System ***");
            getLogger().info("/{} start <duration> <speed>", commandName);
        } else if (arguments[0].equalsIgnoreCase("start")) {
            if (arguments.length != 3) {
                throw new CommandException("Wrong amount of arguments, usage: /%s start <duration> <speed>", commandName);
            }
            if (this.endTime != -1) {
                throw new CommandException("The test system is already running.");
            }
            int duration;
            try {
                duration = Integer.parseInt(arguments[1]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid duration value: %s", arguments[1]);
            }
            double speed;
            try {
                speed = Double.parseDouble(arguments[2]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid speed value: %s", arguments[2]);
            }
            start(duration * 1000L, speed);
            getLogger().info("Started the test system");
        } else {
            throw new CommandException("Unknown sub command: %s", arguments[0]);
        }
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void pulse(double deltaTime) {
        if (this.endTime == -1) {
            return;
        }
        final long time = System.currentTimeMillis();
        if (time > this.endTime - 3000) {
            if (time > this.endTime) {
                this.positioningSystem.saveTo(Paths.get("test-system-" + formatter.format(LocalDateTime.now()) + ".xlsx"));
                this.endTime = -1;
                getLogger().info("The test system finished: test-system-" + formatter.format(LocalDateTime.now()) + ".xlsx");
            }
            this.engine.setSpeed(0f);
        } else if (time > this.startTime + 2000) {
            this.engine.setSpeed(this.speed);
        } else {
            this.engine.setSpeed(0f);
        }
        this.engine.setSteering(0f);
    }

    /**
     * Starts the test system with the specified parameters.
     *
     * @param duration The duration that the car should drive in milliseconds
     * @param speed The speed of the car, scales between -1 and 1, where -1
     *              means driving backwards, 1 forward and 0 is stationary
     */
    private void start(long duration, double speed) {
        this.startTime = System.currentTimeMillis();
        this.positioningSystem.reset();
        this.endTime = this.startTime + duration + 5000;
        this.speed = speed;
    }
}
