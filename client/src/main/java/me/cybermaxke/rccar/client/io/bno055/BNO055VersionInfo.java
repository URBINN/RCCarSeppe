/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import com.google.common.base.MoreObjects;

public final class BNO055VersionInfo {

    private final int bootloaderVersion;
    private final int accelerometerVersion;
    private final int magnetometerVersion;
    private final int gyroscopeVersion;

    BNO055VersionInfo(int bootloaderVersion, int accelerometerVersion, int magnetometerVersion, int gyroscopeVersion) {
        this.bootloaderVersion = bootloaderVersion;
        this.accelerometerVersion = accelerometerVersion;
        this.magnetometerVersion = magnetometerVersion;
        this.gyroscopeVersion = gyroscopeVersion;
    }

    public int getBootloaderVersion() {
        return this.bootloaderVersion;
    }

    public int getAccelerometerVersion() {
        return this.accelerometerVersion;
    }

    public int getMagnetometerVersion() {
        return this.magnetometerVersion;
    }

    public int getGyroscopeVersion() {
        return this.gyroscopeVersion;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("accelerometerVersion", this.accelerometerVersion)
                .add("magnetometerVersion", this.magnetometerVersion)
                .add("gyroscopeVersion", this.gyroscopeVersion)
                .add("bootloaderVersion", this.bootloaderVersion)
                .toString();
    }
}
