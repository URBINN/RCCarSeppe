/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a partial circle, a bow.
 */
public final class ROCircleSection implements RenderObject {

    private final Vector3d center;
    private final double radius;

    private final double angle;
    private final double extent;

    public ROCircleSection(Vector3d center, double radius, double angle, double extent) {
        checkArgument(radius > 0, "The range must be greater then 0");
        checkNotNull(center, "center");
        this.radius = radius;
        this.center = center;
        this.angle = angle;
        this.extent = extent;
    }

    /**
     * Gets the radius.
     *
     * @return The radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Gets the center {@link Vector3d}.
     *
     * @return The center
     */
    public Vector3d getCenter() {
        return this.center;
    }

    public double getAngle() {
        return this.angle;
    }

    public double getExtent() {
        return this.extent;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("center", this.center)
                .add("radius", this.radius)
                .add("angle", this.angle)
                .add("extent", this.extent)
                .toString();
    }

    @Override
    public void draw(Graphics2D graphics, double height, double width) {
        final double diameter = this.radius * 2.0;
        graphics.draw(new Arc2D.Double(this.center.getX() - this.radius, height - (this.center.getZ() + this.radius),
                diameter, diameter, -this.angle, -this.extent, Arc2D.OPEN));
    }
}
