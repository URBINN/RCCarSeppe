/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import java.awt.Graphics2D;

public interface RenderObject {

    void draw(Graphics2D graphics, double height, double width);
}
