/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Throwables;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.DoubleConsumer;

import javax.annotation.Nullable;

/**
 * A {@link Runnable} that handles the I/O of a ultrasonic sensor
 * to measure the distance. This task is supposed to be reused and
 * when finished is a {@link #close()} expected to release the bound
 * {@link Pin}s.
 * <p>
 * This {@link Runnable} should never be run in the main {@link Thread}
 * of the application as it blocks while the measurement.
 * <p>
 * For example:
 *  KY-050 (Ultrasonic Sensor) from SensorKit X40
 */
public final class UltrasonicSensor extends AbstractModule {

    private final GpioPinDigitalOutput trigger;
    private final GpioPinDigitalInput echo;

    private final Lock lock = new ReentrantLock();
    private final Condition lockCondition = this.lock.newCondition();

    private volatile PinState echoPinState;

    /**
     * Creates a new {@link UltrasonicSensor} with the specified {@link Pin}s and name.
     *
     * @param triggerPin The trigger pin (output)
     * @param echoPin The echo pin (input)
     * @param name The base name for the gpio inputs/outputs
     */
    public UltrasonicSensor(Pin triggerPin, Pin echoPin, String name) {
        this(checkName(name), triggerPin, echoPin);
    }

    /**
     * Creates a new {@link UltrasonicSensor} with the specified {@link Pin}s.
     *
     * @param triggerPin The trigger pin (output)
     * @param echoPin The echo pin (input)
     */
    public UltrasonicSensor(Pin triggerPin, Pin echoPin) {
        this(null, triggerPin, echoPin);
    }

    private UltrasonicSensor(@Nullable String name, Pin triggerPin, Pin echoPin) {
        checkNotNull(triggerPin, "triggerPin");
        checkNotNull(echoPin, "echoPin");
        final GpioController controller = GpioFactory.getInstance();
        if (name == null) {
            this.trigger = controller.provisionDigitalOutputPin(triggerPin);
        } else {
            this.trigger = controller.provisionDigitalOutputPin(triggerPin, name + "_trigger");
        }
        this.trigger.setState(false);
        if (name == null) {
            this.echo = controller.provisionDigitalInputPin(echoPin);
        } else {
            this.echo = controller.provisionDigitalInputPin(echoPin, name + "_echo");
        }
        // Add the GpioPinListenerDigital, this will be called when the input
        // signal gets changed. This is where we will notify the measurement
        // thread that the state has changed.
        this.echo.addListener((GpioPinListenerDigital) event -> {
            this.echoPinState = event.getState();
            // Try to acquire the lock, we don't want to freeze the I/O
            // threads. If it failed the other thread isn't waiting,
            // so we don't have anything to signal
            if (this.lock.tryLock()) {
                try {
                    this.lockCondition.signalAll();
                } finally {
                    this.lock.unlock();
                }
            }
        });
        this.echoPinState = this.echo.getState();
    }

    /**
     * Triggers the {@link UltrasonicSensor} to measure the
     * distance. The read distance is in meters.
     * <p>
     * This method will block the current {@link Thread} until the
     * measuring is complete.
     *
     * @return The distance
     */
    public double readDistance() {
        this.lock.lock();
        try {
            // Trigger the sensor
            this.trigger.setState(true);
            // Wait 1 ms to make sure that the output gets the signal
            Thread.sleep(1);
            // Reset the trigger
            this.trigger.setState(false);

            // Wait until the pin state is low
            // Waiting for conditions should be done in a while loop,
            // check the condition class for more info
            while (this.echoPinState == PinState.LOW) {
                this.lockCondition.await();
            }
            // The sensor started send a sound signal out at this point
            final long startTime = System.nanoTime();
            // Wait until the pin state is high
            while (this.echoPinState == PinState.HIGH) {
                this.lockCondition.await();
            }
            // We got a response, we got the distance
            final long endTime = System.nanoTime();

            // Calculate the time in seconds
            final double time = ((double) (endTime - startTime)) / 1000000.0;
            // Calculate the distance in meters and return the value
            return (time * 34300.0) / 2000.0;
        } catch (InterruptedException e) {
            throw Throwables.propagate(e);
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Creates {@link Runnable} task with the specified distance callback.
     *
     * @param distanceCallback The distance callback
     * @return The runnable task
     */
    public Runnable createTask(DoubleConsumer distanceCallback) {
        checkNotNull(distanceCallback, "distanceCallback");
        return () -> distanceCallback.accept(readDistance());
    }

    /**
     * Creates {@link Callable<Double>} task with the specified distance callback.
     *
     * @return The callable
     */
    public Callable<Double> createCallable() {
        return this::readDistance;
    }

    @Override
    public void close() {
        final GpioController controller = GpioFactory.getInstance();
        controller.unprovisionPin(this.trigger, this.echo);
    }
}
