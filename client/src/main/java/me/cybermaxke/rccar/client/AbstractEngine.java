/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import static com.google.common.base.Preconditions.checkArgument;

public abstract class AbstractEngine implements Engine {

    private double steering;
    private double speed;

    @Override
    public void cleanup() {
    }

    @Override
    public double getSteering() {
        return this.steering;
    }

    @Override
    public void setSteering(double steering) {
        checkArgument(steering >= -1 && steering <= 1, "The steering factor %s cannot be smaller then -1 or greater then 1.", steering);
        this.steering = steering;
    }

    @Override
    public double getSpeed() {
        return this.speed;
    }

    @Override
    public void setSpeed(double speed) {
        checkArgument(speed >= -1 && speed <= 1, "The speed factor %s cannot be smaller then -1 or greater then 1.", speed);
        this.speed = speed;
    }

    @Override
    public void setConnectionStatus(boolean status) {
    }
}
