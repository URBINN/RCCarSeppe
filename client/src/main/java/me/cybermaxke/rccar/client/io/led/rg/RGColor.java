/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.led.rg;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;

import java.awt.Color;

/**
 * Represents a color with only a red and green component.
 */
public final class RGColor {

    /**
     * Creates a {@link RGColor} from the {@link Color}. The blue and tha alpha
     * components will be ignored.
     *
     * @param color The color
     * @return The rg color
     */
    public static RGColor ofColor(Color color) {
        checkNotNull(color, "color");
        return new RGColor(color.getRed(), color.getGreen());
    }

    private final byte red;
    private final byte green;

    /**
     * Constructs a new {@link RGColor} from the red and green components.
     *
     * @param red The red component
     * @param green The green component
     */
    public RGColor(int red, int green) {
        this.green = (byte) (green & 0xff);
        this.red = (byte) (red & 0xff);
    }

    /**
     * Gets the red component of this {@link RGColor}. The value scales
     * between 0 and 255.
     *
     * @return The red component
     */
    public int getRed() {
        return this.red & 0xff;
    }

    /**
     * Gets the green component of this {@link RGColor}. The value scales
     * between 0 and 255.
     *
     * @return The green component
     */
    public int getGreen() {
        return this.green & 0xff;
    }

    /**
     * Converts this {@link RGColor} to a {@link Color}. The blue
     * component will be set to {@code 0}.
     *
     * @return The color
     */
    public Color toColor() {
        return new Color(this.red & 0xff, this.green & 0xff, 0);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("red", getRed())
                .add("green", getGreen())
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RGColor)) {
            return false;
        }
        final RGColor other = (RGColor) obj;
        return other.red == this.red && other.green == this.green;
    }

    @Override
    public int hashCode() {
        return this.red << 8 | this.green;
    }
}
