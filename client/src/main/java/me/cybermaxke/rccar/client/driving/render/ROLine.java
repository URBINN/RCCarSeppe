/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;

public final class ROLine implements RenderObject {

    private final Vector3d startPosition;
    private final Vector3d endPosition;

    public ROLine(Vector3d startPosition, Vector3d endPosition) {
        checkNotNull(startPosition, "startPosition");
        checkNotNull(endPosition, "endPosition");
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("startPosition", this.startPosition)
                .add("endPosition", this.endPosition)
                .toString();
    }

    @Override
    public void draw(Graphics2D graphics, double height, double width) {
        graphics.draw(new Line2D.Double(this.startPosition.getX(),
                width - this.startPosition.getZ(),
                this.endPosition.getX(),
                width - this.endPosition.getZ()));
    }
}
