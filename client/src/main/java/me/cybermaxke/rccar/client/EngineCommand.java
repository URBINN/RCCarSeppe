/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;

public final class EngineCommand implements Command {

    private final Engine engine;

    public EngineCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        if (arguments.length == 0) {
            throw new CommandException("Not enough arguments.");
        }
        if (arguments[0].equalsIgnoreCase("?") ||
                arguments[0].equalsIgnoreCase("help")) {
            getLogger().info("*** Position Manager Command ***");
            getLogger().info("/{} speedu <value> -- Sets the speed as a units value.", commandName);
            getLogger().info("/{} speed <value> -- Sets the speed as value in the range -1 to 1", commandName);
            getLogger().info("/{} steeru <value> -- Sets the steering as a units value.", commandName);
            getLogger().info("/{} steer <value> -- Sets the steering as a value in the range -1 to 1", commandName);
        } else if (arguments[0].equalsIgnoreCase("speedu")) {
            if (arguments.length != 2) {
                throw new CommandException("Wrong amount of arguments, usage: /%s speedu <value>", commandName);
            }
            int units;
            try {
                units = Integer.parseInt(arguments[1]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid speed units value: " + arguments[1]);
            }
            this.engine.setSpeedUnits(units);
        } else if (arguments[0].equalsIgnoreCase("steeru")) {
            if (arguments.length != 2) {
                throw new CommandException("Wrong amount of arguments, usage: /%s steeru <value>", commandName);
            }
            int units;
            try {
                units = Integer.parseInt(arguments[1]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid steering units value: " + arguments[1]);
            }
            this.engine.setSteeringUnits(units);
        } else if (arguments[0].equalsIgnoreCase("speed")) {
            if (arguments.length != 2) {
                throw new CommandException("Wrong amount of arguments, usage: /%s speed <value>", commandName);
            }
            double units;
            try {
                units = Double.parseDouble(arguments[1]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid speed value: " + arguments[1]);
            }
            this.engine.setSpeed(units);
        } else if (arguments[0].equalsIgnoreCase("steer")) {
            if (arguments.length != 2) {
                throw new CommandException("Wrong amount of arguments, usage: /%s steer <value>", commandName);
            }
            double units;
            try {
                units = Double.parseDouble(arguments[1]);
            } catch (NumberFormatException e) {
                throw new CommandException("Invalid steering value: " + arguments[1]);
            }
            this.engine.setSteering(units);
        }
    }
}
