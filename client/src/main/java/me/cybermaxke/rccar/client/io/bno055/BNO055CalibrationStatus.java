/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import com.google.common.base.MoreObjects;

public final class BNO055CalibrationStatus {

    public static final int FULLY_CALIBRATED = 3;
    public static final int NOT_CALIBRATED = 0;

    private final byte system;
    private final byte accelerometer;
    private final byte magnetometer;
    private final byte gyroscope;

    BNO055CalibrationStatus(byte system, byte accelerometer, byte magnetometer, byte gyroscope) {
        this.accelerometer = accelerometer;
        this.magnetometer = magnetometer;
        this.gyroscope = gyroscope;
        this.system = system;
    }

    public byte getSystemState() {
        return this.system;
    }

    public byte getAccelerometerState() {
        return this.accelerometer;
    }

    public byte getMagnetometerState() {
        return this.magnetometer;
    }

    public byte getGyroscopeState() {
        return this.gyroscope;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Calibration")
                .add("sys", this.system)
                .add("accelerometer", this.accelerometer)
                .add("magnetometer", this.magnetometer)
                .add("gyroscope", this.gyroscope)
                .toString();
    }
}
