/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.bno055;

import com.google.common.base.MoreObjects;

public final class BNO055SystemStatus {

    private final byte systemStatus;
    private final byte systemError;
    private final byte selfTestResult;

    BNO055SystemStatus(byte systemStatus, byte systemError, byte selfTestResult) {
        this.systemStatus = systemStatus;
        this.systemError = systemError;
        this.selfTestResult = selfTestResult;
    }

    /**
     * Gets the status of the sys, it can be one of the following:
     * <p>
     * <table summary="">
     *   <tr>
     *      <td>0x0</td><td>Idle</td>
     *      <td>0x1</td><td>ISystem Error</td>
     *      <td>0x2</td><td>Initializing Peripherals</td>
     *      <td>0x3</td><td>ISystem Initialization</td>
     *      <td>0x4</td><td>Executing Self-Test</td>
     *      <td>0x5</td><td>Sensor fusion algorithm running</td>
     *      <td>0x6</td><td>ISystem running without fusion algorithms</td>
     *   </tr>
     * </table>
     *
     * @return The sys status
     */
    public int getSystemStatus() {
        return this.systemStatus & 0xff;
    }

    /**
     * Gets the sys error where {@code 0x0} means that there is no error, the
     * following errors may occur.
     * <p>
     * <table summary="">
     *   <tr>
     *      <td>0x0</td><td>No error</td>
     *      <td>0x1</td><td>Peripheral initialization error</td>
     *      <td>0x2</td><td>ISystem initialization error</td>
     *      <td>0x3</td><td>Self test result failed</td>
     *      <td>0x4</td><td>Register map value out of range</td>
     *      <td>0x5</td><td>Register map address out of range</td>
     *      <td>0x6</td><td>Register map write error</td>
     *      <td>0x7</td><td>BNO low power mode not available for selected operation mode</td>
     *      <td>0x8</td><td>Accelerometer power mode not available</td>
     *      <td>0x9</td><td>Fusion algorithm configuration error</td>
     *      <td>0xA</td><td>Sensor configuration error</td>
     *   </tr>
     * </table>
     *
     * @return The sys error
     */
    public int getSystemError() {
        return this.systemError & 0xff;
    }

    /**
     * Gets the self test result where {@code 0xf} means that all the tests are successful. And
     * otherwise represents every bit a successful test, the tests are:
     * <p>
     * <table summary="">
     *   <tr>
     *      <td>0</td><td>Accelerometer self test</td>
     *      <td>1</td><td>Magnetometer self test</td>
     *      <td>2</td><td>Gyroscope self test</td>
     *      <td>3</td><td>MCU self test</td>
     *   </tr>
     * </table>
     *
     * @return The self test result
     */
    public int getSelfTestResult() {
        return this.selfTestResult & 0xff;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("systemStatus", this.systemStatus)
                .add("systemError", this.systemError)
                .add("selfTestResult", this.selfTestResult)
                .toString();
    }
}
