/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

import java.util.Arrays;

public class PathPoint {

    private final Vector3d position;
    private final double maxSpeed;

    /**
     * Creates a new {@link PathPoint} with the specified position {@link Vector3d} and
     * the maximum allowed speed behind this point.
     *
     * @param position The position
     * @param maxSpeed The maximum speed
     */
    public PathPoint(Vector3d position, double maxSpeed) {
        this.position = checkNotNull(position, "position");
        checkArgument(maxSpeed > 0, "The max speed must be greater then zero");
        this.maxSpeed = maxSpeed;
    }

    public PathPoint(Vector3d position) {
        this(position, 1);
    }

    /**
     * Gets the position {@link Vector3d} of the path point.
     *
     * @return The position
     */
    public Vector3d getPosition() {
        return this.position;
    }

    /**
     * Gets the allowed speed behind this point. This will be overridden
     * by the new {@link PathPoint}.
     *
     * @return The max speed
     */
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    protected MoreObjects.ToStringHelper toStringHelper() {
        return MoreObjects.toStringHelper(this)
                .add("position", this.position)
                .add("maxSpeed", this.maxSpeed);
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }
}
