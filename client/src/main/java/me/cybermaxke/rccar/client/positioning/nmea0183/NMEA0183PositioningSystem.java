/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning.nmea0183;

import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.Throwables;
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.SerialPort;
import com.pi4j.io.serial.StopBits;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;
import me.cybermaxke.rccar.client.positioning.PositioningSystem;
import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.HeadingSentence;
import net.sf.marineapi.nmea.sentence.PositionSentence;
import net.sf.marineapi.nmea.sentence.RMCSentence;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.sentence.VTGSentence;
import net.sf.marineapi.nmea.util.Position;

import java.io.IOException;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This {@link PositioningSystem} will read serial data in the NMEA0183 format.
 * <p>
 * The default {@link Baud} is {@link Baud#_4800}, higher values may be used
 * if supported by the target device.
 */
// https://github.com/Pi4J/pi4j/blob/master/pi4j-example/src/main/java/SerialExample.java
// https://en.wikipedia.org/wiki/NMEA_0183
public class NMEA0183PositioningSystem implements PositioningSystem, OrientationSystem {

    private final SentenceFactory sentenceFactory = SentenceFactory.getInstance();
    private final Deque<String> deque = new ConcurrentLinkedDeque<>();

    private ScheduledExecutorService executor;
    private Serial serial;

    private volatile Vector3d position = Vector3d.ZERO;
    private volatile Vector3d rotation = Vector3d.ZERO;
    private volatile double speed;

    private final String port;
    private final Baud baud;

    /**
     * Creates a new {@link NMEA0183PositioningSystem} with the
     * default port ({@link SerialPort#getDefaultPort()}) and
     * the default {@link Baud}: {@link Baud#_4800}.
     */
    public NMEA0183PositioningSystem() {
        this(getDefaultPort());
    }

    /**
     * Creates a new {@link NMEA0183PositioningSystem} with the
     * specified port and the default {@link Baud}: {@link Baud#_4800}.
     *
     * @param port The port
     */
    public NMEA0183PositioningSystem(String port) {
        this(port, Baud._4800);
    }

    /**
     * Creates a new {@link NMEA0183PositioningSystem} with the
     * default port ({@link SerialPort#getDefaultPort()}) and the
     * specified {@link Baud}.
     * <p>
     * The supported {@link Baud} is limited by the device.
     *
     * @param baud The baud
     */
    public NMEA0183PositioningSystem(Baud baud) {
        this(getDefaultPort(), baud);
    }

    /**
     * Creates a new {@link NMEA0183PositioningSystem} with the
     * specified port and {@link Baud}.
     * <p>
     * The supported {@link Baud} is limited by the device.
     *
     * @param port The port
     * @param baud The baud
     */
    public NMEA0183PositioningSystem(String port, Baud baud) {
        this.port = checkNotNull(port, "port");
        this.baud = checkNotNull(baud, "baud");
    }

    private static String getDefaultPort() {
        try {
            return SerialPort.getDefaultPort();
        } catch (IOException | InterruptedException e) {
            throw Throwables.propagate(e);
        }
    }

    @Override
    public void init() {
        if (this.serial != null) {
            return;
        }
        this.deque.clear();
        this.serial = SerialFactory.createInstance();
        this.serial.addListener((SerialDataEventListener) event -> {
            try {
                this.deque.add(event.getAsciiString());
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        });

        final SerialConfig config = new SerialConfig();
        try {
            config.device(this.port)
                    .baud(this.baud)
                    .dataBits(DataBits._8)
                    .parity(Parity.NONE)
                    .stopBits(StopBits._1)
                    .flowControl(FlowControl.NONE);
            this.serial.open(config);
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }

        this.executor = Executors.newSingleThreadScheduledExecutor();
        this.executor.scheduleAtFixedRate(this::run, 40, 40, TimeUnit.MILLISECONDS);
    }

    @Override
    public void cleanup() {
        if (this.serial == null) {
            return;
        }
        try {
            this.serial.close();
            this.serial = null;

            this.executor.shutdownNow();
            this.executor = null;
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    private void run() {
        String line;
        while ((line = this.deque.poll()) != null) {
            handleSentence(this.sentenceFactory.createParser(line));
        }
    }

    // knots -> m/s
    private static final double KNOTS_TO_METER_PER_SECOND = 0.514444;

    private void handleSentence(Sentence sentence) {
        if (sentence instanceof PositionSentence) {
            final Position position = ((PositionSentence) sentence).getPosition();
            this.position = new Vector3d(position.getLongitude(), position.getAltitude(), position.getLatitude());

            // TODO: Convert to local coordinates
        }
        if (sentence instanceof HeadingSentence) {
            // Heading provided by GPS is only around the y as (yaw)
            this.rotation = new Vector3d(0, ((HeadingSentence) sentence).getHeading(), 0);
        }
        if (sentence instanceof RMCSentence) {
            this.speed = ((RMCSentence) sentence).getSpeed() * KNOTS_TO_METER_PER_SECOND;
        } else if (sentence instanceof VTGSentence) {
            this.speed = ((VTGSentence) sentence).getSpeedKnots() * KNOTS_TO_METER_PER_SECOND;
        }
    }

    @Override
    public Vector3d getPosition() {
        return this.position;
    }

    @Override
    public Vector3d getVelocity() {
        final Quaterniond rot = Quaterniond.fromAxesAnglesDeg(0, this.rotation.getY(), 0);
        return rot.rotate(new Vector3d(this.speed, 0, 0));
    }

    @Override
    public double getSpeed() {
        return this.speed;
    }

    @Override
    public Vector3d getRotationVector() {
        return this.rotation;
    }
}
