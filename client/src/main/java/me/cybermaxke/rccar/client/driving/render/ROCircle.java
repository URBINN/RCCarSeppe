/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

import java.awt.Graphics2D;
import java.awt.geom.Arc2D;

/**
 * Represents a partial circle, a bow.
 */
public final class ROCircle implements RenderObject {

    private final Vector3d center;
    private final double radius;

    public ROCircle(Vector3d center, double radius) {
        checkArgument(radius > 0, "The range must be greater then 0");
        checkNotNull(center, "center");
        this.radius = radius;
        this.center = center;
    }

    /**
     * Gets the radius.
     *
     * @return The radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Gets the center {@link Vector3d}.
     *
     * @return The center
     */
    public Vector3d getCenter() {
        return this.center;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("center", this.center)
                .add("radius", this.radius)
                .toString();
    }

    @Override
    public void draw(Graphics2D graphics, double height, double width) {
        final double diameter = this.radius * 2.0;
        graphics.draw(new Arc2D.Double(this.center.getX() - this.radius, height - (this.center.getZ() + this.radius),
                diameter, diameter, 0, 360, Arc2D.OPEN));
    }
}
