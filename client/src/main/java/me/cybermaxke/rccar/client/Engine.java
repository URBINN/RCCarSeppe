/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client;

import com.pi4j.io.gpio.GpioController;
import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.sys.SystemPiece;

/**
 * Represents the engine of the client {@link Car}. This class will control the
 * {@link GpioController} outputs of the car for the steering and driving.
 */
public interface Engine extends SystemPiece {

    /**
     * Gets the {@link CarSpecifications}.
     *
     * @return The car specifications
     */
    CarSpecifications getSpecifications();

    /**
     * Gets the steering factor where {@code -1} is completely
     * left and 1 is completely right.
     *
     * @return The steering factor
     */
    double getSteering();

    /**
     * Applies the steering factor where {@code -1} is completely
     * left and 1 is completely right.
     *
     * @param steering The steering factor
     */
    void setSteering(double steering);

    void setSteeringUnits(int steering);

    /**
     * Gets the speed factor where {@code -1} is completely
     * backwards and 1 is completely forwards.
     *
     * @return The speed factor
     */
    double getSpeed();

    /**
     * Applies the speed factor where {@code -1} is completely
     * backwards and 1 is completely forwards.
     *
     * @param speed The speed factor
     */
    void setSpeed(double speed);

    void setSpeedUnits(int speed);

    /**
     * Sets the whether the connection status LED is active.
     *
     * @param status The status
     */
    void setConnectionStatus(boolean status);
}
