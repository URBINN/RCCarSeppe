/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.io.led.rgb;

import static com.google.common.base.Preconditions.checkNotNull;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import me.cybermaxke.rccar.client.io.led.AbstractLed;

import java.awt.Color;

import javax.annotation.Nullable;

/**
 * Represents a colorable (rgb) LED, each component red, green and blue
 * is presented by a {@link Pin} that will be controlled by a soft pwm.
 *
 * For example:
 *  KY-009 (RGB LED SMD Module) from SensorKit X40,
 *  KY-016 (RGB 5mm LED Module) from SensorKit X40:
 *    Vf [Red] = 1,8 V
 *    Vf [Green, Blue] = 2,8 V
 *    If = 20 mA
 *    -> 3.3 V Output:
 *         Rf [Red]   = 180 ohm
 *         Rf [Green] = 100 ohm
 *         Rf [Blue]  = 100 ohm
 *    Pins:
 *      1. GND
 *      2. Red
 *      3. Green
 *      4. Blue
 */
public final class RGBLed extends AbstractLed<Color> {

    private final GpioPinPwmOutput red;
    private final GpioPinPwmOutput green;
    private final GpioPinPwmOutput blue;

    /**
     * Creates a new {@link RGBLed} for the specified {@link Pin}s
     * and with a specific name.
     *
     * @param redPin The pin for the red component
     * @param greenPin The pin for the green component
     * @param bluePin The pin for the blue component
     * @param name The name of this LED
     */
    public RGBLed(Pin redPin, Pin greenPin, Pin bluePin, String name) {
        this(checkName(name), redPin, greenPin, bluePin);
    }

    /**
     * Creates a new {@link RGBLed} for the specified {@link Pin}s.
     *
     * @param redPin The pin for the red component
     * @param greenPin The pin for the green component
     * @param bluePin The pin for the blue component
     */
    public RGBLed(Pin redPin, Pin greenPin, Pin bluePin) {
        this(null, redPin, greenPin, bluePin);
    }

    private RGBLed(@Nullable String name, Pin redPin, Pin greenPin, Pin bluePin) {
        checkNotNull(redPin, "redPin");
        checkNotNull(greenPin, "greenPin");
        checkNotNull(bluePin, "bluePin");
        this.red = provisionSoftPwmOutput(name, "red", redPin);
        this.red.setShutdownOptions(true);
        this.red.setPwmRange(255);
        this.green = provisionSoftPwmOutput(name, "green", greenPin);
        this.green.setShutdownOptions(true);
        this.green.setPwmRange(255);
        this.blue = provisionSoftPwmOutput(name, "blue", bluePin);
        this.blue.setShutdownOptions(true);
        this.blue.setPwmRange(255);
        setColor(Color.RED);
    }

    @Override
    protected void applyColor(Color color) {
        this.red.setPwm(color.getRed());
        this.green.setPwm(color.getGreen());
        this.blue.setPwm(color.getBlue());
    }

    @Override
    public void close() {
        final GpioController controller = GpioFactory.getInstance();
        controller.unprovisionPin(this.red, this.green, this.blue);
    }
}
