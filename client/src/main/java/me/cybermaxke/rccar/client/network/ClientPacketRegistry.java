/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network;

import me.cybermaxke.rccar.client.network.handler.InitCarBulkHandler;
import me.cybermaxke.rccar.client.network.handler.InitCarHandler;
import me.cybermaxke.rccar.client.network.handler.ServerHandshakeHandler;
import me.cybermaxke.rccar.client.network.handler.UpdateCarHandler;
import me.cybermaxke.rccar.network.PacketRegistry;
import me.cybermaxke.rccar.network.types.server.InitCarBulkPacket;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;
import me.cybermaxke.rccar.network.types.server.ServerHandshakePacket;
import me.cybermaxke.rccar.network.types.server.UpdateCarPacket;

final class ClientPacketRegistry extends PacketRegistry {

    {
        bindClientHandler(ServerHandshakePacket.class, new ServerHandshakeHandler());
        bindClientHandler(InitCarPacket.class, new InitCarHandler());
        bindClientHandler(InitCarBulkPacket.class, new InitCarBulkHandler());
        bindClientHandler(UpdateCarPacket.class, new UpdateCarHandler());
    }
}
