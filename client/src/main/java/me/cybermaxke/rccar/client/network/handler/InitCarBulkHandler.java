/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network.handler;

import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Handler;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.server.InitCarBulkPacket;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;

public final class InitCarBulkHandler implements Handler<InitCarBulkPacket> {

    @Override
    public void handle(Session session, InitCarBulkPacket packet) {
        getLogger().info("Initializing the framework with following cars: [");
        for (InitCarPacket packet1 : packet.getPackets()) {
            final Car car = session.getFramework().getOrAddCar(packet1.getUniqueId());
            car.update(packet1.getPosition(), packet1.getRotation(), packet1.getUpdateTime());
            getLogger().info("    " + packet1.getUniqueId());
        }
        getLogger().info("]");
    }
}
