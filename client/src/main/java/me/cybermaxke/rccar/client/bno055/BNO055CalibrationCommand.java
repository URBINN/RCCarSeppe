/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.bno055;

import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.client.io.bno055.BNO055;
import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;

/**
 * A command that helps with the process to calibrate
 * the {@link BNO055} sensor.
 *
 * Usage:
 *
 * -- Starts the calibration process.
 * /bno055-cal start
 *
 * -- Saves the current calibration data.
 * /bno055-cal save
 */
public class BNO055CalibrationCommand implements Command {

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        final BNO055Sensor sensor = BNO055Sensor.INSTANCE;
        if (arguments.length == 0) {
            throw new CommandException("Not enough arguments.");
        }
        if (arguments[0].equalsIgnoreCase("?") ||
                arguments[0].equalsIgnoreCase("help")) {
            getLogger().info("*** BNO055 Logger ***");
            getLogger().info("/{} start -- Starts the calibration process.", commandName);
            getLogger().info("/{} save -- Saves the current calibration data.", commandName);
        } else if (arguments[0].equalsIgnoreCase("start")) {
            if (arguments.length != 1) {
                throw new CommandException("Wrong amount of arguments, usage: /%s start", commandName);
            }
            sensor.startCalibration();
            getLogger().info("Successfully started the BNO055 calibration.");
        } else if (arguments[0].equalsIgnoreCase("save")) {
            if (arguments.length != 1) {
                throw new CommandException("Wrong amount of arguments, usage: /%s save", commandName);
            }
            sensor.saveCalibrationData();
            getLogger().info("Successfully saved the BNO055 calibration data.");
        }
    }
}
