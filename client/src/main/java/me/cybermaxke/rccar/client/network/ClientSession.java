/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network;

import static me.cybermaxke.rccar.Framework.getLogger;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import me.cybermaxke.rccar.client.Client;
import me.cybermaxke.rccar.network.ConnectionSide;
import me.cybermaxke.rccar.network.Packet;
import me.cybermaxke.rccar.network.PacketRegistry;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.client.ClientHandshakePacket;
import me.cybermaxke.rccar.network.types.common.KeepAlivePacket;

import java.net.InetSocketAddress;
import java.net.NetworkInterface;

public class ClientSession extends Session {

    ClientSession(Client client, Channel channel, PacketRegistry packetRegistry,
            ConnectionSide connectionSide) {
        super(client, packetRegistry, connectionSide, channel);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) throws Exception {
        if (msg instanceof KeepAlivePacket) {
            send(msg);
        } else {
            super.channelRead0(ctx, msg);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        final NetworkInterface network = NetworkInterface.getByInetAddress(((InetSocketAddress) ctx.channel().localAddress()).getAddress());
        getLogger().info("Network Interface: {}", network);
        final byte[] mac = network.getHardwareAddress();
        if (mac != null) {
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            getLogger().info("MAC Address: {}", sb.toString());
        } else {
            getLogger().info("MAC Address: Unknown");
        }
        send(new ClientHandshakePacket(((Client) getFramework()).getUniqueId(), PacketRegistry.PROTOCOL_VERSION));
    }
}
