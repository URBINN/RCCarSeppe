/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

public final class PathTurn extends PathPoint {

    private final Vector3d center;
    private final double range;

    PathTurn(Vector3d center, Vector3d endPosition, double maxSpeed, double range) {
        super(endPosition, maxSpeed);
        this.center = checkNotNull(center, "center");
        this.range = range;
    }

    PathTurn(Vector3d center, Vector3d endPosition, double range) {
        this(center, endPosition, 1, range);
    }

    public Vector3d getCenter() {
        return this.center;
    }

    public Vector3d getEndPosition() {
        return getPosition();
    }

    public double getRange() {
        return this.range;
    }

    @Override
    protected MoreObjects.ToStringHelper toStringHelper() {
        return super.toStringHelper()
                .add("center", this.center)
                .add("range", this.range);
    }
}
