/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.network.handler;

import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.SyncHandler;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;

public final class InitCarHandler implements SyncHandler<InitCarPacket> {

    @Override
    public void handle(Session session, InitCarPacket packet) {
        final boolean added = session.getFramework().getCar(packet.getUniqueId()).isPresent();
        final Car car = session.getFramework().getOrAddCar(packet.getUniqueId());
        car.update(packet.getPosition(), packet.getRotation(), packet.getUpdateTime());
        if (!added) {
            getLogger().info("Added a new car to the framework: {}", car.getUniqueId());
        } else {
            getLogger().info("Updated the car from the framework: {}", car.getUniqueId());
        }
    }
}
