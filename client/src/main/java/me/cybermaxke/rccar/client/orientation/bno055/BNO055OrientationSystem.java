/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.orientation.bno055;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.Throwables;
import com.google.common.collect.EvictingQueue;
import me.cybermaxke.rccar.client.bno055.BNO055Sensor;
import me.cybermaxke.rccar.client.filter.Filters;
import me.cybermaxke.rccar.client.io.bno055.BNO055;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;
import me.cybermaxke.rccar.util.StampedValue;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.Nullable;

public class BNO055OrientationSystem implements OrientationSystem, PulseableSystemPiece {

    private static final int MAX_MEASUREMENTS = 200;

    private final EvictingQueue<StampedValue<Vector3d>> measurements = EvictingQueue.create(MAX_MEASUREMENTS);
    @Nullable private ScheduledFuture<?> scheduledFuture;
    private Vector3d rotation = Vector3d.ZERO;

    @Override
    public Vector3d getRotationVector() {
        return this.rotation;
    }

    @Override
    public void init() {
        if (this.scheduledFuture != null) {
            return;
        }
        // Read the data from the sensor on the i2c bus executor periodically
        this.scheduledFuture = BNO055Sensor.INSTANCE.submit(this::accept, Duration.ofMillis(0), Duration.ofMillis(10));
    }

    @Override
    public void cleanup() {
        if (this.scheduledFuture != null) {
            this.scheduledFuture.cancel(true);
            this.scheduledFuture = null;
        }
    }

    @Override
    public void pulse(double deltaTime) {
        final StampedValue<Vector3d>[] stampedValues;
        synchronized (this.measurements) {
            //noinspection unchecked
            stampedValues = this.measurements.toArray(new StampedValue[this.measurements.size()]);
        }

        // Filter the vectors
        Vector3d[] vectors = new Vector3d[stampedValues.length];
        for (int i = 0; i < stampedValues.length; i++) {
            vectors[i] = stampedValues[i].getValue();
        }
        vectors = Filters.filterRotation(vectors,
                5, 400.0, 0, vectors.length - 1, true).getData()[0];

        this.rotation = vectors[vectors.length - 1];
    }

    private void accept(BNO055 bno055) {
        // Read and store measurements
        try {
            final Vector3d rotation = bno055.readEulerAngleData();
            final StampedValue<Vector3d> stampedValue = new StampedValue<>(rotation);
            synchronized (this.measurements) {
                this.measurements.add(stampedValue);
            }
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }
}
