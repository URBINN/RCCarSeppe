/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Path {

    public static final Path EMPTY = new Path(Collections.emptyList());

    private final List<PathPoint> points;

    /**
     * Creates a new {@link Path} with the given {@link PathPoint}s.
     *
     * @param points The points
     */
    public Path(List<PathPoint> points) {
        this.points = ImmutableList.copyOf(points);
    }

    /**
     * Gets the list of {@link PathPoint}s.
     *
     * @return The points
     */
    public List<PathPoint> getPoints() {
        return this.points;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("points", Arrays.toString(this.points.toArray(new PathPoint[this.points.size()])))
                .toString();
    }
}
