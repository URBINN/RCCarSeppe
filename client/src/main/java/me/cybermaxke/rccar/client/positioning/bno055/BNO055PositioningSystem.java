/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.positioning.bno055;

import static me.cybermaxke.rccar.Framework.getLogger;

import com.flowpowered.math.imaginary.Quaterniond;
import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.Throwables;
import com.google.common.collect.EvictingQueue;
import me.cybermaxke.rccar.client.bno055.BNO055Sensor;
import me.cybermaxke.rccar.client.filter.SGFilter3;
import me.cybermaxke.rccar.client.filter.Filters;
import me.cybermaxke.rccar.client.io.bno055.BNO055;
import me.cybermaxke.rccar.client.orientation.OrientationSystem;
import me.cybermaxke.rccar.client.positioning.PositioningSystem;
import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;
import me.cybermaxke.rccar.sys.SystemPiece;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.Nullable;

/**
 * This {@link SystemPiece} implements both the {@link PositioningSystem} and {@link OrientationSystem}.
 * <p>
 * The position is calculated with acceleration and orientation of the {@link BNO055} sensor, all signals
 * are constantly being reprocessed to filterAcceleration out bad signals and noise (by using old measurements).
 * The position and velocity is also being recalculated for the {@link #REVISITED_MEASUREMENTS} last values
 * since the curve was not completely finished for the last retrieved values and they need recalculation
 * to remove noise and errors.
 * <p>
 * Filter process:
 *  Acceleration Data:
 *   1. Filter out big peaks
 *   2. Apply a Moving Average Filter
 *   3. Filter out acceleration and movement when stationary
 *   4. Optionally apply a Savitzky-Golay filterAcceleration to make the curve smoother
 *  Orientation Data:
 *   1. Filter out big peaks.
 *   2. Apply a Standard Deviation Filter
 * <p>
 * Commands:
 *
 * -- Resets the measurements, position and velocity.
 *      /bno055-pos reset
 *
 */
public final class BNO055PositioningSystem implements PositioningSystem, OrientationSystem, PulseableSystemPiece, Command {

    /**
     * The past and future points quantity that should be used for the smoothing.
     */
    private static final int FILTER_POINTS = 11;

    /**
     * The coefficients for the Savitzky-Golay filterAcceleration.
     */
    private static final double[] SG_FILTER_COEFFICIENTS = SGFilter3.computeSGCoefficients(FILTER_POINTS, FILTER_POINTS, 2);

    /**
     * The amount of measurements that should be kept in the memory.
     */
    private static final int MAX_MEASUREMENTS = 20000;

    /**
     * The amount of measurements that should be revisited to generate the current position,
     * the last point that isn't revisited will be used as reference.
     */
    private static final int REVISITED_MEASUREMENTS = 50;

    /**
     * The queue with all the {@link Measurement}s, old ones are automatically removed.
     */
    private final EvictingQueue<Measurement> measurements = EvictingQueue.create(MAX_MEASUREMENTS);

    /**
     * The Savitzky-Golay filterAcceleration instance.
     */
    private final SGFilter3 filter = new SGFilter3(FILTER_POINTS, FILTER_POINTS);

    @Nullable private ScheduledFuture<?> scheduledFuture;
    private volatile Vector3d position = Vector3d.ZERO;
    private volatile Vector3d rotation = Vector3d.ZERO;
    private volatile Vector3d velocity = Vector3d.ZERO;

    private volatile Quaterniond baseRotation = null;

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        if (arguments.length == 0) {
            throw new CommandException("Not enough arguments.");
        }
        if (arguments[0].equalsIgnoreCase("?") ||
                arguments[0].equalsIgnoreCase("help")) {
            getLogger().info("*** BNO055 Positioning System ***");
            getLogger().info("/{} reset -- Resets the measurements, position and velocity.", commandName);
        } else if (arguments[0].equalsIgnoreCase("reset")) {
            if (arguments.length != 1) {
                throw new CommandException("Wrong amount of arguments, usage: /{} reset", commandName);
            }
            reset();
            getLogger().info("Successfully resetted the position.");
        } else if (arguments[0].equalsIgnoreCase("save")) {
            if (arguments.length != 2) {
                throw new CommandException("Wrong amount of arguments, usage: /{} save <path>", commandName);
            }
            saveTo(Paths.get(arguments[1] + ".xlsx"));
            getLogger().info("Successfully saved the measurements.");
        }
    }

    private static class Measurement {

        /**
         * The raw acceleration data.
         */
        private final Vector3d accelerationData;
        /**
         * The timestamp in millis when the measurement was made.
         */
        private final long accelerationStamp;

        /**
         * The raw orientation data.
         */
        private final Vector3d orientationData;
        /**
         * The timestamp in millis when the measurement was made, currently
         * is just the {@link #accelerationStamp} used instead.
         */
        private final long orientationStamp;

        @Nullable private Vector3d sensorMovement;
        @Nullable private Vector3d sensorPosition;
        @Nullable private Vector3d position;
        @Nullable private Vector3d velocity;

        @Nullable private Vector3d filteredAccelerationData;
        @Nullable private Vector3d filteredOrientationData;

        @Nullable private Vector3d filteredAccelerationData1;
        @Nullable private Vector3d filteredAccelerationData2;

        private Measurement(
                Vector3d accelerationData, long accelerationStamp,
                Vector3d orientationData, long orientationStamp) {
            this.accelerationData = accelerationData;
            this.accelerationStamp = accelerationStamp;
            this.orientationData = orientationData;
            this.orientationStamp = orientationStamp;
        }
    }

    @Override
    public Vector3d getRotationVector() {
        return this.rotation;
    }

    @Override
    public Vector3d getPosition() {
        return this.position;
    }

    @Override
    public Vector3d getVelocity() {
        return Quaterniond.fromAxesAnglesDeg(this.rotation.getX(),
                this.rotation.getY(), this.rotation.getZ()).rotate(this.velocity);
    }

    @Override
    public double getSpeed() {
        return this.velocity.length();
    }

    @Override
    public void init() {
        if (this.scheduledFuture != null) {
            return;
        }
        // Read the data from the sensor on the i2c bus executor periodically
        this.scheduledFuture = BNO055Sensor.INSTANCE.submit(this::accept, Duration.ofMillis(0), Duration.ofMillis(10));
    }

    @Override
    public void cleanup() {
        if (this.scheduledFuture != null) {
            this.scheduledFuture.cancel(true);
            this.scheduledFuture = null;
        }
    }

    private long lastLogTime;

    private Filters.FilterData filterAccelerationVectors(Vector3d[] accelerationVectors) {
        return Filters.filterAcceleration(this.filter, accelerationVectors, SG_FILTER_COEFFICIENTS,
                12, 10.0, 0, Math.max(0, accelerationVectors.length - 1), 0.4);
    }

    public synchronized void saveTo(Path path) {
        final Measurement[] measurements;
        synchronized (this.measurements) {
            //noinspection unchecked
            measurements = this.measurements.toArray(new Measurement[this.measurements.size()]);
        }

        final XSSFWorkbook workbook = new XSSFWorkbook();
        final XSSFSheet sheet = workbook.createSheet("BNO055 Positioning System Measurements");

        // Write all the sensor data to a excel file, don't mind the mess, it's just for testing
        // so there is no point in cleaning it up.

        int rowCount0 = 0;

        Row row = sheet.createRow(rowCount0++);
        int columnCount = 0;

        row.createCell(columnCount++).setCellValue("Time Stamp");
        row.createCell(columnCount).setCellValue("Raw Acceleration");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Filtered Acceleration");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Velocity");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Position");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Sensor Movement");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Sensor Position");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Filtered Acceleration Temp 1");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Filtered Acceleration Temp 2");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Raw Orientation");
        columnCount += 3;
        row.createCell(columnCount).setCellValue("Filtered Orientation");
        columnCount += 3;

        row = sheet.createRow(rowCount0++);
        columnCount = 0;
        row.createCell(columnCount++).setCellValue("time");
        for (int i = 0; i < 9; i++) {
            row.createCell(columnCount++).setCellValue("x");
            row.createCell(columnCount++).setCellValue("y");
            row.createCell(columnCount++).setCellValue("z");
        }
        row.createCell(columnCount++).setCellValue("pitch");
        row.createCell(columnCount++).setCellValue("yaw");
        row.createCell(columnCount++).setCellValue("roll");
        row.createCell(columnCount++).setCellValue("pitch");
        row.createCell(columnCount++).setCellValue("yaw");
        row.createCell(columnCount++).setCellValue("roll");

        final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS").withZone(ZoneId.systemDefault());

        for (Measurement measurement : measurements) {
            row = sheet.createRow(rowCount0++);
            columnCount = 0;
            row.createCell(columnCount++).setCellValue(timeFormatter.format(Instant.ofEpochMilli(measurement.accelerationStamp)));
            row.createCell(columnCount++).setCellValue(measurement.accelerationData.getX());
            row.createCell(columnCount++).setCellValue(measurement.accelerationData.getY());
            row.createCell(columnCount++).setCellValue(measurement.accelerationData.getZ());
            if (measurement.filteredAccelerationData != null) {
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData.getX());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData.getY());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.velocity != null) {
                row.createCell(columnCount++).setCellValue(measurement.velocity.getX());
                row.createCell(columnCount++).setCellValue(measurement.velocity.getY());
                row.createCell(columnCount++).setCellValue(measurement.velocity.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.position != null) {
                row.createCell(columnCount++).setCellValue(measurement.position.getX());
                row.createCell(columnCount++).setCellValue(measurement.position.getY());
                row.createCell(columnCount++).setCellValue(measurement.position.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.sensorMovement != null) {
                row.createCell(columnCount++).setCellValue(measurement.sensorMovement.getX());
                row.createCell(columnCount++).setCellValue(measurement.sensorMovement.getY());
                row.createCell(columnCount++).setCellValue(measurement.sensorMovement.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.sensorPosition != null) {
                row.createCell(columnCount++).setCellValue(measurement.sensorPosition.getX());
                row.createCell(columnCount++).setCellValue(measurement.sensorPosition.getY());
                row.createCell(columnCount++).setCellValue(measurement.sensorPosition.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.filteredAccelerationData1 != null) {
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData1.getX());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData1.getY());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData1.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.filteredAccelerationData2 != null) {
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData2.getX());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData2.getY());
                row.createCell(columnCount++).setCellValue(measurement.filteredAccelerationData2.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.orientationData != null) {
                row.createCell(columnCount++).setCellValue(measurement.orientationData.getX());
                row.createCell(columnCount++).setCellValue(measurement.orientationData.getY());
                row.createCell(columnCount++).setCellValue(measurement.orientationData.getZ());
            } else {
                columnCount += 3;
            }
            if (measurement.filteredOrientationData != null) {
                row.createCell(columnCount++).setCellValue(measurement.filteredOrientationData.getX());
                row.createCell(columnCount++).setCellValue(measurement.filteredOrientationData.getY());
                row.createCell(columnCount++).setCellValue(measurement.filteredOrientationData.getZ());
            } else {
                columnCount += 3;
            }
        }

        try (OutputStream outputStream = Files.newOutputStream(path)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reset() {
        synchronized (this.measurements) {
            this.measurements.clear();
        }
        this.position = Vector3d.ZERO;
        this.rotation = Vector3d.ZERO;
    }

    // The displacement of the sensor relative to the rotation point of the car.
    private final static Vector3d NEG_SENSOR_OFFSET = new Vector3d(-0.005, 0, -0.09);

    @SuppressWarnings("ConstantConditions")
    @Override
    public synchronized void pulse(double deltaTime) {
        final Measurement[] measurements;
        synchronized (this.measurements) {
            //noinspection unchecked
            measurements = this.measurements.toArray(new Measurement[this.measurements.size()]);
        }

        if (measurements.length == 0) {
            return;
        }

        // Generate vector arrays
        Vector3d[] orientationVectors = new Vector3d[measurements.length];
        Vector3d[] accelerationVectors = new Vector3d[measurements.length];

        for (int i = 0; i < measurements.length; i++) {
            orientationVectors[i] = measurements[i].orientationData;
            accelerationVectors[i] = measurements[i].accelerationData;
        }

        // Filter the vectors
        orientationVectors = Filters.filterRotation(orientationVectors,
                5, 400.0, 0, Math.max(0, orientationVectors.length - 1), true).getData()[0];
        final Filters.FilterData accelerationVectors0 = filterAccelerationVectors(accelerationVectors);
        accelerationVectors = accelerationVectors0.getData()[0];

        // Store the filtered vectors
        for (int i = 0; i < measurements.length; i++) {
            measurements[i].filteredOrientationData = orientationVectors[i];
            measurements[i].filteredAccelerationData = accelerationVectors[i];
            measurements[i].filteredAccelerationData1 = accelerationVectors0.getData()[1][i];
            measurements[i].filteredAccelerationData2 = accelerationVectors0.getData()[0][i];
        }

        // Find the last known position, the oldest revisited measurement
        // This is the starting point
        int pos = measurements.length < REVISITED_MEASUREMENTS ? 0 : measurements.length - REVISITED_MEASUREMENTS;
        for (int i = pos; i >= 0; i--) {
            if (measurements[i].position != null) {
                pos = i;
                break;
            }
        }
        // The initial position
        if (measurements[pos].position == null) {
            measurements[pos].position = Vector3d.ZERO;
            measurements[pos].sensorPosition = Vector3d.ZERO;
            measurements[pos].sensorMovement = Vector3d.ZERO;
            measurements[pos].velocity = Vector3d.ZERO;
        }
        // https://www.reddit.com/r/robotics/comments/3o7ay6/integrating_accelerometer_data_into_position/
        Vector3d sensorMovement = measurements[pos].sensorMovement;
        Vector3d sensorPosition = measurements[pos].sensorPosition;
        Vector3d position = measurements[pos].position;
        Vector3d velocity = measurements[pos].velocity;
        // Now, calculate the new positions based on that position
        for (int i = pos + 1; i < measurements.length; i++) {
            final Vector3d lastAcc = measurements[i - 1].filteredAccelerationData;
            final Vector3d currAcc = measurements[i].filteredAccelerationData;
            final double dTime = (double) (measurements[i].accelerationStamp - measurements[i - 1].accelerationStamp) / 1000.0;
            double vx = velocity.getX();
            double vy = velocity.getY();
            double vz = velocity.getZ();
            if (accelerationVectors0.getStationary()[0][i]) {
                vx = 0;
            } else {
                vx += (lastAcc.getX() + (currAcc.getX() - lastAcc.getX()) / 2.0) * dTime;
            }
            if (accelerationVectors0.getStationary()[1][i]) {
                vy = 0;
            } else {
                vy += (lastAcc.getY() + (currAcc.getY() - lastAcc.getY()) / 2.0) * dTime;
            }
            if (accelerationVectors0.getStationary()[2][i]) {
                vz = 0;
            } else {
                vz += (lastAcc.getZ() + (currAcc.getZ() - lastAcc.getZ()) / 2.0) * dTime;
            }
            velocity = new Vector3d(vx, vy, vz);
            // Only use the z velocity for now
            velocity = new Vector3d(vx == 0 ? 0 : -0.02, 0, vz * 1.0005);

            // TODO: There is currently a issue that the sensor has a increased deviation
            // TODO: on the x axis, which brakes the whole positioning process, maybe this
            // TODO: is related to the orientation sensor? The total relative movement (no axes) is OK,
            // TODO: but not when translated to the axes system.

            // final Vector3d r = measurements[i].filteredOrientationData.mul(10.0).toInt().toDouble().div(10.0);
            // measurements[i].filteredOrientationData = r;
            final Vector3d r = measurements[i].filteredOrientationData.mul(0, 1, 0);
            Quaterniond rot = Quaterniond.fromAxesAnglesDeg(r.getX(), r.getY(), r.getZ());
            if (this.baseRotation != null) {
                rot = rot.add(this.baseRotation);
            }

            // final Quaterniond rot = Quaterniond.fromAxesAnglesDeg(0, measurements[i].filteredOrientationData.getY(), 0);
            final Vector3d deltaMovement = velocity.mul(dTime);
            sensorMovement = measurements[i].sensorMovement = sensorMovement.add(deltaMovement);
            sensorPosition = measurements[i].sensorPosition = sensorPosition.add(
                    rot.equals(Quaterniond.ZERO) ? deltaMovement : rot.rotate(deltaMovement));
            position = /*rot.rotate(NEG_SENSOR_OFFSET).add(*/sensorPosition/*)*/;
            measurements[i].position = position;
            measurements[i].velocity = velocity;
        }
        this.rotation = orientationVectors[measurements.length - 1];
        if (this.baseRotation != null) {
            this.rotation = this.baseRotation.sub(Quaterniond.fromAxesAnglesDeg(
                    this.rotation.getX(), this.rotation.getY(), this.rotation.getZ()))
                    .getAxesAnglesDeg().add(0, 90, 0);
            final Quaterniond rot = Quaterniond.fromAxesAnglesDeg(
                    0, this.baseRotation.getAxesAnglesDeg().getY(), 0);
            //System.out.println("ORIG POS: " + position);
            //this.position = rot.rotate(position);
        } else {
            //this.position = position;
        }
        this.position = position;
        // Log the position
        final long time = System.currentTimeMillis();
        if (this.lastLogTime == 0 || time - this.lastLogTime > 1000) {
            this.lastLogTime = time;
            // Log the position and orientation, for now
            System.out.println("The position: " + this.position);
            System.out.println("The orientation: " + this.rotation);
        }
    }

    private void accept(BNO055 bno055) {
        try {
            Vector3d accelerationData = bno055.readLinearAccelerationData();
            final long accelerationStamp = System.currentTimeMillis();
            accelerationData = new Vector3d(accelerationData.getY(), accelerationData.getX() - 0.23, accelerationData.getZ());

            Vector3d orientationData = bno055.readEulerAngleData();
            final long orientationStamp = System.currentTimeMillis();
            orientationData = new Vector3d(orientationData.getZ(), orientationData.getX(), orientationData.getY());

            final Measurement measurement = new Measurement(accelerationData, accelerationStamp, orientationData, orientationStamp);
            synchronized (this.measurements) {
                this.measurements.add(measurement);
            }
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public void setBaseRotation(Quaterniond rotation) {
        this.baseRotation = rotation == null ? null : rotation;//rotation.sub(Quaterniond.fromAxesAnglesRad(0, 90, 0));
    }
}
