/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.client.driving.render;

import com.flowpowered.math.vector.Vector3d;
import me.cybermaxke.rccar.client.driving.Path;
import me.cybermaxke.rccar.client.driving.PathPoint;
import me.cybermaxke.rccar.client.driving.PathProcessor;
import me.cybermaxke.rccar.util.Transform;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class RenderFrame {

    public static void main(String[] args) {

/*
        final Path path = new Path(Arrays.asList(
                new PathPoint(new Vector3d(0, 0, 10), 1.0),
                new PathPoint(new Vector3d(200, 0, 10), 1.0),
                new PathPoint(new Vector3d(200, 0, 320), 1.0),
                new PathPoint(new Vector3d(40, 0, 480), 1.0),
                new PathPoint(new Vector3d(300, 0, 480), 1.0),
                new PathPoint(new Vector3d(350, 0, 200), 1.0),
                new PathPoint(new Vector3d(450, 0, 200), 1.0),
                new PathPoint(new Vector3d(450, 0, 400), 1.0),
                new PathPoint(new Vector3d(0, 0, 400), 1.0),
                new PathPoint(new Vector3d(0, 0, 10), 1.0)
        ));

/*
        final Path path = new Path(Arrays.asList(
                new PathPoint(new Vector3d(200, 0, 250), 1.0),
                new PathPoint(new Vector3d(400, 0, 250), 1.0),
                new PathPoint(new Vector3d(250, 0, 400), 1.0),
                new PathPoint(new Vector3d(340, 0, 400), 1.0),
                new PathPoint(new Vector3d(400, 0, 600), 1.0),
                new PathPoint(new Vector3d(650, 0, 600), 1.0),
                new PathPoint(new Vector3d(500, 0, 400), 1.0),
                new PathPoint(new Vector3d(550, 0, 310), 1.0),
                new PathPoint(new Vector3d(400, 0, 330), 1.0),
                new PathPoint(new Vector3d(420, 0, 500), 1.0)
        ));*//*

        final Path path = new Path(Arrays.asList(
                new PathPoint(new Vector3d(10, 0, 10), 1.0),
                new PathPoint(new Vector3d(490, 0, 10), 1.0),
                new PathPoint(new Vector3d(10, 0, 490), 1.0),
                new PathPoint(new Vector3d(490, 0, 490), 1.0)
        ));*/

        /*
        final Path path = new Path(Arrays.asList(
                new PathPoint(new Vector3d(200, 0, 300), 1.0),
                new PathPoint(new Vector3d(400, 0, 300), 1.0),
                new PathPoint(new Vector3d(400, 0, 500), 1.0),
                new PathPoint(new Vector3d(200, 0, 600), 1.0),
                new PathPoint(new Vector3d(200, 0, 700), 1.0),
                new PathPoint(new Vector3d(400, 0, 700), 1.0)
        ));*/
        final Path path = new Path(Arrays.asList(
                new PathPoint(new Vector3d(200, 0, 100)),
                new PathPoint(new Vector3d(200, 0, 500)),
                new PathPoint(new Vector3d(700, 0, 500))));

        final List<RenderObject> objects = new ArrayList<>();
        final PathProcessor pathProcessor = new PathProcessor();

        objects.add((graphics, height, width) -> graphics.setStroke(new BasicStroke(5)));
        objects.addAll(pathProcessor.toRender(path));
        objects.add((graphics, height, width) -> graphics.setStroke(new BasicStroke(2)));
        objects.add((graphics, height, width) -> graphics.setColor(Color.RED));
        final PathProcessor.Result result = pathProcessor.process(path, 69.5);
        objects.addAll(result.getRenderObjects());
        objects.addAll(pathProcessor.processControlling(new Transform(new Vector3d(230, 0, 150), new Vector3d(0, 80, 0)),
                result.getPath(), 69.5, 25, 20.0, 0).getRenderObjects());
/*
        System.out.println(pathProcessor.processControlling(new Transform(new Vector3d(250, 0, 470), new Vector3d(0, 10, 0)),
                result.getPath(), 69.5, 25, 20.0, 0));*/
        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, new Component(objects)));
    }

    private final static class Component extends JComponent {

        private final List<RenderObject> objects;

        Component(List<RenderObject> objects) {
            setPreferredSize(new Dimension(800, 800));
            this.objects = objects;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            final Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.setColor(Color.WHITE);
            graphics2D.fillRect(0, 0, 800, 800);
            graphics2D.setColor(Color.BLACK);
            for (RenderObject object : this.objects) {
                object.draw(graphics2D, getPreferredSize().getHeight(), getPreferredSize().getWidth());
            }
        }
    }
}
