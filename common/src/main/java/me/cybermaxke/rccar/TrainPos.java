/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar;

import com.google.common.base.MoreObjects;

/**
 * Represents the position of a {@link Car}
 * within a {@link Train}.
 */
public final class TrainPos {

    private final Car car;
    private final Train train;
    private final int position;

    TrainPos(Car car, Train train, int position) {
        this.car = car;
        this.train = train;
        this.position = position;
    }

    public Train getTrain() {
        return this.train;
    }

    public Car getCar() {
        return this.car;
    }

    public int getPosition() {
        return this.position;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("car", this.car)
                .add("position", this.position)
                .toString();
    }
}
