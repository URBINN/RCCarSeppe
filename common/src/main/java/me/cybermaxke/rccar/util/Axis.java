/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.util;

public enum Axis {
    X,
    Y,
    Z,
    ;
}
