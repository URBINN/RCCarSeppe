/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.util;

import static com.google.common.base.Preconditions.checkNotNull;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

public final class Transform {

    private final Vector3d position;
    private final Vector3d rotation;

    public Transform(Vector3d position, Vector3d rotation) {
        checkNotNull(position, "position");
        checkNotNull(rotation, "rotation");
        this.position = position;
        this.rotation = rotation;
    }

    public Vector3d getPosition() {
        return this.position;
    }

    public Vector3d getRotation() {
        return this.rotation;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("position", this.position)
                .add("rotation", this.rotation)
                .toString();
    }
}

