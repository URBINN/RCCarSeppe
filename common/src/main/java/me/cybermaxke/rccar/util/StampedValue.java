/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.util;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;

public final class StampedValue<T> {

    private final T value;
    private final long timestamp;

    public StampedValue(T value) {
        this(value, System.currentTimeMillis());
    }

    public StampedValue(T value, long timestamp) {
        checkNotNull(value, "value");
        this.timestamp = timestamp;
        this.value = value;
    }

    /**
     * Gets the timestamp of this value.
     *
     * @return The timestamp
     */
    public long getTimestamp() {
        return this.timestamp;
    }

    /**
     * Gets the value.
     *
     * @return The value
     */
    public T getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", this.value)
                .add("timestamp", this.timestamp)
                .toString();
    }
}
