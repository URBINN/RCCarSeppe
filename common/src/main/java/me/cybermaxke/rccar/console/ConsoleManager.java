/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/*
 * This file is part of LanternServer, licensed under the MIT License (MIT).
 *
 * Copyright (c) LanternPowered <https://www.lanternpowered.org>
 * Copyright (c) SpongePowered <https://www.spongepowered.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package me.cybermaxke.rccar.console;

import static me.cybermaxke.rccar.Framework.getLogger;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.util.function.Consumer;

public final class ConsoleManager {

    // Whether the command threads are running
    private volatile boolean active;

    private final Consumer<String> lineConsumer;

    public ConsoleManager(Consumer<String> lineConsumer) {
        this.lineConsumer = lineConsumer;
    }

    public void start() {
        this.active = true;

        // Start the command reader thread
        final Thread thread = new Thread(this::commandReaderTask, "console");
        thread.setDaemon(true);
        thread.start();
    }

    public void shutdown() {
        this.active = false;
    }

    /**
     * This task handles the commands that are executed through the console.
     */
    private void commandReaderTask() {
        final ConsoleReader reader = ConsoleLaunch.getReader();
        while (this.active) {
            try {
                //noinspection ConstantConditions
                String line = reader.readLine("> ");
                if (line != null && !line.isEmpty()) {
                    this.lineConsumer.accept(line);
                }
            } catch (IOException e) {
                getLogger().error("Error while reading the command line!", e);
            }
        }
    }

}
