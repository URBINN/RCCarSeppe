/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.sys;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Duration;
import java.util.function.DoubleConsumer;

/**
 * A pulseable object will receive continuously updates.
 */
public interface PulseableSystemPiece extends SystemPiece {

    /**
     * The default interval of the pulses.
     */
    Duration INTERVAL_50_MILLIS = Duration.ofMillis(50);

    /**
     * The default interval of the pulses.
     */
    Duration INTERVAL_25_MILLIS = Duration.ofMillis(25);

    static PulseableSystemPiece of(DoubleConsumer function) {
        return of(function, INTERVAL_50_MILLIS);
    }

    static PulseableSystemPiece of(DoubleConsumer function, Duration pulseInterval) {
        checkNotNull(function, "function");
        checkNotNull(pulseInterval, "pulseInterval");
        return new PulseableSystemPiece() {
            @Override
            public void pulse(double deltaTime) {
                function.accept(deltaTime);
            }

            @Override
            public Duration getPulseInterval() {
                return pulseInterval;
            }

            @Override
            public void init() {
            }

            @Override
            public void cleanup() {
            }
        };
    }

    /**
     * Gets the delay of this {@link PulseableSystemPiece}.
     *
     * @return The duration
     */
    default Duration getPulseInterval() {
        return INTERVAL_50_MILLIS;
    }

    /**
     * Pulses the {@link PulseableSystemPiece}.
     *
     * @param deltaTime The time since the last update in seconds
     */
    void pulse(double deltaTime);
}
