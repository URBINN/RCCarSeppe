/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.sys;

/**
 * A sys is like a sub system of a big system.
 */
public interface SystemPiece extends AutoCloseable {

    /**
     * Initializes this {@link SystemPiece}.
     */
    void init();

    /**
     * Cleans up and releases the resources of this {@link SystemPiece}.
     */
    void cleanup();

    @Override
    default void close() {
        cleanup();
    }
}
