/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar;

import com.google.common.base.MoreObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Represents a train of {@link Car}s.
 */
public final class Train {

    private final List<Car> cars = new ArrayList<>();
    private final List<Car> unmodifiableCars = Collections.unmodifiableList(this.cars);

    /**
     * Gets a unmodifiable {@link List} with all the {@link Car}s inside this
     * train. Where the car at index 0 is the first {@link Car} in the train.
     *
     * @return The cars
     */
    public List<Car> getCars() {
        return this.unmodifiableCars;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("cars", Arrays.toString(this.cars.stream().map(Car::toString).toArray(String[]::new)))
                .toString();
    }
}
