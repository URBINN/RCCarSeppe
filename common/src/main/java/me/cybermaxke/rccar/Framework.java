/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Throwables;
import me.cybermaxke.rccar.command.Command;
import me.cybermaxke.rccar.command.CommandException;
import me.cybermaxke.rccar.command.CommandManager;
import me.cybermaxke.rccar.command.CommandSystem;
import me.cybermaxke.rccar.console.ConsoleManager;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;
import me.cybermaxke.rccar.sys.SystemPiece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class Framework implements SystemPiece {

    private static final Logger logger = LoggerFactory.getLogger("RCCar");

    public static Logger getLogger() {
        return logger;
    }

    private ScheduledExecutorService systemExecutor;

    private final Map<UUID, Car> cars = new ConcurrentHashMap<>();
    private final Collection<Car> unmodifiableCars = Collections.unmodifiableCollection(this.cars.values());

    private final List<Train> trains = new ArrayList<>();
    private final List<Train> unmodifiableTrains = Collections.unmodifiableList(this.trains);

    private final CommandManager commandManager = new CommandManager();
    private final ConsoleManager consoleManager;

    private final List<SystemPiece> systems = new ArrayList<>();
    private final List<Task> tasks = new ArrayList<>();

    private final static class Task {

        private final Runnable runnable;
        private final long interval;

        private Task(Runnable runnable, long interval) {
            this.runnable = runnable;
            this.interval = interval;
        }
    }

    public Framework() {
        // Setup the command system
        final CommandSystem commandSystem = new CommandSystem(this.commandManager);
        this.consoleManager = new ConsoleManager(commandSystem::add);
        this.consoleManager.start();
        addSystem(commandSystem);
        getCommandManager().register("shutdown", (name, args) -> {
            cleanup();
        });
    }

    @Override
    public void init() {
        if (this.systemExecutor != null) {
            return;
        }
        this.systemExecutor = Executors.newSingleThreadScheduledExecutor(
                runnable -> new Thread(runnable, "system"));
        final List<Future<?>> futures = this.systems.stream()
                .map(systemPiece -> this.systemExecutor.submit(systemPiece::init))
                .collect(Collectors.toList());
        // Wait for all the systems to initialize
        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        for (Task task : this.tasks) {
            this.systemExecutor.scheduleAtFixedRate(task.runnable, task.interval, task.interval, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void cleanup() {
        final List<Future<?>> futures = this.systems.stream()
                .map(systemPiece -> this.systemExecutor.submit(systemPiece::cleanup))
                .collect(Collectors.toList());
        // Wait for all the systems to initialize
        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        this.systemExecutor.shutdownNow();
        this.systemExecutor = null;
        this.consoleManager.shutdown();
    }

    /**
     * Gets a unmodifiable {@link Collection} with all the {@link Car}s within
     * the network/framework.
     *
     * @return The cars
     */
    public Collection<Car> getCars() {
        return this.unmodifiableCars;
    }

    /**
     * Gets a unmodifiable {@link Collection} with all the {@link Train}s within
     * the network/framework.
     *
     * @return The trains
     */
    public Collection<Train> getTrains() {
        return this.unmodifiableTrains;
    }

    /**
     * Adds a {@link Car} to the framework or gets the previous
     * instance if present.
     *
     * @param uniqueId The unique id
     * @return The car
     */
    public Car getOrAddCar(UUID uniqueId) {
        checkNotNull(uniqueId, "uniqueId");
        return this.cars.computeIfAbsent(uniqueId, this::newCar);
    }

    /**
     * Gets the {@link Car} with it's {@link UUID} if present.
     *
     * @param uniqueId The unique id
     * @return The car
     */
    public Optional<Car> getCar(UUID uniqueId) {
        return Optional.ofNullable(this.cars.get(uniqueId));
    }

    protected Car newCar(UUID uniqueId) {
        return new Car(uniqueId);
    }

    /**
     * Gets the {@link CommandManager}.
     *
     * @return The command manager
     */
    public CommandManager getCommandManager() {
        return this.commandManager;
    }

    /**
     * Adds a new {@link SystemPiece} to the {@link Framework}.
     *
     * @param systemPiece The system piece
     */
    protected void addSystem(SystemPiece systemPiece) {
        checkNotNull(systemPiece, "systemPiece");
        if (systemPiece == this) {
            return;
        }
        this.systems.add(systemPiece);
        if (systemPiece instanceof PulseableSystemPiece) {
            final PulseableSystemPiece pulseable = (PulseableSystemPiece) systemPiece;
            final Duration duration = pulseable.getPulseInterval();
            final long millis = duration.toMillis();
            checkArgument(millis > 1, "The interval should be at least 1 millisecond.");
            final long[] lastTime = new long[1];
            final Runnable runnable = () -> {
                final float deltaTime;
                final long time = System.currentTimeMillis();
                if (lastTime[0] == 0) {
                    deltaTime = (float) ((double) millis / 1000.0);
                } else {
                    final long lastTime0 = lastTime[0];
                    deltaTime = (float) (((double) (time - lastTime0)) / 1000.0);
                }
                lastTime[0] = time;
                try {
                    pulseable.pulse(deltaTime);
                } catch (Exception e) {
                    getLogger().error("An error occurred while pulsing {}", pulseable.getClass().getName(), e);
                }
            };
            this.tasks.add(new Task(runnable, millis));
            if (this.systemExecutor != null) {
                try {
                    this.systemExecutor.submit(systemPiece::init).get();
                } catch (InterruptedException | ExecutionException e) {
                    throw Throwables.propagate(e);
                }
                this.systemExecutor.scheduleAtFixedRate(runnable, millis, millis, TimeUnit.MILLISECONDS);
            }
        }
    }
}
