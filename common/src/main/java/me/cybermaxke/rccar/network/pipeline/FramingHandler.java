/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.pipeline;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.readableVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVarInt;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

public class FramingHandler extends ByteToMessageCodec<ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf buf0, ByteBuf output) throws Exception {
        writeVarInt(output, buf0.readableBytes());
        output.writeBytes(buf0);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> output) throws Exception {
        while (readableVarInt(buf)) {
            buf.markReaderIndex();

            final int length = readVarInt(buf);
            if (buf.readableBytes() < length) {
                buf.resetReaderIndex();
                break;
            }

            final ByteBuf msg = ctx.alloc().buffer(length);
            buf.readBytes(msg, length);

            output.add(msg);
        }
    }
}
