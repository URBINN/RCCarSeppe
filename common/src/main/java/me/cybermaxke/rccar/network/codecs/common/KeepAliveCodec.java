/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.common;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.common.KeepAlivePacket;

public class KeepAliveCodec implements Codec<KeepAlivePacket> {

    @Override
    public KeepAlivePacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        return new KeepAlivePacket(buf.readLong());
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, KeepAlivePacket packet) {
        return allocator.buffer(Long.BYTES).writeLong(packet.getTime());
    }
}
