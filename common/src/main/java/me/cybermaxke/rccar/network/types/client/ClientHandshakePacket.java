/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.client;

import com.google.common.base.MoreObjects;

import me.cybermaxke.rccar.network.Packet;

import java.util.UUID;

public final class ClientHandshakePacket implements Packet {

    private final UUID uniqueId;
    private final int protocol;

    public ClientHandshakePacket(UUID uniqueId, int protocol) {
        this.uniqueId = uniqueId;
        this.protocol = protocol;
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public int getProtocol() {
        return this.protocol;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uniqueId", this.uniqueId)
                .add("protocol", this.protocol)
                .toString();
    }
}
