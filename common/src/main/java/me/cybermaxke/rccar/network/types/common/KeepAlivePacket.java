/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.common;

import com.google.common.base.MoreObjects;

import me.cybermaxke.rccar.network.Packet;

public final class KeepAlivePacket implements Packet {

    private final long time;

    public KeepAlivePacket(long time) {
        this.time = time;
    }

    public long getTime() {
        return this.time;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("time", this.time)
                .toString();
    }
}
