/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.client;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;
import me.cybermaxke.rccar.network.Packet;

public final class TransformPacket implements Packet {

    private final Vector3d position;
    private final Vector3d rotation;
    private final long updateTime;

    public TransformPacket(Vector3d position, Vector3d rotation, long updateTime) {
        this.position = position;
        this.rotation = rotation;
        this.updateTime = updateTime;
    }

    public Vector3d getPosition() {
        return this.position;
    }

    public Vector3d getRotation() {
        return this.rotation;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("position", this.position)
                .add("rotation", this.rotation)
                .add("updateTime", this.updateTime)
                .toString();
    }
}
