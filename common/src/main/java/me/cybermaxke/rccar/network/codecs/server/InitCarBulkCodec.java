/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.server;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVarInt;

import com.google.common.collect.ImmutableList;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.server.InitCarBulkPacket;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;

import java.util.List;

public final class InitCarBulkCodec implements Codec<InitCarBulkPacket> {

    @Override
    public InitCarBulkPacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        final ImmutableList.Builder<InitCarPacket> packets = ImmutableList.builder();
        final int count = readVarInt(buf);
        for (int i = 0; i < count; i++) {
            packets.add(InitCarCodec.decode0(buf));
        }
        return new InitCarBulkPacket(packets.build());
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, InitCarBulkPacket packet) {
        final ByteBuf buf = allocator.buffer();
        final List<InitCarPacket> packets = packet.getPackets();
        writeVarInt(buf, packets.size());
        for (InitCarPacket packet1 : packets) {
            InitCarCodec.encode0(buf, packet1);
        }
        return buf;
    }
}
