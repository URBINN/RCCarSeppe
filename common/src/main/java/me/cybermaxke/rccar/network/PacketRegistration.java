/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;

import java.util.Optional;

import javax.annotation.Nullable;

public final class PacketRegistration<P extends Packet> {

    private final int opcode;
    private final Class<P> message;
    private final Codec<P> codec;

    @Nullable private Handler<? super P> clientHandler;
    @Nullable private Handler<? super P> serverHandler;

    PacketRegistration(int opcode, Class<P> message, Codec<P> codec) {
        this.message = message;
        this.opcode = opcode;
        this.codec = codec;
    }

    public void bindClientHandler(Handler<? super P> handler) {
        this.clientHandler = checkNotNull(handler, "handler");
    }

    public void bindServerHandler(Handler<? super P> handler) {
        this.serverHandler = checkNotNull(handler, "handler");
    }

    public int getOpcode() {
        return this.opcode;
    }

    public Class<P> getMessage() {
        return message;
    }

    public Codec<P> getCodec() {
        return this.codec;
    }

    public Optional<Handler<? super P>> getClientHandler() {
        return Optional.ofNullable(this.clientHandler);
    }

    public Optional<Handler<? super P>> getServerHandler() {
        return Optional.ofNullable(this.serverHandler);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("opcode", this.opcode)
                .add("message", this.message)
                .add("codec", this.codec.getClass().getName())
                .toString();
    }
}
