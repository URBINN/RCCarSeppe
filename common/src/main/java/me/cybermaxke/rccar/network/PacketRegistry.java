/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import me.cybermaxke.rccar.network.codecs.client.ClientHandshakeCodec;
import me.cybermaxke.rccar.network.codecs.client.TransformCodec;
import me.cybermaxke.rccar.network.codecs.server.InitCarBulkCodec;
import me.cybermaxke.rccar.network.codecs.server.InitCarCodec;
import me.cybermaxke.rccar.network.codecs.common.KeepAliveCodec;
import me.cybermaxke.rccar.network.codecs.server.ServerHandshakeCodec;
import me.cybermaxke.rccar.network.codecs.server.UpdateCarCodec;
import me.cybermaxke.rccar.network.types.client.ClientHandshakePacket;
import me.cybermaxke.rccar.network.types.client.TransformPacket;
import me.cybermaxke.rccar.network.types.common.KeepAlivePacket;
import me.cybermaxke.rccar.network.types.server.InitCarBulkPacket;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;
import me.cybermaxke.rccar.network.types.server.ServerHandshakePacket;
import me.cybermaxke.rccar.network.types.server.UpdateCarPacket;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PacketRegistry {

    public static final int PROTOCOL_VERSION = 1;

    private final List<PacketRegistration> registrationsByIndex = new ArrayList<>();
    private final Map<Class<?>, PacketRegistration> registrationsByType = new HashMap<>();

    {
        register(ClientHandshakePacket.class, ClientHandshakeCodec.class);
        register(ServerHandshakePacket.class, ServerHandshakeCodec.class);
        register(KeepAlivePacket.class, KeepAliveCodec.class);
        register(InitCarPacket.class, InitCarCodec.class);
        register(InitCarBulkPacket.class, InitCarBulkCodec.class);
        register(TransformPacket.class, TransformCodec.class);
        register(UpdateCarPacket.class, UpdateCarCodec.class);
    }

    /**
     * Registers a {@link Packet} type with a specific {@link Codec}.
     *
     * @param type The packet type to register
     * @param codec The codec to use to encode/decode the packet
     * @param <P> The packet type
     * @return The registration
     */
    @SuppressWarnings("unchecked")
    public <P extends Packet> PacketRegistration<P> register(Class<P> type, Class<? extends Codec<P>> codec) {
        checkNotNull(type, "type");
        checkNotNull(codec, "codec");
        checkArgument(!this.registrationsByType.containsKey(type), "The Packet type %s is already registered.", type.getName());
        final Codec<P> codec0;
        try {
            final Constructor<?> constructor = codec.getDeclaredConstructor();
            constructor.setAccessible(true);
            codec0 = (Codec<P>) constructor.newInstance();
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("The Codec %s is missing a empty constructor.");
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        final int opcode = this.registrationsByIndex.size();
        final PacketRegistration packetRegistration = new PacketRegistration(opcode, type, codec0);
        this.registrationsByIndex.add(packetRegistration);
        this.registrationsByType.put(type, packetRegistration);
        return packetRegistration;
    }

    public <P extends Packet> void bindClientHandler(Class<P> type, Handler<? super P> handler) {
        checkNotNull(type, "type");
        checkNotNull(handler, "handler");
        final PacketRegistration<P> registration = get(type)
                .orElseThrow(() -> new IllegalArgumentException(String.format("The Packet type %s isn't registered.", type.getName())));
        registration.bindClientHandler(handler);
    }

    public <P extends Packet> void bindServerHandler(Class<P> type, Handler<? super P> handler) {
        checkNotNull(type, "type");
        checkNotNull(handler, "handler");
        final PacketRegistration<P> registration = get(type)
                .orElseThrow(() -> new IllegalArgumentException(String.format("The Packet type %s isn't registered.", type.getName())));
        registration.bindServerHandler(handler);
    }

    /**
     * Gets the {@link PacketRegistration} for the specified
     * {@link Packet} type if present.
     *
     * @param type The packet type
     * @param <P> The packet type
     * @return The packet registration if present
     */
    public <P extends Packet> Optional<PacketRegistration<P>> get(Class<P> type) {
        //noinspection unchecked
        return Optional.ofNullable(this.registrationsByType.get(checkNotNull(type, "type")));
    }

    /**
     * Gets the {@link PacketRegistration} for the specified
     * opcode type if present.
     *
     * @param opcode The opcode
     * @param <P> The packet type
     * @return The packet registration if present
     */
    public <P extends Packet> Optional<PacketRegistration<P>> get(int opcode) {
        if (opcode < 0 || opcode >= this.registrationsByIndex.size()) {
            return Optional.empty();
        }
        //noinspection unchecked
        return Optional.of(this.registrationsByIndex.get(opcode));
    }
}
