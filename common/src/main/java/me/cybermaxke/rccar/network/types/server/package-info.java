/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/**
 * This package contains all the packets that are send from the
 * the server to the client. Will be handled on the client.
 */
package me.cybermaxke.rccar.network.types.server;