/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.client;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVarInt;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.client.ClientHandshakePacket;

import java.util.UUID;

public class ClientHandshakeCodec implements Codec<ClientHandshakePacket> {

    @Override
    public ClientHandshakePacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        final long most = buf.readLong();
        final long least = buf.readLong();
        final int protocol = readVarInt(buf);
        return new ClientHandshakePacket(new UUID(most, least), protocol);
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, ClientHandshakePacket packet) {
        final ByteBuf buf = allocator.buffer();
        final UUID uuid = packet.getUniqueId();
        buf.writeLong(uuid.getMostSignificantBits());
        buf.writeLong(uuid.getLeastSignificantBits());
        writeVarInt(buf, packet.getProtocol());
        return buf;
    }
}
