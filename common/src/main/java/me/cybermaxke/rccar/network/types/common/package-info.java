/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/**
 * This package contains all the packets that are send from the
 * the client to the server and from the server to the client.
 * Will be handled on both the server and client.
 */
package me.cybermaxke.rccar.network.types.common;