/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

public interface Handler<P extends Packet> {

    void handle(Session session, P packet);
}
