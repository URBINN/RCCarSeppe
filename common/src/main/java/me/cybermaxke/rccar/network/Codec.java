/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

public interface Codec<P extends Packet> {

    P decode(ByteBufAllocator allocator, ByteBuf buf);

    ByteBuf encode(ByteBufAllocator allocator, P packet);
}
