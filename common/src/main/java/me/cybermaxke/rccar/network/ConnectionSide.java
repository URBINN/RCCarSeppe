/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

public enum ConnectionSide {
    CLIENT,
    SERVER,
}
