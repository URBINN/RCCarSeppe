/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/**
 * Connection protocol:
 *
 * 1. Car enters network area, automatically attempts to connect to the server.
 * 2. Car (client) sends a ClientHandshakePacket to the server with initial info (UUID), protocol version.
 * 3. Server verifies the current protocol version and sends a ServerHandshakePacket to the client.
 *    The connection will be closed afterwards if the protocol versions were a mismatch.
 *    -> Can be expanded in the future to allow cross protocol version support.
 * 4. Send InitCarBulkPacket to the client to register all the known cars and their positions. Not all
 *    of them may be connected at the time.
 * 5. Send KeepAlivePacket's between client and server to keep the connection open, every 2 seconds or so.
 *
 *
 * Car UUID:
 *
 * Every cars gets a unique id, they will be generated when the program is run for the first time or when
 * the UUID file is removed from the drive. The UUID is saved in a single file.
 *
 *
 * Data protocol:
 *
 * Every time a car joins the framework, a InitCarPacket will be send to all the other car sessions in
 * the network.
 */
package me.cybermaxke.rccar.network;
