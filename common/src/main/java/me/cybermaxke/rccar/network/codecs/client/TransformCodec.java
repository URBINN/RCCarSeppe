/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.client;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVector3d;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVector3d;

import com.flowpowered.math.vector.Vector3d;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.client.TransformPacket;

public final class TransformCodec implements Codec<TransformPacket> {

    @Override
    public TransformPacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        final Vector3d position = readVector3d(buf);
        final Vector3d rotation = readVector3d(buf);
        final long updateTime = buf.readLong();
        return new TransformPacket(position, rotation, updateTime);
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, TransformPacket packet) {
        final ByteBuf buf = allocator.buffer();
        writeVector3d(buf, packet.getPosition());
        writeVector3d(buf, packet.getRotation());
        buf.writeLong(packet.getUpdateTime());
        return buf;
    }
}
