/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.server;

import com.google.common.base.MoreObjects;

import me.cybermaxke.rccar.network.Packet;

public final class ServerHandshakePacket implements Packet {

    private final int protocol;

    public ServerHandshakePacket(int protocol) {
        this.protocol = protocol;
    }

    public int getProtocol() {
        return this.protocol;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("protocol", this.protocol)
                .toString();
    }
}
