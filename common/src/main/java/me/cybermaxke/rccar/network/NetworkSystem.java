/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import io.netty.util.concurrent.FastThreadLocalThread;

import me.cybermaxke.rccar.sys.SystemPiece;

import java.util.concurrent.ThreadFactory;

public interface NetworkSystem extends SystemPiece {

    /**
     * The netty {@link Thread} factory.
     */
    ThreadFactory NETTY_THREAD_FACTORY = runnable -> new FastThreadLocalThread(runnable, "netty-" + NettyThreadCounter.INSTANCE.getAndIncrement());

}
