/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.server;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVarInt;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.server.ServerHandshakePacket;

public class ServerHandshakeCodec implements Codec<ServerHandshakePacket> {

    @Override
    public ServerHandshakePacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        final int protocol = readVarInt(buf);
        return new ServerHandshakePacket(protocol);
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, ServerHandshakePacket packet) {
        final ByteBuf buf = allocator.buffer();
        writeVarInt(buf, packet.getProtocol());
        return buf;
    }
}
