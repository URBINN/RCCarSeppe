/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.codecs.server;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVector3d;
import static me.cybermaxke.rccar.network.ByteBufUtils.readUUID;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVector3d;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeUUID;

import com.flowpowered.math.vector.Vector3d;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.types.server.UpdateCarPacket;

import java.util.UUID;

public final class UpdateCarCodec implements Codec<UpdateCarPacket> {

    @Override
    public UpdateCarPacket decode(ByteBufAllocator allocator, ByteBuf buf) {
        final UUID uniqueId = readUUID(buf);
        final Vector3d position = readVector3d(buf);
        final Vector3d rotation = readVector3d(buf);
        final long time = buf.readLong();
        return new UpdateCarPacket(uniqueId, position, rotation, time);
    }

    @Override
    public ByteBuf encode(ByteBufAllocator allocator, UpdateCarPacket packet) {
        final ByteBuf buf = allocator.buffer();
        writeUUID(buf, packet.getUniqueId());
        writeVector3d(buf, packet.getPosition());
        writeVector3d(buf, packet.getRotation());
        buf.writeLong(packet.getUpdateTime());
        return buf;
    }
}
