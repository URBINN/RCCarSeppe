/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.server;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;
import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Packet;

import java.util.UUID;

/**
 * Is send when a {@link Car} enters the network.
 *
 * TODO: The position will in the future be replaced with latitude and longitude values
 */
public final class InitCarPacket implements Packet {

    private final UUID uniqueId;
    private final Vector3d position;
    private final Vector3d rotation;
    private final long updateTime;

    public InitCarPacket(UUID uniqueId, Vector3d position, Vector3d rotation, long updateTime) {
        this.uniqueId = uniqueId;
        this.position = position;
        this.rotation = rotation;
        this.updateTime = updateTime;
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public Vector3d getPosition() {
        return this.position;
    }

    public Vector3d getRotation() {
        return this.rotation;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uniqueId", this.uniqueId)
                .add("position", this.position)
                .add("rotation", this.rotation)
                .add("updateTime", this.updateTime)
                .toString();
    }
}
