/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

public final class HandlerPacket<P extends Packet> implements Packet {

    private final P packet;
    private final Handler<? super P> handler;

    public HandlerPacket(P packet, Handler<? super P> handler) {
        this.packet = packet;
        this.handler = handler;
    }

    public P getPacket() {
        return this.packet;
    }

    public Handler<? super P> getHandler() {
        return this.handler;
    }
}
