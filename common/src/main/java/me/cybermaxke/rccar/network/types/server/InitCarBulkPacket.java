/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.types.server;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import me.cybermaxke.rccar.network.Packet;

import java.util.Arrays;
import java.util.List;

public final class InitCarBulkPacket implements Packet {

    private final List<InitCarPacket> packets;

    public InitCarBulkPacket(List<InitCarPacket> packets) {
        this.packets = ImmutableList.copyOf(packets);
    }

    public List<InitCarPacket> getPackets() {
        return this.packets;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("entries", Arrays.toString(this.packets.toArray(new InitCarPacket[this.packets.size()])))
                .toString();
    }
}
