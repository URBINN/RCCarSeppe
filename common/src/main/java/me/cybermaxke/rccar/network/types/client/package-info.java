/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
/**
 * This package contains all the packets that are send from the
 * the client to the server. Will be handled on the server.
 */
package me.cybermaxke.rccar.network.types.client;