/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import static com.google.common.base.Preconditions.checkState;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.CodecException;
import io.netty.handler.timeout.ReadTimeoutException;
import me.cybermaxke.rccar.Framework;

import java.io.IOException;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Session extends SimpleChannelInboundHandler<Packet> {

    public static final int READ_TIMEOUT_SECONDS = 10;

    /**
     * The port the client/server will listen on.
     */
    public static final int PORT = 5555;

    private final Channel channel;
    private final PacketRegistry packetRegistry;
    private final ConnectionSide connectionSide;

    /**
     * A queue of incoming messages that must be handled on
     * the synchronous thread.
     */
    private final Queue<HandlerPacket> packetQueue = new ConcurrentLinkedDeque<>();

    private final Framework framework;

    private int protocolVersion = -1;

    protected String disconnectReason;

    public Session(Framework framework, PacketRegistry packetRegistry, ConnectionSide connectionSide, Channel channel) {
        this.packetRegistry = packetRegistry;
        this.connectionSide = connectionSide;
        this.framework = framework;
        this.channel = channel;
    }

    /**
     * Gets the {@link Channel}.
     *
     * @return The channel
     */
    public Channel getChannel() {
        return this.channel;
    }

    /**
     * Sends a {@link Packet} to this session.
     *
     * @param packet The packet
     */
    public void send(Packet packet) {
        if (this.channel.isOpen()) {
            this.channel.writeAndFlush(packet, this.channel.voidPromise());
        }
    }

    /**
     * Sends a {@link Packet} to this session with a {@link ChannelFuture}.
     *
     * @param packet The packet
     * @return The channel future
     */
    public ChannelFuture sendWithFuture(Packet packet) {
        if (this.channel.isOpen()) {
            return this.channel.writeAndFlush(packet);
        }
        return this.channel.closeFuture();
    }

    /**
     * Processes all the received {@link Packet}s.
     */
    @SuppressWarnings("unchecked")
    public void process() {
        HandlerPacket packet;
        while ((packet = this.packetQueue.poll()) != null) {
            handlePacket(packet.getPacket(), packet.getHandler());
        }
    }

    @SuppressWarnings("unchecked")
    private void handlePacket(Packet packet, Handler handler) {
        try {
            handler.handle(this, packet);
        } catch (Exception e) {
            new RuntimeException(String.format("An error occurred while handling the Packet %s with the Handler %s",
                    packet, handler.getClass().getName()), e).printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) throws Exception {
        if (msg instanceof HandlerPacket) {
            final Handler handler = ((HandlerPacket) msg).getHandler();
            if (handler instanceof SyncHandler) {
                this.packetQueue.add((HandlerPacket) msg);
            } else {
                handlePacket(((HandlerPacket) msg).getPacket(), handler);
            }
        } else {
            final Optional<PacketRegistration> registration = (Optional) this.packetRegistry.get(msg.getClass());
            if (registration.isPresent()) {
                Handler handler;
                if (this.connectionSide == ConnectionSide.CLIENT) {
                    handler = (Handler) registration.get().getClientHandler().orElse(null);
                } else {
                    handler = (Handler) registration.get().getServerHandler().orElse(null);
                }
                if (handler instanceof SyncHandler) {
                    this.packetQueue.add(new HandlerPacket(msg, handler));
                } else if (handler != null) {
                    handlePacket(msg, handler);
                }
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Ignore the exceptions:
        // java.io.IOException: De externe host heeft een verbinding verbroken
        // java.io.IOException: The external host broke the connection
        // java.io.IOException: Connection reset by peer
        if (cause instanceof IOException) {
            final StackTraceElement[] stack = cause.getStackTrace();
            if ((stack.length != 0 && stack[0].toString().startsWith("sun.nio.ch.SocketDispatcher.read0"))
                    || cause.getMessage().contains("Connection reset by peer")) {
                this.disconnectReason = "Disconnected";
                return;
            }
        }
        if (cause instanceof ReadTimeoutException) {
            this.disconnectReason = "Read Timeout";
        } else if (cause instanceof CodecException) {
            new RuntimeException("A netty pipeline error occurred", cause).printStackTrace();
        } else {
            this.channel.close();
            new RuntimeException("A netty connection error occurred", cause).printStackTrace();
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        this.disconnectReason = null;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    /**
     * Gets the protocol version of this {@link Session}.
     *
     * @return The protocol version
     */
    public int getProtocolVersion() {
        return this.protocolVersion;
    }

    /**
     * Sets the protocol version of this {@link Session}. This
     * can only be done once.
     *
     * @param protocolVersion The protocol version
     */
    public void setProtocolVersion(int protocolVersion) {
        checkState(this.protocolVersion == -1, "The protocol version is already set");
        this.protocolVersion = protocolVersion;
    }

    public Framework getFramework() {
        return this.framework;
    }
}
