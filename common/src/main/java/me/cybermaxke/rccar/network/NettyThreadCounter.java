/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network;

import java.util.concurrent.atomic.AtomicInteger;

final class NettyThreadCounter {

    static final AtomicInteger INSTANCE = new AtomicInteger();
}
