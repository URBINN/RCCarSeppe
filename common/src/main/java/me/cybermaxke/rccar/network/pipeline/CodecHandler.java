/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.network.pipeline;

import static me.cybermaxke.rccar.network.ByteBufUtils.readVarInt;
import static me.cybermaxke.rccar.network.ByteBufUtils.writeVarInt;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.EncoderException;
import io.netty.handler.codec.MessageToMessageCodec;

import me.cybermaxke.rccar.network.Codec;
import me.cybermaxke.rccar.network.ConnectionSide;
import me.cybermaxke.rccar.network.Handler;
import me.cybermaxke.rccar.network.HandlerPacket;
import me.cybermaxke.rccar.network.Packet;
import me.cybermaxke.rccar.network.PacketRegistration;
import me.cybermaxke.rccar.network.PacketRegistry;

import java.util.List;

public final class CodecHandler extends MessageToMessageCodec<ByteBuf, Packet> {

    private final PacketRegistry packetRegistry;
    private final ConnectionSide connectionSide;

    public CodecHandler(PacketRegistry packetRegistry, ConnectionSide connectionSide) {
        this.packetRegistry = packetRegistry;
        this.connectionSide = connectionSide;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void encode(ChannelHandlerContext ctx, Packet msg, List<Object> out) throws Exception {
        final PacketRegistration registration = this.packetRegistry.get(msg.getClass())
                .orElseThrow(() -> new EncoderException("Unsupported Packet type: " + msg.getClass().getName()));
        final ByteBuf buf = ctx.alloc().buffer();
        writeVarInt(buf, registration.getOpcode());
        final Codec codec = registration.getCodec();
        try {
            final ByteBuf content = codec.encode(ctx.alloc(), msg);
            try {
                buf.writeBytes(content);
            } finally {
                content.release();
            }
        } catch (Exception e) {
            buf.release();
            throw e;
        }
        out.add(buf);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        final int opcode = readVarInt(msg);
        final PacketRegistration registration = this.packetRegistry.get(opcode)
                .orElseThrow(() -> new EncoderException("Unsupported Packet type: " + opcode));
        final Codec codec = registration.getCodec();
        final Packet packet = codec.decode(ctx.alloc(), msg.slice());
        Handler handler;
        if (this.connectionSide == ConnectionSide.CLIENT) {
            handler = (Handler) registration.getClientHandler().orElse(null);
        } else {
            handler = (Handler) registration.getServerHandler().orElse(null);
        }
        if (handler != null) {
            out.add(new HandlerPacket(packet, handler));
        } else {
            out.add(packet);
        }
    }
}
