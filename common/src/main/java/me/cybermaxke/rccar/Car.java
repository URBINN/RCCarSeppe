/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.base.MoreObjects;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

/**
 * Represents a car that is connected to a server. This object contains
 * information specific for the car.
 */
public class Car {

    public static final Vector3d UNKNOWN_POSITION = new Vector3d(Double.NaN, Double.NaN, Double.NaN);

    private final UUID uniqueId;

    /**
     * The time when the last update from this
     * car was received.
     */
    protected long lastKnownUpdate = -1L;

    /**
     * The position of this car.
     */
    protected Vector3d position = UNKNOWN_POSITION;

    /**
     * The rotation of this car.
     */
    protected Vector3d rotation = Vector3d.ZERO;

    @Nullable private Train train;
    private int trainPos;

    public Car(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Updates this {@link Car} with the supplied information.
     *
     * @param position The position
     * @param rotation The rotation
     * @param updateTime The update time
     */
    public boolean update(Vector3d position, Vector3d rotation, long updateTime) {
        // We already got an earlier update, by maybe another source
        if (this.lastKnownUpdate > updateTime) {
            return false;
        }
        this.lastKnownUpdate = updateTime;
        this.position = position;
        this.rotation = rotation;
        return true;
    }

    /**
     * Gets the {@link UUID} of this car.
     *
     * @return The unique id
     */
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Gets the {@link TrainPos} of this car if present.
     *
     * @return The train pos
     */
    public Optional<TrainPos> getTrain() {
        if (this.train != null) {
            return Optional.of(new TrainPos(this, this.train, this.trainPos));
        }
        return Optional.empty();
    }

    public void setTrain(@Nullable Train train, int trainPos) {
        this.trainPos = trainPos;
        this.train = train;
    }

    public Vector3d getPosition() {
        return this.position;
    }

    public Vector3d getRotation() {
        return this.rotation;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uniqueId", this.uniqueId)
                .add("position", this.position)
                .add("rotation", this.rotation)
                .toString();
    }
}
