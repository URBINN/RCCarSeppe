/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.command;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class CommandManager {

    private final Map<String, Command> commands = new HashMap<>();

    public void register(String name, Command command) {
        checkNotNull(name, "name");
        checkNotNull(command, "command");
        checkArgument(!name.isEmpty(), "The name cannot be empty");
        name = name.toLowerCase(Locale.ENGLISH);
        checkState(!this.commands.containsKey(name), "The name %s is already in use", name);
        this.commands.put(name, command);
    }

    public void process(String line) throws CommandException {
        line = StringUtils.normalizeSpace(line);
        final int i = line.indexOf('/');
        if (i != -1) {
            line = line.substring(i + 1);
        }
        final String[] args = line.split(" ");
        if (args.length == 0) {
            throw new CommandException("The command cannot be empty.");
        }
        final Command command = this.commands.get(args[0]);
        if (command == null) {
            throw new CommandException("The command " + args[0] + " doesn't exist.");
        }
        final String[] args0 = Arrays.copyOfRange(args, 1, args.length);
        try {
            command.process(args[0], args0);
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException("An error occurred while processing " + args[0], e);
        }
    }
}
