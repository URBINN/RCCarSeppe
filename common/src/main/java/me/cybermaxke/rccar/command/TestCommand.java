/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.command;

import static me.cybermaxke.rccar.Framework.getLogger;

public class TestCommand implements Command {

    @Override
    public void process(String commandName, String[] arguments) throws CommandException {
        getLogger().info("Test");
    }
}
