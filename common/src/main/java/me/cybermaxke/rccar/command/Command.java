/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.command;

public interface Command {

    /**
     * Attempts to process the command for the given arguments.
     *
     * @param commandName The command name that was used
     * @param arguments The arguments
     */
    void process(String commandName, String[] arguments) throws CommandException;
}
