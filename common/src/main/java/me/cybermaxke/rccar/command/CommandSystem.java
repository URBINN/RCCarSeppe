/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.command;

import static com.google.common.base.Preconditions.checkNotNull;
import static me.cybermaxke.rccar.Framework.getLogger;

import me.cybermaxke.rccar.sys.PulseableSystemPiece;
import me.cybermaxke.rccar.sys.SystemPiece;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CommandSystem implements SystemPiece, PulseableSystemPiece {

    private final Queue<String> commands = new ConcurrentLinkedQueue<>();
    private final CommandManager commandManager;

    public CommandSystem(CommandManager commandManager) {
        this.commandManager = checkNotNull(commandManager, "commandManager");
    }

    public void add(String command) {
        this.commands.add(command);
    }

    @Override
    public void init() {
    }

    @Override
    public void cleanup() {
        this.commands.clear();
    }

    @Override
    public void pulse(double deltaTime) {
        String command;
        while ((command = this.commands.poll()) != null) {
            try {
                this.commandManager.process(command);
            } catch (Throwable t) {
                getLogger().error("An error occurred while processing the command: {}", command, t);
            }
        }
    }
}
