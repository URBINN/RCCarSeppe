# RCCar

The purpose of this project is to allow multiple RC Cars to communicate with each other to allow them to move as a train. The main focus is on the communication part and allowing the RC Cars to position themselves through GPS, Wi-Fi, Bluetooth position system. The RC Car needs to be able to move to a specific point within the range of the positioning system.

The RC Cars are controlled with a Raspberry Pi 3 B. This project contains a server and client, the client will be run on all the RC Cars and the server on for example my laptop. There is also a debug client available that can be used in a testing environment.

## Prerequisites
* [Java 8]

## Clone
The following steps will ensure your project is cloned properly.

1. `git clone https://Cybermaxke@gitlab.com/URBINN/RCCarSeppe.git`
2. `cd rccar`

## Building
__Note:__ If you do not have [Gradle] installed then use ./gradlew for Unix systems or Git Bash and gradlew.bat for Windows systems in place of any 'gradle' command.

In order to build RC Car you simply need to run the `gradle build` command. You can find the compiled JAR file in `./build/libs` labeled similarly to 
'rccar-x.x.x-SNAPSHOT.jar'.

## IDE Setup
__Note:__ If you do not have [Gradle] installed then use ./gradlew for Unix systems or Git Bash and gradlew.bat for Windows systems in place of any 'gradle' command.

__For [Eclipse]__
  1. Run `gradle eclipse`
  2. Import RCCar as an existing project (File > Import > General)
  3. Select the root folder for RCCar
  4. Check RCCar when it finishes building and click **Finish**

__For [IntelliJ]__
  1. Make sure you have the Gradle plugin enabled (File > Settings > Plugins)
  2. Click File > New > Project from Existing Sources > Gradle and select the root folder for RCCar
  3. Select Use customizable gradle wrapper if you do not have Gradle installed.
  4. IntelliJ will now ask to reload the project, click **Yes**

[Eclipse]: https://eclipse.org/
[Gradle]: https://www.gradle.org/
[IntelliJ]: http://www.jetbrains.com/idea/
[Java 8]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
