/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server;

import com.flowpowered.math.vector.Vector3d;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;
import me.cybermaxke.rccar.network.types.server.UpdateCarPacket;
import me.cybermaxke.rccar.server.network.ServerSession;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

public class ServerCar extends Car {

    @Nullable private volatile ServerSession session;

    public ServerCar(UUID uniqueId) {
        super(uniqueId);
    }

    @Override
    public boolean update(Vector3d position, Vector3d rotation, long updateTime) {
        final boolean result = super.update(position, rotation, updateTime);
        final ServerSession session = this.session;
        if (result && session != null) {
            // Send the position update to the client
            final Collection<Car> cars = new ArrayList<>(session.getFramework().getCars());
            if (cars.size() > 1) {
                final UpdateCarPacket packet = new UpdateCarPacket(getUniqueId(), position, rotation, updateTime);
                cars.forEach(car -> {
                    if (car != this) {
                        ((ServerCar) car).getSession().ifPresent(s -> s.send(packet));
                    }
                });
            }
        }
        return result;
    }

    public InitCarPacket createInitPacket() {
        return new InitCarPacket(getUniqueId(), this.position, rotation, this.lastKnownUpdate);
    }

    public Optional<ServerSession> getSession() {
        return Optional.ofNullable(this.session);
    }

    public void setSession(@Nullable ServerSession session) {
        this.session = session;
    }
}
