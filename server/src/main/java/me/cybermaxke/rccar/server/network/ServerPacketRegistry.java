/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server.network;

import me.cybermaxke.rccar.network.PacketRegistry;
import me.cybermaxke.rccar.network.types.client.ClientHandshakePacket;
import me.cybermaxke.rccar.network.types.client.TransformPacket;
import me.cybermaxke.rccar.server.network.handler.ClientHandshakeHandler;
import me.cybermaxke.rccar.server.network.handler.TransformHandler;

public class ServerPacketRegistry extends PacketRegistry {

    {
        bindServerHandler(ClientHandshakePacket.class, new ClientHandshakeHandler());
        bindServerHandler(TransformPacket.class, new TransformHandler());
    }
}
