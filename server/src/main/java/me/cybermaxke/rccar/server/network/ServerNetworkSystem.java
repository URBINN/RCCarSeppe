/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server.network;

import com.google.common.collect.Sets;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

import me.cybermaxke.rccar.network.ConnectionSide;
import me.cybermaxke.rccar.network.NetworkSystem;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.pipeline.CodecHandler;
import me.cybermaxke.rccar.network.pipeline.FramingHandler;
import me.cybermaxke.rccar.server.Server;
import me.cybermaxke.rccar.sys.PulseableSystemPiece;

import java.util.Collection;
import java.util.Set;

public class ServerNetworkSystem implements NetworkSystem, PulseableSystemPiece {

    /**
     * A {@link Set} with all the connected {@link ServerSession}s.
     */
    private final Set<ServerSession> sessions = Sets.newConcurrentHashSet();
    private final Server server;

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    public ServerNetworkSystem(Server server) {
        this.server = server;
    }

    @Override
    public void init() {
        if (this.workerGroup != null) {
            return;
        }
        final ServerPacketRegistry packetRegistry = new ServerPacketRegistry();

        final boolean epoll = Epoll.isAvailable();
        this.bossGroup = epoll ? new EpollEventLoopGroup(0, NETTY_THREAD_FACTORY) : new NioEventLoopGroup(0, NETTY_THREAD_FACTORY);
        this.workerGroup = epoll ? new EpollEventLoopGroup(0, NETTY_THREAD_FACTORY) : new NioEventLoopGroup(0, NETTY_THREAD_FACTORY);

        final ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(this.bossGroup, this.workerGroup);
        bootstrap.channel(epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class);
        bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline()
                        .addLast(new ReadTimeoutHandler(Session.READ_TIMEOUT_SECONDS))
                        .addLast(new FramingHandler())
                        .addLast(new CodecHandler(packetRegistry, ConnectionSide.SERVER))
                        .addLast(new ServerSession(server, ch, packetRegistry, ConnectionSide.SERVER, ServerNetworkSystem.this));
            }
        });
        try {
            bootstrap.bind(Session.PORT).sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cleanup() {
        if (this.workerGroup == null) {
            return;
        }
        try {
            this.bossGroup.shutdownGracefully().await();
            this.workerGroup.shutdownGracefully().await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void channelActive(ServerSession session) {
        this.sessions.add(session);
    }

    void channelInactive(ServerSession session) {
        this.sessions.add(session);
    }

    @Override
    public void pulse(double deltaTime) {
        this.sessions.forEach(me.cybermaxke.rccar.server.network.ServerSession::process);
    }

    public Collection<ServerSession> getSessions() {
        return this.sessions;
    }
}
