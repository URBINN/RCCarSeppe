/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server.network.handler;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Handler;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.client.TransformPacket;
import me.cybermaxke.rccar.server.network.ServerSession;

public final class TransformHandler implements Handler<TransformPacket> {

    @Override
    public void handle(Session session, TransformPacket packet) {
        final Car car = ((ServerSession) session).getCar();
        car.update(packet.getPosition(), packet.getRotation(), packet.getUpdateTime());
    }
}
