/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server;

import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.Framework;
import me.cybermaxke.rccar.server.network.ServerNetworkSystem;

import java.util.UUID;

public final class Server extends Framework {

    public static void main(String[] args) {
        final Server server = new Server();
        server.init();
    }

    private Server() {
        addSystem(new ServerNetworkSystem(this));
    }

    @Override
    public void init() {
        super.init();
        getLogger().info("The server is now successfully started.");
    }

    @Override
    protected Car newCar(UUID uniqueId) {
        return new ServerCar(uniqueId);
    }
}
