/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server.network;

import static com.google.common.base.Preconditions.checkState;
import static me.cybermaxke.rccar.Framework.getLogger;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.ScheduledFuture;
import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.ConnectionSide;
import me.cybermaxke.rccar.network.Packet;
import me.cybermaxke.rccar.network.PacketRegistry;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.common.KeepAlivePacket;
import me.cybermaxke.rccar.server.Server;
import me.cybermaxke.rccar.server.ServerCar;

import java.util.concurrent.TimeUnit;

public class ServerSession extends Session {

    private static final int KEEP_ALIVE_PACKET_DELAY = 2000;

    private final ServerNetworkSystem networkSystem;

    private long keepAliveTime = -1L;
    private ScheduledFuture<?> keepAliveTask;

    private volatile Car car;

    public ServerSession(Server server, Channel channel, PacketRegistry packetRegistry,
            ConnectionSide connectionSide, ServerNetworkSystem networkSystem) {
        super(server, packetRegistry, connectionSide, channel);
        this.networkSystem = networkSystem;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        this.keepAliveTask = ctx.channel().eventLoop().scheduleAtFixedRate(() -> {
            final long time = System.currentTimeMillis();
            if (this.keepAliveTime != -1L) {
                getLogger().info("Took too long for a KeepAlivePacket response from {}, sending next packet.",
                        ctx.channel().remoteAddress());
            }
            this.keepAliveTime = time;
            send(new KeepAlivePacket(time));
        }, 0, KEEP_ALIVE_PACKET_DELAY, TimeUnit.MILLISECONDS);
        getLogger().info("Connection opened from: {}", ctx.channel().remoteAddress().toString());
        this.networkSystem.channelActive(this);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        this.keepAliveTask.cancel(true);
        this.keepAliveTask = null;
        getLogger().info("Connection closed from: {} -> Reason: {}",
                ctx.channel().remoteAddress().toString(), (this.disconnectReason == null ? "Unknown" : this.disconnectReason));
        this.networkSystem.channelInactive(this);
        if (this.car != null) {
            ((ServerCar) this.car).setSession(null);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) throws Exception {
        if (msg instanceof KeepAlivePacket) {
            final long time = ((KeepAlivePacket) msg).getTime();
            if (time == this.keepAliveTime) {
                this.keepAliveTime = -1L;
            }
            // TODO: Calculate latency?
        } else {
            super.channelRead0(ctx, msg);
        }
    }

    public Car getCar() {
        checkState(this.car != null, "The car is not initialized yet");
        return this.car;
    }

    public void setCar(Car car) {
        checkState(this.car == null, "The car only be set once");
        this.car = car;
    }
}
