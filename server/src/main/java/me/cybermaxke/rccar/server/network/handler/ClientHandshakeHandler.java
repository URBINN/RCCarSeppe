/*
 * Copyright (c) Seppe Volkaerts - All Rights Reserved
 */
package me.cybermaxke.rccar.server.network.handler;

import static me.cybermaxke.rccar.Framework.getLogger;

import io.netty.channel.ChannelFutureListener;
import me.cybermaxke.rccar.Car;
import me.cybermaxke.rccar.network.Handler;
import me.cybermaxke.rccar.network.PacketRegistry;
import me.cybermaxke.rccar.network.Session;
import me.cybermaxke.rccar.network.types.client.ClientHandshakePacket;
import me.cybermaxke.rccar.network.types.server.InitCarBulkPacket;
import me.cybermaxke.rccar.network.types.server.InitCarPacket;
import me.cybermaxke.rccar.network.types.server.ServerHandshakePacket;
import me.cybermaxke.rccar.server.ServerCar;
import me.cybermaxke.rccar.server.network.ServerSession;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class ClientHandshakeHandler implements Handler<ClientHandshakePacket> {

    @Override
    public void handle(Session session, ClientHandshakePacket packet) {
        session.setProtocolVersion(packet.getProtocol());
        final ServerHandshakePacket response = new ServerHandshakePacket(PacketRegistry.PROTOCOL_VERSION);
        getLogger().info("Connection from {} performed a handshake with uuid {}",
                session.getChannel().remoteAddress().toString(), packet.getUniqueId());
        if (packet.getProtocol() != PacketRegistry.PROTOCOL_VERSION) {
            session.sendWithFuture(response).addListener(ChannelFutureListener.CLOSE);
            getLogger().info("Handshake from {} ({}) failed: protocol version mismatch.",
                    session.getChannel().remoteAddress().toString(), packet.getUniqueId());
        } else {
            session.send(response);
            getLogger().info("Handshake from {} ({}) is successful.",
                    session.getChannel().remoteAddress().toString(), packet.getUniqueId());
            // Add the car to the framework
            final ServerCar car = (ServerCar) session.getFramework().getOrAddCar(packet.getUniqueId());
            ((ServerSession) session).setCar(car);
            // Check if the connection is not present
            if (car.getSession().isPresent()) {
                getLogger().warn("A session attempted to connect with a duplicate uuid: {}", packet.getUniqueId());
                session.getChannel().close();
                return;
            }
            car.setSession((ServerSession) session);
            session.send(new InitCarBulkPacket(session.getFramework().getCars().stream()
                    .filter(car1 -> car1 != car)
                    .map(car1 -> ((ServerCar) car1).createInitPacket())
                    .collect(Collectors.toList())));
            // Send the position update to the client
            final Collection<Car> cars = new ArrayList<>(session.getFramework().getCars());
            if (cars.size() > 1) {
                final InitCarPacket packet1 = car.createInitPacket();
                cars.forEach(car1 -> {
                    if (car1 != car) {
                        ((ServerCar) car1).getSession().ifPresent(s -> s.send(packet1));
                    }
                });
            }
        }
    }
}
